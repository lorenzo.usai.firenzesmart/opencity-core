<?php

namespace Tests\Controller;

use App\Controller\APIController;
use App\Entity\Allegato;
use App\Entity\ComponenteNucleoFamiliare;
use App\Entity\Ente;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Entity\User;
use App\Logging\LogConstants;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\VarDumper;
use Tests\App\Base\AbstractAppTestCase;

/**
 * Class ApiControllerTest
 */
class ApiControllerTest extends AbstractAppTestCase
{

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->em->getConnection()->executeQuery('DELETE FROM servizio_erogatori')->execute();
        $this->em->getConnection()->executeQuery('DELETE FROM erogatore_ente')->execute();
        $this->em->getConnection()->executeQuery('DELETE FROM ente_asili')->execute();
        $this->cleanDb(ComponenteNucleoFamiliare::class);
        $this->cleanDb(Pratica::class);
        $this->cleanDb(Allegato::class);
        $this->cleanDb(User::class);
        $this->cleanDb(Ente::class);
        $this->cleanDb(Servizio::class);
    }

    /**
     * @test
     */
    public function testStatusAPI()
    {
        $expectedResponse = (object)[
            'status' => 'ok',
            'version' => APIController::CURRENT_API_VERSION,
        ];
        $this->client->request('GET', '/api/' . APIController::CURRENT_API_VERSION . '/status');

        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($expectedResponse, $response);
        $this->assertEquals('application/json', $this->client->getResponse()->headers->get('Content-Type'));
    }

    /**
     * @test
     */
    public function testUsageAPI()
    {
        $pratiche = [
            '2019' => [
                $this->createPratica($this->createCPSUser(),null,Pratica::STATUS_SUBMITTED)->setSubmissionTime(1553763008)
                ],
            '2017' => [
        $this->createPratica($this->createCPSUser(),null,Pratica::STATUS_SUBMITTED)->setSubmissionTime(1491004800),
        $this->createPratica($this->createCPSUser(),null,Pratica::STATUS_SUBMITTED)->setSubmissionTime(1491004802)
                ]
        ];

        $expectedResponse = (object)[
            'status' => 'ok',
            'version' => APIController::CURRENT_API_VERSION,
            'count' => (object)[
                '2019' => count($pratiche['2019']),
                '2017' => count($pratiche['2017'])
            ]
        ];
        $this->client->request('GET', '/api/' . APIController::CURRENT_API_VERSION . '/usage');

        $response = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($expectedResponse, $response);
        $this->assertEquals('application/json', $this->client->getResponse()->headers->get('Content-Type'));
    }

    /**
     * @test
     */
    public function testPostAnnotationsAPI()
    {
        $user = $this->createCPSUser(true);
        $pratica = $this->createPratica($user);
        $notes = "La marianna la va in campagna @#èòàù€’”ß@ł€ ";

        $this->clientRequestAsCPSUser($user, 'POST', '/api/' . APIController::CURRENT_API_VERSION . '/user/' . $pratica->getId() . '/notes', [
            'ContentType' => 'application/json'
        ], [], [], $notes);

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($notes, $pratica->getUserCompilationNotes());
    }

    /**
     * @test
     */
    public function testGetAnnotationsAPI()
    {
        $user = $this->createCPSUser(true);
        $pratica = $this->createPratica($user);
        $notes = "La marianna la va in campagna @#èòàù€’”ß@ł€ ";
        $pratica->setUserCompilationNotes($notes);
        $this->em->flush();

        $this->clientRequestAsCPSUser($user, 'GET', '/api/' . APIController::CURRENT_API_VERSION . '/user/' . $pratica->getId() . '/notes');

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($notes, $this->client->getResponse()->getContent());
    }

    /**
     * @test
     */
    public function testCannotPostNotesIfPraticaIsNotMine()
    {
        $user = $this->createCPSUser(true);
        $user2 = $this->createCPSUser(true);
        $pratica = $this->createPratica($user2);
        $notes = "La marianna la va in campagna @#èòàù€’”ß@ł€ ";

        $this->clientRequestAsCPSUser($user, 'POST', '/api/' . APIController::CURRENT_API_VERSION . '/user/' . $pratica->getId() . '/notes', [
            'ContentType' => 'application/json'
        ], [], [], $notes);

        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(null, $pratica->getUserCompilationNotes());
    }

    /**
     * @test
     */
    public function testAnnotationsAPIIsProtected()
    {
        $user = $this->createCPSUser(true);
        $pratica = $this->createPratica($user);
        $notes = "La marianna la va in campagna @#èòàù€’”ß@ł€ ";
        $this->client->request('POST', '/api/' . APIController::CURRENT_API_VERSION . '/user/' . $pratica->getId() . '/notes', [
            'ContentType' => 'application/json'
        ], [], [], $notes);

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function testGetServizi()
    {
        $servizio1 = $this->createServizioWithAssociatedErogatori([]);
        $servizio2 = $this->createServizioWithAssociatedErogatori([]);

        $expectedResponse = [
            (object)[
                'name' => $servizio1->getName(),
                'slug' => $servizio1->getSlug(),
            ],
            (object)[
                'name' => $servizio2->getName(),
                'slug' => $servizio2->getSlug(),
            ],
        ];

        $this->client->request('GET', '/api/' . APIController::CURRENT_API_VERSION . '/services');
        $response = json_decode($this->client->getResponse()->getContent(), false);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($expectedResponse, $response);
    }

}
