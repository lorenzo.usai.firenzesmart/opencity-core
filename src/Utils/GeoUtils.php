<?php

namespace App\Utils;

use Exception;
use GeoJson\BoundingBox;
use PHPCoord\CoordinateReferenceSystem\Geographic2D;
use PHPCoord\Exception\UnknownCoordinateReferenceSystemException;
use PHPCoord\GeographicPoint;
use PHPCoord\UnitOfMeasure\Angle\Degree;

class GeoUtils
{

  /**
   * @throws UnknownCoordinateReferenceSystemException
   */
  public static function getClusterDistance(array $boundingBox): float
  {

    //ST_MakeEnvelope(13.374481201171877, 42.00950942549379, 13.84483337402344, 42.264098158855845, 4326);
    $crs = Geographic2D::fromSRID(Geographic2D::EPSG_WGS_84);
    $from = GeographicPoint::create(
      new Degree($boundingBox[1]),
      new Degree($boundingBox[0]),
      null,
      $crs
    );

    // Per calcolare la lunghezza del lato minore ricavo l'angolo in basso a dx del bounding box
    $to = GeographicPoint::create(
      new Degree($boundingBox[1]),
      new Degree($boundingBox[3]),
      null,
      $crs
    );

    // In questo modo riesco a calcolare la distanza tra i due punti che compongono il bounding box (lato minore) in metri
    $distance = $from->calculateDistance($to)->getValue();

    return $distance / 10;
  }

  /**
   * @param $boundingBoxCoords
   * @return BoundingBox
   */
  public static function createBoundingBox($boundingBoxCoords): BoundingBox
  {
    $isOver = false;
    $boundingBox = new BoundingBox($boundingBoxCoords);

    // Verifico che le coordinate passate non siano oltre i limiti della bounding box (si genera un errore php per max execution time)
    // Invece di restituire un 400 provo a correggere con la migliore bounding box possibile
    if (!(-180 <= $boundingBoxCoords[0] && $boundingBoxCoords[0] <= 180)) {
      $isOver = true;
      $boundingBoxCoords[0] = -180;
    }

    if (!(-90 <= $boundingBoxCoords[1] && $boundingBoxCoords[1] <= 90)) {
      $isOver = true;
      $boundingBoxCoords[1] = -90;
    }

    if (!(-180 <= $boundingBoxCoords[2] && $boundingBoxCoords[2] <= 180)) {
      $isOver = true;
      $boundingBoxCoords[2] = 180;
    }

    if (!(-90 <= $boundingBoxCoords[3] && $boundingBoxCoords[3] <= 90)) {
      $isOver = true;
      $boundingBoxCoords[3] = 90;
    }

    if ($isOver) {
      $boundingBox = new BoundingBox($boundingBoxCoords);
    }

    return $boundingBox;
  }



}
