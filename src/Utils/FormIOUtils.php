<?php

namespace App\Utils;

class FormIOUtils
{
  public static function simplifyFlattenedKey(string $key): string
  {
    $parts = explode('.', $key);
    $path = [];
    foreach ($parts as $p) {
      if ($p !== 'data' && !ctype_digit($p)) {
        $path [] = $p;
      }
    }
    return implode('.', $path);
  }
}
