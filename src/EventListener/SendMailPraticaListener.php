<?php

namespace App\EventListener;

use App\Event\PraticaOnChangeStatusEvent;
use App\Services\MailerService;

class SendMailPraticaListener
{

  private MailerService $mailer;

  private string $defaultSender;

  public function __construct(MailerService $mailer, $defaultSender)
  {
    $this->mailer = $mailer;
    $this->defaultSender = $defaultSender;
  }

  public function onStatusChange(PraticaOnChangeStatusEvent $event)
  {
    $pratica = $event->getPratica();
    $this->mailer->dispatchMailForPratica($pratica, $this->defaultSender);
  }

}
