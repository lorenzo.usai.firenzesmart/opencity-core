<?php

namespace App\EventListener;

use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Services\CPSUserProvider;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class CompleteProfileListener
{

  /**
   * @var Router
   */
  private Router $router;

  /**
   * @var TokenStorageInterface
   */
  private TokenStorageInterface $tokenStorage;

  /**
   * @var CPSUserProvider
   */
  private CPSUserProvider $userProvider;

  private $passwordLifeTime;

  private array $ignoreListenerRoutes = [
    'attachment_upload',
    'attachment_upload_finalize'
  ];

  /**
   * CompleteProfileListener constructor.
   *
   * @param Router $router
   * @param TokenStorageInterface $tokenStorage
   * @param CPSUserProvider $userProvider
   */
  public function __construct(Router $router, TokenStorageInterface $tokenStorage, CPSUserProvider $userProvider, $passwordLifeTime)
  {
    $this->router = $router;
    $this->tokenStorage = $tokenStorage;
    $this->userProvider = $userProvider;
    $this->passwordLifeTime = $passwordLifeTime;
  }

  public function onKernelRequest(RequestEvent $event)
  {

    $request = $event->getRequest();
    if (strpos($request->getRequestUri(), '/api/') !== false) {
      return;
    }

    $user = $this->getUser();
    if ($user instanceof CPSUser && !$user->isAnonymous()) {
      $currentRoute = $event->getRequest()->get('_route');
      if (in_array($currentRoute, $this->ignoreListenerRoutes)){
        return;
      }
      $currentRouteParams = $event->getRequest()->get('_route_params');
      $currentRouteQuery = $event->getRequest()->query->all();

      if (!$this->userProvider->userHasEnoughData($user)
        && $currentRoute !== ''
        && $currentRoute !== 'user_profile'
        && $currentRoute !== 'terms_accept'
      ) {

        $redirectParameters = [
          'r' => $event->getRequest()->getUri()
        ];

        $redirectUrl = $this->router->generate('user_profile', $redirectParameters);
        $event->setResponse(new RedirectResponse($redirectUrl));
      }
    } elseif ($user instanceof OperatoreUser) {

      // Redirect al security_change_password se lastChangePassword è più vecchio della data odierna - $passwordLifeTime
      $currentRoute = $event->getRequest()->get('_route');
      $currentRouteParams = $event->getRequest()->get('_route_params');
      $currentRouteQuery = $event->getRequest()->query->all();
      $needsToChangePassword = $user->getLastChangePassword()->getTimestamp() < strtotime('-' . $this->passwordLifeTime . ' day') && !$user->isSystemUser();

      if (($user->getLastChangePassword() == null || $needsToChangePassword) && $currentRoute !== 'security_change_password') {
        $redirectParameters['r'] = $currentRoute;
        if ($currentRouteParams) {
          $redirectParameters['p'] = serialize($currentRouteParams);
        }
        if ($currentRouteParams) {
          $redirectParameters['q'] = serialize($currentRouteQuery);
        }

        $redirectUrl = $this->router->generate('security_change_password', $redirectParameters);
        $event->setResponse(new RedirectResponse($redirectUrl));
      }

    }

  }

  protected function getUser()
  {
    if (null === $token = $this->tokenStorage->getToken()) {
      return null;
    }

    if (!is_object($user = $token->getUser())) {
      return null;
    }

    return $user;
  }
}

