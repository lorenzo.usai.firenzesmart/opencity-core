<?php

namespace App\EventListener;

use App\Entity\CPSUser;
use App\Services\TermsAcceptanceCheckerService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class TermsAcceptListener
 */
class TermsAcceptListener
{

  private RouterInterface $router;

  private TokenStorageInterface $tokenStorage;

  private TermsAcceptanceCheckerService $termsAcceptanceChecker;

  private array $ignoreListenerRoutes = [
    'attachment_upload',
    'attachment_upload_finalize'
  ];

  /**
   * TermsAcceptListener constructor.
   * @param RouterInterface $router
   * @param TokenStorageInterface $tokenStorage
   * @param TermsAcceptanceCheckerService $termsAcceptanceChecker
   */
  public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage, TermsAcceptanceCheckerService $termsAcceptanceChecker)
  {
    $this->router = $router;
    $this->tokenStorage = $tokenStorage;
    $this->termsAcceptanceChecker = $termsAcceptanceChecker;
  }

  /**
   * @param GetResponseEvent $event
   */
  public function onKernelRequest(GetResponseEvent $event)
  {

    $request = $event->getRequest();
    if (strpos($request->getRequestUri(), '/api/') !== false) {
      return;
    }

    $user = $this->getUser();
    if ($user instanceof CPSUser && !$user->isAnonymous()) {

      $currentRoute = $event->getRequest()->get('_route');
      if (in_array($currentRoute, $this->ignoreListenerRoutes)){
        return;
      }
      if (!$this->termsAcceptanceChecker->checkIfUserHasAcceptedMandatoryTerms($user)
        && !empty($currentRoute)
        && $currentRoute !== 'terms_accept'
      ) {

        $redirectParameters = [
          'r' => $event->getRequest()->getUri()
        ];

        $redirectUrl = $this->router->generate('terms_accept', $redirectParameters);
        $event->setResponse(new RedirectResponse($redirectUrl));
      }
    }
  }

  protected function getUser()
  {
    if (null === $token = $this->tokenStorage->getToken()) {
      return null;
    }

    if (!is_object($user = $token->getUser())) {
      return null;
    }

    return $user;
  }
}
