<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

class MetaPagedList
{
  /**
   * @Serializer\Type("int")
   * @OA\Property(description="Total number of objects")
   */
  private int $count = 0;


  /**
   * @var array
   * @Serializer\Type("array<string, string>")
   * @OA\Property(description="Query parameter for current url")
   *
   */
  private array $parameter = [];


  public function getCount(): int
  {
    return $this->count;
  }

  public function setCount(int $count): void
  {
    $this->count = $count;
  }

  public function getParameter(): array
  {
    return $this->parameter;
  }

  public function setParameter($parameter): void
  {
    $this->parameter = $parameter;
  }

  public function jsonSerialize(): array
  {
    return get_object_vars($this);
  }
}
