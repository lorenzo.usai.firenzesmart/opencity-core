<?php

namespace App\Model\Payment;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class PaymentTransaction
{
  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment uuid")
   * @Groups({"read", "kafka"})
   */
  private $transactionId;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Created at date time")
   * @Groups({"read", "kafka"})
   */
  private $paidAt = null;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Created at date time")
   * @Groups({"read", "kafka"})
   */
  private $expireAt;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $amount;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $currency = 'EUR';

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $noticeCode;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $iud;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $iuv;

  /**
   * @var PaymentSplit[]
   * @OA\Property(type="array", @OA\Items(ref=@Model(type=PaymentSplit::class, groups={"read"})))
   * @Serializer\Type("array")
   * @Groups({"read", "kafka"})
   */
  private $split = [];

  /**
   * @return mixed
   */
  public function getTransactionId()
  {
    return $this->transactionId;
  }

  /**
   * @param mixed $transactionId
   */
  public function setTransactionId($transactionId): void
  {
    $this->transactionId = $transactionId;
  }

  /**
   * @return DateTime
   */
  public function getPaidAt(): DateTime
  {
    return $this->paidAt;
  }

  /**
   * @param DateTime $paidAt
   */
  public function setPaidAt(DateTime $paidAt): void
  {
    $this->paidAt = $paidAt;
  }

  /**
   * @return DateTime
   */
  public function getExpireAt(): DateTime
  {
    return $this->expireAt;
  }

  /**
   * @param DateTime $expireAt
   */
  public function setExpireAt(DateTime $expireAt): void
  {
    $this->expireAt = $expireAt;
  }

  /**
   * @return mixed
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * @param mixed $amount
   */
  public function setAmount($amount): void
  {
    $this->amount = $amount;
  }

  /**
   * @return mixed
   */
  public function getCurrency()
  {
    return $this->currency;
  }

  /**
   * @param mixed $currency
   */
  public function setCurrency($currency): void
  {
    $this->currency = $currency;
  }

  /**
   * @return mixed
   */
  public function getNoticeCode()
  {
    return $this->noticeCode;
  }

  /**
   * @param mixed $noticeCode
   */
  public function setNoticeCode($noticeCode): void
  {
    $this->noticeCode = $noticeCode;
  }

  /**
   * @return mixed
   */
  public function getIud()
  {
    return $this->iud;
  }

  /**
   * @param mixed $iud
   */
  public function setIud($iud): void
  {
    $this->iud = $iud;
  }

  /**
   * @return mixed
   */
  public function getIuv()
  {
    return $this->iuv;
  }

  /**
   * @param mixed $iuv
   */
  public function setIuv($iuv): void
  {
    $this->iuv = $iuv;
  }

  /**
   * @return PaymentSplit[]
   */
  public function getSplit(): array
  {
    return $this->split;
  }

  /**
   * @param PaymentSplit[] $split
   */
  public function setSplit(array $split): void
  {
    $this->split = $split;
  }

}
