<?php

namespace App\Model\Payment;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class PaymentSplit
{
  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment split code")
   * @Groups({"read", "kafka"})
   */
  private $code;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment split amount")
   * @Groups({"read", "kafka"})
   */
  private $amount;

  /**
   * @var array
   * @OA\Property(description="Payment split meta")
   * @Serializer\Type("array")
   * @Groups({"read", "kafka"})
   */
  private $meta;

  /**
   * @return mixed
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * @param mixed $code
   */
  public function setCode($code): void
  {
    $this->code = $code;
  }

  /**
   * @return mixed
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * @param mixed $amount
   */
  public function setAmount($amount): void
  {
    $this->amount = $amount;
  }

  /**
   * @return array
   */
  public function getMeta(): array
  {
    return $this->meta;
  }

  /**
   * @param array $meta
   */
  public function setMeta(array $meta): void
  {
    $this->meta = $meta;
  }

}
