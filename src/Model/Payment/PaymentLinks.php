<?php

namespace App\Model\Payment;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class PaymentLinks
{
  /**
   * @OA\Property(description="Online payment begin")
   * @Groups({"kafka"})
   */
  private $onlinePaymentBegin;

  /**
   * @OA\Property(description="Online payment landing")
   * @Groups({"kafka"})
   */
  private $onlinePaymentLanding;

  /**
   * @OA\Property(description="Offline payment")
   * @Groups({"kafka"})
   */
  private $offlinePayment = null;

  /**
   * @OA\Property(description="Receipt")
   * @Groups({"kafka"})
   */
  private $receipt = null;

  /**
   * @OA\Property(description="Notify url")
   * @Groups({"kafka"})
   */
  private $notify = null;

  /**
   * @OA\Property(description="Update")
   * @Groups({"kafka"})
   */
  private $update = null;

  /**
   * @return mixed
   */
  public function getOnlinePaymentBegin()
  {
    return $this->onlinePaymentBegin;
  }

  /**
   * @param mixed $onlinePaymentBegin
   */
  public function setOnlinePaymentBegin($onlinePaymentBegin): void
  {
    $this->onlinePaymentBegin = $onlinePaymentBegin;
  }

  /**
   * @return mixed
   */
  public function getOnlinePaymentLanding()
  {
    return $this->onlinePaymentLanding;
  }

  /**
   * @param mixed $onlinePaymentLanding
   */
  public function setOnlinePaymentLanding($onlinePaymentLanding): void
  {
    $this->onlinePaymentLanding = $onlinePaymentLanding;
  }

  /**
   * @return null
   */
  public function getOfflinePayment()
  {
    return $this->offlinePayment;
  }

  /**
   * @param null $offlinePayment
   */
  public function setOfflinePayment($offlinePayment): void
  {
    $this->offlinePayment = $offlinePayment;
  }

  /**
   * @return null
   */
  public function getReceipt()
  {
    return $this->receipt;
  }

  /**
   * @param null $receipt
   */
  public function setReceipt($receipt): void
  {
    $this->receipt = $receipt;
  }

  /**
   * @return null
   */
  public function getNotify()
  {
    return $this->notify;
  }

  /**
   * @param null $notify
   */
  public function setNotify($notify): void
  {
    $this->notify = $notify;
  }

  /**
   * @return null
   */
  public function getUpdate()
  {
    return $this->update;
  }

  /**
   * @param null $update
   */
  public function setUpdate($update): void
  {
    $this->update = $update;
  }

}
