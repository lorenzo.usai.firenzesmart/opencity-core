<?php


namespace App\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class Stamp implements \JsonSerializable
{

  public const PHASE_REQUEST = 'request';
  public const PHASE_RELEASE = 'release';

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Is the reason why the stamp must be paid", example="Secretariat fees")
   * @Groups({"read", "write", "service"})
   */
  private string $reason;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Stamp idetifier", example="secretariat_fees")
   * @Groups({"read", "write", "service"})
   */
  private string $identifier;

  /**
   * @Serializer\Type("float")
   * @OA\Property(description="The amount of the stamp, may not be 0", example="2.00")
   * @Groups({"read", "write", "service"})
   */
  private float $amount;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Identifies the stage at which the user fills in the stamps.<ul><li>'request' => the stamp is filled in when filling in the application</li><li>'release' => the stamp is filled in when application is accepted</li></ul>", enum={"request", "release"})
   * @Groups({"read", "write", "service"})
   */
  private string $phase;

  /**
   * @Serializer\Type("float")
   * @OA\Property(description="The number of the stamp", example="012000204012548")
   * @Groups({"write"})
   */
  private string $number = '';

  /**
   * @Serializer\Type("DateTime")
   * @OA\Property(description="The emission date of the stamp", type="string", format="date-time")
   * @Groups({"write"})
   */
  private ?DateTime $emittedAt = null;

  private ?bool $paid = false;

  /**
   * @return string
   */
  public function getReason(): string
  {
    return $this->reason;
  }

  /**
   * @param string $reason
   */
  public function setReason(string $reason): void
  {
    $this->reason = $reason;

  }

  /**
   * @return string
   */
  public function getIdentifier(): string
  {
    return $this->identifier;
  }

  /**
   * @param string|null $identifier
   */
  public function setIdentifier(string $identifier): void
  {
    $this->identifier = $identifier;
  }

  /**
   * @return float
   */
  public function getAmount(): float
  {
    return $this->amount;
  }

  /**
   * @param float $amount
   */
  public function setAmount(float $amount): void
  {
    $this->amount = $amount;
  }

  /**
   * @return string
   */
  public function getPhase(): string
  {
    return $this->phase;
  }

  /**
   * @param string $phase
   */
  public function setPhase(string $phase): void
  {
    $this->phase = $phase;
  }

  /**
   * @return string
   */
  public function getNumber(): string
  {
    return $this->number;
  }

  /**
   * @param string $number
   */
  public function setNumber(string $number): void
  {
    $this->number = $number;
  }

  /**
   * @return ?DateTime
   */
  public function getEmittedAt(): ?DateTime
  {
    return $this->emittedAt;
  }

  /**
   * @param DateTime|null $emittedAt
   */
  public function setEmittedAt(?DateTime $emittedAt): void
  {
    $this->emittedAt = $emittedAt;
  }

  /**
   * @return bool
   */
  public function isPaid(): bool
  {
    return (!empty($this->number) && !empty($this->emittedAt));
  }

  /**
   * @param bool $paid
   */
  public function setPaid(?bool $paid): void
  {
    $this->paid = $paid;
  }


  public function jsonSerialize(): array
  {
    return [
      'reason' => $this->reason,
      'identifier' => $this->identifier,
      'amount' => $this->amount,
      'phase' => $this->phase,
      'number' => $this->number,
      'emitted_at' => $this->emittedAt ? $this->emittedAt->format(\DateTimeInterface::W3C) : null,
      'paid' => $this->isPaid()
    ];
  }

  public static function fromArray($data = []): Stamp
  {
    $stamp = new self();
    $stamp->setReason($data['reason'] ?? '');
    $stamp->setIdentifier($data['identifier'] ?? '');
    $stamp->setAmount($data['amount'] ?? 0);
    $stamp->setPhase($data['phase'] ?? self::PHASE_REQUEST);
    $stamp->setNumber($data['number'] ?? '');
    $stamp->setEmittedAt(isset($data['emitted_at']) && $data['emitted_at'] ? new DateTime($data['emitted_at']) : null);
    $stamp->setPaid($data['paid'] ?? false);

    return $stamp;
  }

}
