<?php


namespace App\Model\Tenant;


class ThemeOptions implements \JsonSerializable
{

  const DEFAULT_THEME = 'blu';

  const THEMES = [
    'Acqua' => 'acqua',
    'Acquamarina' => 'acquamarina',
    'Amalfi' => 'amalfi',
    'Amaranto' => 'amaranto',
    'Apss' => 'apss',
    'Blu' => 'blu',
    'Cagliari' => 'cagliari',
    'Cenerentola' => 'cenerentola',
    'Default' => 'default',
    'Elegance' => 'elegance',
    'Firenze' => 'firenze',
    'Mare' => 'mare',
    'Mediterraneo' => 'mediterraneo',
    'Roma' => 'roma',
    'Rustico' => 'rustico',
    'Trento' => 'trento',
    'Turquoise' => 'turquoise',
    'Verdone' => 'verdone',
    'Warmred' => 'warmred'
  ];

  private string $identifier = self::DEFAULT_THEME;

  private bool $lightTopHeader = false;

  private bool $lightCenterHeader = false;

  private bool $lightNavbarHeader = false;

  /**
   * @return string
   */
  public function getIdentifier(): string
  {
    return $this->identifier;
  }

  /**
   * @param string $identifier
   */
  public function setIdentifier(string $identifier): void
  {
    $this->identifier = $identifier;
  }

  /**
   * @return bool
   */
  public function isLightTopHeader(): bool
  {
    return $this->lightTopHeader;
  }

  /**
   * @param bool $lightTopHeader
   */
  public function setLightTopHeader(bool $lightTopHeader): void
  {
    $this->lightTopHeader = $lightTopHeader;
  }

  /**
   * @return bool
   */
  public function isLightCenterHeader(): bool
  {
    return $this->lightCenterHeader;
  }

  /**
   * @param bool $lightCenterHeader
   */
  public function setLightCenterHeader(bool $lightCenterHeader): void
  {
    $this->lightCenterHeader = $lightCenterHeader;
  }

  /**
   * @return bool
   */
  public function isLightNavbarHeader(): bool
  {
    return $this->lightNavbarHeader;
  }

  /**
   * @param bool $lightNavbarHeader
   */
  public function setLightNavbarHeader(bool $lightNavbarHeader): void
  {
    $this->lightNavbarHeader = $lightNavbarHeader;
  }


  /**
   * @return array
   */
  public function jsonSerialize()
  {
    return [
      'identifier' => $this->identifier,
      'light_top_header' => $this->lightTopHeader,
      'light_center_header' => $this->lightCenterHeader,
      'light_navbar_header' => $this->lightNavbarHeader,
    ];
  }


  public static function fromArray($data = []): ThemeOptions
  {
    $themeOptions = new ThemeOptions();
    $themeOptions->setIdentifier($data['identifier'] ?? 'default');
    $themeOptions->setLightTopHeader($data['light_top_header'] ?? false);
    $themeOptions->setLightCenterHeader($data['light_center_header'] ?? false);
    $themeOptions->setLightNavbarHeader($data['light_navbar_header'] ?? false);

    return $themeOptions;
  }

}
