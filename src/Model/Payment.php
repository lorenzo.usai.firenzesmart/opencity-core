<?php

namespace App\Model;

use App\Model\Payment\PaymentLinks;
use App\Model\Payment\PaymentTransaction;
use App\Model\Payment\Payer;
use \DateTime;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Payment
{

  const STATUS_CREATION_PENDING = 'CREATION_PENDING';
  const STATUS_CREATION_FAILED = 'CREATION_FAILED';
  const STATUS_PAYMENT_PENDING = 'PAYMENT_PENDING';
  const STATUS_PAYMENT_STARTED = 'PAYMENT_STARTED';
  const STATUS_PAYMENT_CONFIRMED = 'PAYMENT_CONFIRMED';
  const STATUS_PAYMENT_FAILED = 'PAYMENT_FAILED';
  const STATUS_NOTIFICATION_PENDING = 'NOTIFICATION_PENDING';
  const STATUS_COMPLETE = 'COMPLETE';
  const STATUS_EXPIRED = 'EXPIRED';

  const PAYMENT_STATUSES = [
    self::STATUS_CREATION_PENDING,
    self::STATUS_CREATION_FAILED,
    self::STATUS_PAYMENT_PENDING,
    self::STATUS_PAYMENT_STARTED,
    self::STATUS_PAYMENT_CONFIRMED,
    self::STATUS_PAYMENT_FAILED,
    self::STATUS_NOTIFICATION_PENDING,
    self::STATUS_COMPLETE,
    self::STATUS_EXPIRED,
  ];

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment uuid")
   * @Groups({"read", "kafka"})
   */
  private $id;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $userId;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $type = 'PAGOPA';

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $tenantId;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $serviceId;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Created at date time")
   * @Groups({"read", "kafka"})
   */
  private DateTime $createdAt;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Created at date time", format="date-time")
   * @Groups({"read", "kafka"})
   */
  private DateTime $updatedAt;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $status;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment user id")
   * @Groups({"read", "kafka"})
   */
  private $reason;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment read id")
   * @Groups({"read", "kafka"})
   */
  private $remoteId;

  /**
   * @var PaymentTransaction
   * @OA\Property(type="object", ref=@Model(type=PaymentTransaction::class, groups={"read"}))
   * @Groups({"read", "kafka"})
   */
  private $payment;

  /**
   * @var PaymentLinks
   * @OA\Property(type="object", ref=@Model(type=PaymentLinks::class, groups={"kafka"}))
   * @Groups({"kafka"})
   */
  private $links;

  /**
   * @var Payer
   * @OA\Property(type="object", ref=@Model(type=Payer::class, groups={"kafka"}))
   * @Groups({"read", "kafka"})
   */
  private $payer;


  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getUserId()
  {
    return $this->userId;
  }

  /**
   * @param mixed $userId
   */
  public function setUserId($userId): void
  {
    $this->userId = $userId;
  }

  /**
   * @return mixed
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * @param mixed $type
   */
  public function setType($type): void
  {
    $this->type = $type;
  }

  /**
   * @return mixed
   */
  public function getTenantId()
  {
    return $this->tenantId;
  }

  /**
   * @param mixed $tenantId
   */
  public function setTenantId($tenantId): void
  {
    $this->tenantId = $tenantId;
  }

  /**
   * @return mixed
   */
  public function getServiceId()
  {
    return $this->serviceId;
  }

  /**
   * @param mixed $serviceId
   */
  public function setServiceId($serviceId): void
  {
    $this->serviceId = $serviceId;
  }

  /**
   * @return DateTime
   */
  public function getCreatedAt(): DateTime
  {
    return $this->createdAt;
  }

  /**
   * @param DateTime $createdAt
   */
  public function setCreatedAt(DateTime $createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  /**
   * @return DateTime
   */
  public function getUpdatedAt(): DateTime
  {
    return $this->updatedAt;
  }

  /**
   * @param DateTime $updatedAt
   */
  public function setUpdatedAt(DateTime $updatedAt): void
  {
    $this->updatedAt = $updatedAt;
  }

  /**
   * @return mixed
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status): void
  {
    $this->status = $status;
  }

  /**
   * @return mixed
   */
  public function getReason()
  {
    return $this->reason;
  }

  /**
   * @param mixed $reason
   */
  public function setReason($reason): void
  {
    $this->reason = $reason;
  }

  /**
   * @return mixed
   */
  public function getRemoteId()
  {
    return $this->remoteId;
  }

  /**
   * @param mixed $remoteId
   */
  public function setRemoteId($remoteId): void
  {
    $this->remoteId = $remoteId;
  }

  /**
   * @return PaymentTransaction
   */
  public function getPayment(): PaymentTransaction
  {
    return $this->payment;
  }

  /**
   * @param PaymentTransaction $payment
   */
  public function setPayment(PaymentTransaction $payment): void
  {
    $this->payment = $payment;
  }

  /**
   * @return PaymentLinks
   */
  public function getLinks(): PaymentLinks
  {
    return $this->links;
  }

  /**
   * @param PaymentLinks $links
   */
  public function setLinks(PaymentLinks $links): void
  {
    $this->links = $links;
  }

  /**
   * @return Payer
   */
  public function getPayer(): Payer
  {
    return $this->payer;
  }

  /**
   * @param Payer $payer
   */
  public function setPayer(Payer $payer): void
  {
    $this->payer = $payer;
  }



}
