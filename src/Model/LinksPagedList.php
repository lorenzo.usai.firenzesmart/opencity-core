<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

class LinksPagedList
{
  /**
   * @var string|null
   * @Serializer\Type("string")
   * @OA\Property(description="link to current page", format="url", example="/applications?offset=1&limit=10")
   */
  private ?string $self = null;

  /**
   * @var string|null
   * @Serializer\Type("string")
   * @OA\Property(description="Link to previous page", type="string", format="url", example="/applications?offset=0&limit=10")
   */
  private ?string $prev = null;

  /**
   * @var string|null
   * @Serializer\Type("string")
   * @OA\Property(description="Link to next page", type="string", format="url", example="/applications?offset=2&limit=10")
   */
  private ?string $next = null;


  public function getSelf(): ?string
  {
    return $this->self;
  }

  public function setSelf(?string $self): void
  {
    $this->self = $self;
  }

  public function getPrev(): ?string
  {
    return $this->prev;
  }

  public function setPrev(?string $prev): void
  {
    $this->prev = $prev;
  }

  public function getNext(): ?string
  {
    return $this->next;
  }

  public function setNext(?string $next): void
  {
    $this->next = $next;
  }


  /**
   * @return array
   */
  public function jsonSerialize(): array
  {
    return get_object_vars($this);
  }
}
