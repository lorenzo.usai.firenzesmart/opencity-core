<?php

namespace App\Event;

use App\Entity\Pratica;
use Symfony\Contracts\EventDispatcher\Event;

class GenerateApplicationReceiptEvent extends Event
{

  const NAME = 'ocsdc.application.generate_receipt';

  private Pratica $application;

  public function __construct(Pratica $application)
  {
    $this->application = $application;
  }

  public function getApplication(): Pratica
  {
    return $this->application;
  }



}
