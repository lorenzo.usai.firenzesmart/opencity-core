<?php

namespace App\Controller\Ui\Backend;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ProviderFactoryInterface;
use App\Security\Voters\OAuthClientVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WebhookController
 * @Route("/admin/oauth")
 */
class OAuthClientController extends AbstractController
{
  private ProviderFactoryInterface $providerFactory;

  public function __construct(ProviderFactoryInterface $providerFactory)
  {
    $this->providerFactory = $providerFactory;
  }

  /**
   * @Route("/", name="admin_oauth_client_index")
   * @Method("GET")
   */
  public function indexAction(): Response
  {
    $this->denyAccessUnlessGranted(OAuthClientVoter::READ);

    $currentProvider = $this->providerFactory->instanceProvider();
    return $this->render('Admin/indexOAuthClient.html.twig', [
      'user' => $this->getUser(),
      'configuration' => $this->providerFactory->getConfiguration(),
      'provider' => $currentProvider,
      'provider_name' => $currentProvider instanceof ConfigurableProviderInterface ? $currentProvider->getName() : '',
    ]);
  }
}
