<?php

namespace App\Controller\Ui\Backend;

use App\Dto\User;
use App\Entity\AdminUser;
use App\Form\AdminUserType;
use App\Services\InstanceService;
use App\Services\Manager\UserManager;
use App\Utils\StringUtils;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/users/administrators")
 */
class AdministratorUsersController extends AbstractController
{
  private EntityManagerInterface $entityManager;
  private InstanceService $instanceService;
  private UserManager $userManager;
  private TranslatorInterface $translator;

  public function __construct(EntityManagerInterface $entityManager, InstanceService $instanceService, UserManager $userManager, TranslatorInterface $translator)
  {
    $this->entityManager = $entityManager;
    $this->instanceService = $instanceService;
    $this->userManager = $userManager;
    $this->translator = $translator;
  }


  /**
   * @Route("/", name="admin_administrator_users_index", methods={"GET"})
   */
  public function index(EntityManagerInterface $entityManager): Response
  {
    $adminUsers = $entityManager
      ->getRepository(AdminUser::class)
      ->findAll();

    return $this->render('Admin/indexAdministrators.html.twig', [
      'admin_users' => $adminUsers,
    ]);
  }

  /**
   * @Route("/new", name="admin_administrator_users_new", methods={"GET", "POST"})
   */
  public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
  {
    $user = $this->getUser();
    $adminUser = new AdminUser();
    $form = $this->createForm(AdminUserType::class, $adminUser);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $ente = $this->instanceService->getCurrentInstance();
      $adminUser->setEnte($ente);
      $adminUser->setPassword(
        $passwordEncoder->encodePassword(
          $adminUser,
          StringUtils::randomPassword()
        )
      );

      try {
        $this->userManager->save($adminUser);
        $this->userManager->resetPassword($adminUser, $user);
        return $this->redirectToRoute('admin_administrator_users_edit', ['id'=> $adminUser->getId()]);
      } catch (UniqueConstraintViolationException $e) {
        $this->addFlash('warning', $this->translator->trans('admin.error_duplicate_username'));
      }
    }

    return $this->render('Admin/editAdmin.html.twig', [
      'user' => $user,
      'admin_user' => $adminUser,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/{id}/edit", name="admin_administrator_users_edit", methods={"GET", "POST"})
   */
  public function edit(Request $request, AdminUser $adminUser): Response
  {
    $user = $this->getUser();
    $form = $this->createForm(AdminUserType::class, $adminUser);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      try {
        $this->userManager->save($adminUser);
        return $this->redirectToRoute('admin_administrator_users_edit', ['id'=> $adminUser->getId()]);
      } catch (UniqueConstraintViolationException $e) {
        $this->addFlash('warning', $this->translator->trans('admin.error_duplicate_username'));
      }
    }

    return $this->render('Admin/editAdmin.html.twig', [
      'user' => $user,
      'admin_user' => $adminUser,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/{id}/delete", name="admin_administrator_users_delete", methods={"GET"})
   */
  public function delete(Request $request, AdminUser $adminUser): Response
  {
    try {
      $this->userManager->remove($adminUser);
      $this->addFlash('feedback', $this->translator->trans('admin.delete_operator_notify'));
    } catch (ForeignKeyConstraintViolationException $exception) {
      $this->addFlash('warning', $this->translator->trans('admin.error_delete_operator_notify'));
    }

    return $this->redirectToRoute('admin_administrator_users_index', [], Response::HTTP_SEE_OTHER);
  }
}
