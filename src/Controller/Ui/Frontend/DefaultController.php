<?php

namespace App\Controller\Ui\Frontend;

use App\Controller\Ui\Frontend\ServiziController;
use App\InstancesProvider;
use App\Services\InstanceService;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use App\Entity\TerminiUtilizzo;
use App\Logging\LogConstants;
use App\Security\AbstractAuthenticator;
use App\Security\LogoutSuccessHandler;
use App\Services\Manager\JwksManager;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Services\KeyLoader\KeyLoaderInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{

  /** @var LoggerInterface */
  private LoggerInterface $logger;

  /** @var TranslatorInterface */
  private TranslatorInterface $translator;

  /** @var InstanceService */
  private InstanceService $instanceService;
  /**
   * DefaultController constructor.
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   * @param InstanceService $instanceService
   */
  public function __construct(TranslatorInterface $translator, LoggerInterface $logger, InstanceService $instanceService)
  {
    $this->logger = $logger;
    $this->translator = $translator;
    $this->instanceService = $instanceService;
  }


  /**
   * @param Request $request
   * @return Response|null
   */
  public function commonAction(Request $request): ?Response
  {
    if ($this->instanceService->hasInstance()) {
      return $this->forward(ServiziController::class . '::serviziAction');
    } else {
      $host = $request->server->get('HTTP_HOST');
      $enti = [];
      foreach (InstancesProvider::factory()->getInstances() as $identifier => $instance) {
        $indentifierParts = explode('/', $identifier);
        if ($indentifierParts[0] === $host) {
          $enti[] = [
            'name' => $instance['name'] ?? ucwords(str_replace('-', ' ', $instance['identifier'])),
            'slug' => $indentifierParts[1]
          ];
        }
      }

      if (count($enti) === 1) {
        return $this->redirect('/'. $enti[0]['slug']. '/'. $request->getDefaultLocale());
      }

      return $this->render(
        'Default/common.html.twig',
        ['enti' => $enti]
      );
    }
  }

  /**
   * @Route("/", name="instance_home")
   * @param Request $request
   * @return Response
   */
  public function indexAction(Request $request): Response
  {
    $ente = $this->instanceService->getCurrentInstance();

    if ($ente->isSearchAndCatalogueEnabled()) {
      $homepage = $this->getParameter('home_page');
      $routes = $this->get('router')->getRouteCollection();
      if ($routes->get($homepage)) {
        return $this->forward($routes->get($homepage)->getDefaults()['_controller']);
      }
      return $this->forward(ServiziController::class . '::serviziAction');
    }
    $servicesI18n = $this->translator->trans('services', [], null, $request->getLocale());
    return $this->redirect($ente->getServicesUrl($servicesI18n));
  }

  /**
   * @Route("/privacy", name="privacy")
   */
  public function privacyAction()
  {
  }

  /**
   * @Route("/terms_accept/", name="terms_accept")
   * @param Request $request
   *
   * @return Response
   */
  public function termsAcceptAction(Request $request)
  {
    $repo = $this->getDoctrine()->getRepository('App\Entity\TerminiUtilizzo');
    $terms = $repo->findAll();
    $form = $this->setupTermsAcceptanceForm($terms)->handleRequest($request);
    $user = $this->getUser();

    if ($form->isSubmitted()) {
      $redirectUrl = $request->query->has('r') ? $request->query->get('r') : $this->generateUrl('home');
      return $this->markTermsAcceptedForUser($user, $redirectUrl, $terms);
    }

    return $this->render( 'Default/termsAccept.html.twig', [
      'form' => $form->createView(),
      'terms' => $terms,
      'user' => $user,
    ]);
  }

  /**
   * @param TerminiUtilizzo[] $terms
   * @return FormInterface
   */
  private function setupTermsAcceptanceForm(array $terms): FormInterface
  {
    $data = array();
    $formBuilder = $this->createFormBuilder($data);
    foreach ($terms as $term) {
      $formBuilder->add(
        (string)$term->getId(),
        CheckboxType::class,
        array(
          'label' => $this->translator->trans('terms_do_il_consenso'),
          'required' => true,
        )
      );
    }
    $formBuilder->add('save', SubmitType::class, array('label' => $this->translator->trans('salva')));
    return $formBuilder->getForm();
  }

  /**
   * @param CPSUser $user
   * @param $redirectUrl
   * @param array $terms
   * @return RedirectResponse
   */
  private function markTermsAcceptedForUser(CPSUser $user, $redirectUrl, array $terms): RedirectResponse {
    $manager = $this->getDoctrine()->getManager();
    foreach ($terms as $term) {
      $user->addTermsAcceptance($term);
    }

    try {
      $manager->persist($user);
      $manager->flush();
      $this->logger->info(LogConstants::USER_HAS_ACCEPTED_TERMS, ['userid' => $user->getId()]);
    } catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return $this->redirect($redirectUrl);
  }

  /**
   * @Route("/elenco-segnalazioni/", name="inefficiencies")
   * @return Response|null
   */
  public function listInefficienciesAction()
  {
    $ente = $this->instanceService->getCurrentInstance();
    return $this->render('Servizi/inefficiency.html.twig', [
      'user' => $this->getUser(),
      'privacy_url' => $ente->getPrivacyInfo() ?? ''
    ]);
  }

  /**
   * @return JsonResponse
   */
  public function statusAction(): JsonResponse
  {
    return new JsonResponse(['status' => 'ok'], Response::HTTP_OK);
  }

  /**
   * @param JwksManager $jwksManager
   * @return JsonResponse
   */
  public function jwksAction(JwksManager $jwksManager): JsonResponse
  {
    return new JsonResponse($jwksManager->generateJwks(), 200);
  }

}
