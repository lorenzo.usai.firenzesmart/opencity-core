<?php

namespace App\Controller\Ui\Frontend;

use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Security\Voters\ApplicationVoter;
use App\Services\ModuloPdfBuilderService;
use Gotenberg\Exceptions\GotenbergApiErroed;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


/**
 * Class PraticheAnonimeController
 *
 * @package App\Controller
 * @Route("/print")
 */
class PrintController extends AbstractController
{

  private ModuloPdfBuilderService $moduloPdfBuilderService;
  private LoggerInterface $logger;

  /**
   * PrintController constructor.
   * @param ModuloPdfBuilderService $moduloPdfBuilderService
   * @param LoggerInterface $logger
   */
  public function __construct(ModuloPdfBuilderService $moduloPdfBuilderService, LoggerInterface $logger)
  {
    $this->moduloPdfBuilderService = $moduloPdfBuilderService;
    $this->logger = $logger;
  }


  /**
   * @Route("/pratica/{pratica}", name="print_pratiche")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   * @param Request $request
   * @param Pratica $pratica
   *
   * @return Response
   * @throws LoaderError|RuntimeError|SyntaxError
   */
  public function printPraticaAction(Request $request, Pratica $pratica): Response
  {

    $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $pratica, "User can not read application {$pratica->getId()}");
    $showProtocolNumber = $request->get('protocol', false);

    try {

      if ($request->query->has('preview')) {
        return new Response($this->moduloPdfBuilderService->renderForPratica($pratica, $showProtocolNumber));
      }

      $fileContent = $this->moduloPdfBuilderService->generateApplicationPdf($pratica, $showProtocolNumber);
      $filename = time() . '.pdf';
      $response = new Response($fileContent);
      $disposition = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        $filename
      );
      $response->headers->set('Content-Disposition', $disposition);
      return $response;
    } catch (GotenbergApiErroed $e) {
      $this->logger->error('print_pratiche_show', [
        'application_id' => $pratica->getId(),
        'exception' => $e->getMessage(),
      ]);
    }

    return new Response("Unable to print the requested application", Response::HTTP_INTERNAL_SERVER_ERROR);
  }

  /**
   * @Route("/application/{pratica}", name="print_application")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   * @param Request $request
   * @param Pratica $pratica
   *
   * @return Response
   * @throws LoaderError|RuntimeError|SyntaxError
   */
  public function printApplicationAction(Request $request, Pratica $pratica): Response
  {
    $showProtocolNumber = $request->get('protocol', false);
    return new Response($this->moduloPdfBuilderService->renderForPratica($pratica, $showProtocolNumber));
  }

  /**
   * @Route("/service/{service}", name="print_service")
   * @ParamConverter("service", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $service
   *
   * @return Response
   */
  public function printServiceAction(Request $request, Servizio $service): Response
  {

    $pratica = $this->createApplication($service);

    $form = $this->createForm('App\Form\FormIO\FormIORenderType', $pratica);

    return $this->render('Print/printService.html.twig', [
      'formserver_url' => $this->getParameter('formserver_admin_url'),
      'form' => $form->createView(),
      'pratica' => $pratica
    ]);
  }

  /**
   * @Route("/service/{service}/pdf", name="print_service_pdf")
   * @ParamConverter("service", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $service
   *
   * @return Response
   * @throws GotenbergApiErroed
   */
  public function printServicePdfAction(Request $request, Servizio $service): Response
  {

    $fileContent = $this->moduloPdfBuilderService->generateServicePdfUsingGotemberg($service);
    // Provide a name for your file with extension
    $filename = time() . '.pdf';
    // Return a response with a specific content
    $response = new Response($fileContent);
    // Create the disposition of the file
    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $filename
    );
    // Set the content disposition
    $response->headers->set('Content-Disposition', $disposition);
    // Dispatch request
    return $response;

  }

  /**
   * @Route("/service/{service}/preview", name="preview_service")
   * @ParamConverter("service", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $service
   *
   * @return Response
   */
  public function previewServiceAction(Request $request, Servizio $service): Response
  {

    $pratica = $this->createApplication($service);

    $form = $this->createForm('App\Form\FormIO\FormIORenderType', $pratica);

    return $this->render('Print/previewService.html.twig', [
      'formserver_url' => $this->getParameter('formserver_admin_url'),
      'form' => $form->createView(),
      'pratica' => $pratica
    ]);
  }

  /**
   * @param Servizio $service
   * @return Pratica
   */
  private function createApplication(Servizio $service): Pratica
  {
    $praticaClassName = $service->getPraticaFCQN();
    $pratica = new $praticaClassName();
    if (!$pratica instanceof Pratica) {
      throw new \RuntimeException("Wrong Pratica FCQN for servizio {$service->getName()}");
    }
    $pratica
      ->setServizio($service)
      ->setStatus(Pratica::STATUS_DRAFT);

    return $pratica;
  }


}
