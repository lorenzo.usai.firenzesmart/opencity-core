<?php

namespace App\Controller\Ui\Frontend;

use App\Entity\Servizio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController extends AbstractController
{
  /**
   * @Route("/pratiche/{servizio}/new", name="redirect_pratiche_new")
   * @Route("/pratiche-anonime/{servizio}/new", name="redirect_pratiche_anonime_new")
   * @ParamConverter("servizio", class=Servizio::class, options={"mapping": {"servizio": "slug"}})
   * @param Request $request
   * @param Servizio $servizio
   * @return Response
   */
  public function redirectPraticheNewAction(Request $request, Servizio $servizio): Response
  {
    return $this->redirectToRoute('service_access', ['servizio' => $servizio->getSlug()], Response::HTTP_MOVED_PERMANENTLY);
  }
}
