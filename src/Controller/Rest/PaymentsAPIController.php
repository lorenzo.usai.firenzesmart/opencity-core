<?php

namespace App\Controller\Rest;

use App\Entity\PraticaRepository;
use App\Entity\ServizioRepository;
use App\Security\Voters\ServiceVoter;
use Nelmio\ApiDocBundle\Annotation\Security;
use App\Entity\CPSUser;
use App\Model\Payment;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Security\Voters\ApplicationVoter;
use App\Services\PaymentService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class PaymentsAPIController
 * @package App\Controller
 * @Route("/payments")
 */
class PaymentsAPIController extends AbstractFOSRestController
{
  const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;

  private PaymentService $paymentService;

  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param PaymentService $paymentService
   * @param LoggerInterface $logger
   */
  public function __construct(
    EntityManagerInterface $entityManager,
    PaymentService $paymentService,
    LoggerInterface $logger
  ) {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->paymentService = $paymentService;
  }

  /**
   * List all payments
   * @Rest\Get("", name="payments_api_list")
   *
   *
   * @OA\Parameter(
   *      name="remote_id",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Uuid of remote object of payments"
   *  )
   *
   * @OA\Parameter(
   *      name="user_id",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Uuid of the user"
   *  )
   *
   * @OA\Parameter(
   *      name="status",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Status of the payment, allowed values are; creation_pending, creation_failed, payment_pending, payment_started, payment_confirmed, payment_failed, complete, expired"
   *  )
   *
   * @OA\Parameter(
   *      name="created_at[after|before|strictly_after|strictly_before]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Created at filter allows to filter a collection by date or intervals.<br>Syntax: <code>?created_at[after|before|strictly_after|strictly_before]=value</code></p><p>Value can take this supported format <code>yyyy-mm-dd</code> or <code>yyyy-mm-ddTHH:ii:ssP</code></p><p>The after and before filters will filter including the value whereas strictly_after and strictly_before will filter excluding the value.</p>
   *  You can filter payments by date with the following query: <code>/payments?created_at[after]=2023-07-14</code>, it will return all payments where created_at is superior or equal to 2023-07-14.
   *  You can filter payments by range with the following query: <code>/payments?created_at[after]=2023-07-01&created_at[before]=2023-07-30</code>, it will return all payments where created_at is beetwen 2023-07-01 and 2023-07-30."
   * )
   *
   * @OA\Parameter(
   *      name="service_id",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Uuid of related service"
   *  )
   *
   * @OA\Parameter(
   *      name="payer_tax_identification_number",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Payer tax identification number"
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of payments",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=Payment::class, groups={"read"}))
   *     )
   * )
   *
   * @OA\Tag(name="payments")
   * @param Request $request
   * @return View
   */
  public function getPaymentsAction(Request $request): View
  {
    $this->denyAccessUnlessGranted(['ROLE_CPS_USER', 'ROLE_OPERATORE', 'ROLE_ADMIN']);

    $criteria = [];

    $user = $this->getUser();
    $userId = $request->get('user_id', false);
    if ($user instanceof CPSUser) {
      $userId = $user->getId();
    }
    if ($userId) {
      if (!Uuid::isValid($userId)) {
        return $this->view(["user_id parameters $userId is not a valid uuid"], Response::HTTP_BAD_REQUEST);
      }
      if ($userId != $user->getId()) {
        $result = $this->entityManager->getRepository('App\Entity\CpsUser')->find($userId);
        if ($result === null) {
          return $this->view(["User not found"], Response::HTTP_NOT_FOUND);
        }
      }
      $criteria['user_id'] = $userId;
    }

    $serviceId = $request->get('service_id', false);
    if ($serviceId) {
      if (!Uuid::isValid($serviceId)) {
        return $this->view(["service_id parameters $serviceId is not a valid uuid"], Response::HTTP_BAD_REQUEST);
      }
      $service = $this->getDoctrine()->getRepository(Servizio::class)->find($serviceId);
      if (!$service instanceof Servizio){
        return $this->view(["service by service_id parameters $serviceId not found"], Response::HTTP_BAD_REQUEST);
      }

      if ($user instanceof OperatoreUser) {
        try {
          $this->denyAccessUnlessGranted(ServiceVoter::IMPORT_PAYMENTS, $service);
        }catch (AccessDeniedException $e){
          return $this->view(["You are not allowed to view payments by selected service_id"], Response::HTTP_FORBIDDEN);
        }
      }
      $criteria['service_id'] = $serviceId;
    }

    $payerTaxNumber = $request->get('payer_tax_identification_number', false);
    if ($payerTaxNumber){
      if ($user instanceof CPSUser && $user->getCodiceFiscale() !== $payerTaxNumber) {
        return $this->view(["You are not allowed to view payments by selected payer_tax_identification_number"], Response::HTTP_FORBIDDEN);
      }
      $criteria['payer_tax_identification_number'] = strtoupper($payerTaxNumber);
    }

    $remoteId = $request->get('remote_id', false);
    if ($remoteId) {
      $repository = $this->entityManager->getRepository('App\Entity\Pratica');
      /** @var Pratica $result */
      $result = $repository->find($remoteId);
      if ($result === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $criteria['remote_id'] = $remoteId;
      $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $result);
    }

    $status = $request->get('status', false);
    if ($status) {
      if (!in_array(strtoupper($status), Payment::PAYMENT_STATUSES)) {
        return $this->view(["$status is not a valid status"], Response::HTTP_BAD_REQUEST);
      }

      $criteria['status'] = strtoupper($status);
    }

    $createdAtParameter = $request->get('created_at', false);
    if ($createdAtParameter) {
      $dateFormat = 'Y-m-d';
      foreach ($createdAtParameter as $v) {
        $date = DateTime::createFromFormat($dateFormat, $v) ?: DateTime::createFromFormat(DATE_ATOM, $v);
        if (!$date || ($date->format($dateFormat) !== $v && $date->format(DATE_ATOM) !== $v)) {
          return $this->view(
            ["Parameter created_at must be in on of these formats: yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"],
            Response::HTTP_BAD_REQUEST
          );
        }
      }
      $criteria['created_at'] = $createdAtParameter;
    }

    try {
      $data = $this->paymentService->getPayments($criteria);
      if (empty($data)) {
        return $this->view(["Payment data not found"], Response::HTTP_NOT_FOUND);
      }

      $result = [];
      $result['meta']['parameter'] = $criteria;
      $result['meta']['count'] = count($data);
      $result['data'] = $data;

      return $this->view($result, Response::HTTP_OK);
    } catch (\Exception $exception) {
      return $this->view($exception->getMessage(), Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * Retrieve a payment
   * @Rest\Get("/{id}", name="payments_api_get")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve an Application",
   *     @OA\JsonContent(
   *       ref=@Model(type=Payment::class, groups={"read"})
   *     ),
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   *
   * @OA\Tag(name="payments")
   * @param $id
   * @return View
   */
  public function getPaymentAction($id): View
  {
    $this->denyAccessUnlessGranted(['ROLE_CPS_USER', 'ROLE_OPERATORE', 'ROLE_ADMIN']);

    $user = $this->getUser();

    try {

      $data = $this->paymentService->getPayment($id);
      if (empty($data)) {
        return $this->view(["Payment not found"], Response::HTTP_NOT_FOUND);
      }

      return $this->view(reset($data), Response::HTTP_OK);
    } catch (\Exception $exception) {
      return $this->view($exception->getMessage(), Response::HTTP_BAD_REQUEST);
    }
  }

}
