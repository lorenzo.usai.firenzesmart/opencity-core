<?php

namespace App\Controller\Rest;

use App\Entity\Job;
use App\Services\InstanceService;
use App\Services\JobService;
use App\Utils\FormUtils;
use App\Utils\UploadedBase64File;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;

/**
 * Class JobsAPIController
 * @package App\Controller
 * @Route("/jobs")
 */
class JobsAPIController extends AbstractFOSRestController
{
  const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;

  private InstanceService $instanceService;
  private JobService $jobService;

  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param InstanceService $instanceService
   * @param JobService $jobService
   * @param LoggerInterface $logger
   */
  public function __construct(EntityManagerInterface $entityManager, InstanceService $instanceService, JobService $jobService, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->instanceService = $instanceService;
    $this->jobService = $jobService;
    $this->logger = $logger;
  }

  /**
   * List all Jobs
   * @Rest\Get("", name="jobs_api_list")
   *
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of jobs",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=Job::class, groups={"read"}))
   *     )
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="jobs")
   * @param Request $request
   * @return View
   */
  public function getJobsAction(Request $request)
  {
    $this->denyAccessUnlessGranted(['ROLE_OPERATORE', 'ROLE_ADMIN']);
    $result = $this->entityManager->getRepository('App\Entity\Job')->findBy([], ['createdAt' => 'asc']);
    return $this->view($result, Response::HTTP_OK);
  }

  /**
   * Retrieve a Job by id
   * @Rest\Get("/{id}", name="job_api_get")
   *
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a Job",
   *     @Model(type=Job::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Job not found"
   * )
   * @OA\Tag(name="jobs")
   *
   * @param Request $request
   * @param string $id
   * @return View
   */
  public function getJobAction(Request $request, $id)
  {
    $this->denyAccessUnlessGranted(['ROLE_OPERATORE', 'ROLE_ADMIN']);
    try {
      $repository = $this->getDoctrine()->getRepository('App\Entity\Job');
      $result = $repository->find($id);
    } catch (\Exception $e) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    if ($result === null) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    return $this->view($result, Response::HTTP_OK);
  }

  /**
   * Create a Job
   * @Rest\Post(name="jobs_api_post")
   *
   * @Security(name="Bearer")
   *
   *
   * @OA\RequestBody(
   *     description="The job to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Job::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a Job"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="jobs")
   *
   * @param Request $request
   * @return View
   */
  public function postJobAction(Request $request)
  {
    $this->denyAccessUnlessGranted(['ROLE_OPERATORE', 'ROLE_ADMIN']);

    $item = new Job();
    $form = $this->createForm('App\Form\Api\JobApiType', $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $item
        ->setStatus(Job::STATUS_PENDING)
        ->setTenant($this->instanceService->getCurrentInstance());


      if ($request->request->get('attachment')) {
        $attachment = $request->request->get('attachment');
        if (filter_var($attachment['file'], FILTER_VALIDATE_URL)) {
          $base64Content = base64_encode(file_get_contents($attachment['file']));
          $file = new UploadedBase64File($base64Content, $attachment['mime_type']);
        } else {
          $file = new UploadedBase64File($attachment['file'], $attachment['mime_type']);
        }

        $item->setFile($file);
        $item->setOriginalFilename($attachment['name']);
      }

      $this->entityManager->persist($item);
      $this->entityManager->flush();

      $this->jobService->processJobAsync($item);

    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view($item, Response::HTTP_CREATED);
  }

  /**
   * Edit full Job
   * @Rest\Put("/{id}", name="jobs_api_put")
   *
   * @Security(name="Bearer")
   *
   *
   * @OA\RequestBody(
   *     description="The job to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Job::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full Job"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="jobs")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function putJobAction($id, Request $request)
  {

    $this->denyAccessUnlessGranted(['ROLE_OPERATORE', 'ROLE_ADMIN']);

    $repository = $this->getDoctrine()->getRepository('App\Entity\Job');
    $item = $repository->find($id);

    if (!$item) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm('App\Form\Api\JobApiType', $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'put_validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (\Exception $e) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Modified Successfully"], Response::HTTP_OK);
  }

  /**
   * Patch a Job
   * @Rest\Patch("/{id}", name="jobs_api_patch")
   *
   * @Security(name="Bearer")
   *
   *
   * @OA\RequestBody(
   *     description="The job to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Job::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch a Job"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="jobs")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function patchJobAction($id, Request $request)
  {
    $this->denyAccessUnlessGranted(['ROLE_OPERATORE', 'ROLE_ADMIN']);

    $repository = $this->getDoctrine()->getRepository('App\Entity\Job');
    $item = $repository->find($id);

    if (!$item) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm('App\Form\Api\JobApiType', $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (\Exception $e) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_OK);
  }

  /**
   * Delete a Job
   * @Rest\Delete("/{id}", name="job_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="jobs")
   *
   * @param $id
   * @return View
   */
  public function deleteJobAction($id)
  {
    $this->denyAccessUnlessGranted(['ROLE_OPERATORE', 'ROLE_ADMIN']);
    $item = $this->getDoctrine()->getRepository('App\Entity\Job')->find($id);
    if ($item) {
      $this->entityManager->remove($item);
      $this->entityManager->flush();
    }
    return $this->view(null, Response::HTTP_NO_CONTENT);
  }

  /**
   * @param Request $request
   * @param FormInterface $form
   */
  private function processForm(Request $request, FormInterface $form)
  {
    $data = json_decode($request->getContent(), true);

    // Todo: find better way
    if (isset($data['args']) && count($data['args']) > 0) {
      $data['args'] = \json_encode($data['args']);
    }

    $clearMissing = $request->getMethod() != 'PATCH';
    $form->submit($data, $clearMissing);
  }
}
