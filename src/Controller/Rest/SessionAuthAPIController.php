<?php


namespace App\Controller\Rest;

use App\Services\CPSUserProvider;
use Nelmio\ApiDocBundle\Annotation\Security;
use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SessionAuthAPIController extends AbstractFOSRestController
{

  private JWTTokenManagerInterface $JWTTokenManager;

  private CPSUserProvider $cpsUserProvider;

  /**
   * SessionAuthAPIController constructor.
   * @param JWTTokenManagerInterface $JWTTokenManager
   * @param CPSUserProvider $cpsUserProvider
   */
  public function __construct(
    JWTTokenManagerInterface $JWTTokenManager,
    CPSUserProvider $cpsUserProvider
  )
  {
    $this->JWTTokenManager = $JWTTokenManager;
    $this->cpsUserProvider = $cpsUserProvider;
  }

  /**
   * Retrieve a session auth token
   * @Rest\Options("/session-auth", name="user_session_auth_token_options")
   *
   * @OA\Response(
   *     response=200,
   *     description="Empty response",
   * )
   *
   * @OA\Tag(name="session-auth")
   *
   * @return View
   */
  public function optionsSessionAuthToken(): View
  {
    return $this->view([]);
  }

  /**
   * Retrieve a session auth token
   * @Rest\Get("/session-auth", name="user_session_auth_token_get")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve logged in user auth Token",
   *     @OA\JsonContent(
   *       type="object",
   *       @OA\Property(property="token", type="string"),
   *     ),
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Unauthorized"
   * )
   * @OA\Tag(name="session-auth")
   *
   * @return View
   */
  public function getSessionAuthToken()
  {
    //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    $user = $this->getUser();
    if ($user instanceof User) {
      return $this->view(['token' => $this->JWTTokenManager->create($user)]);
    }

    return $this->view([
      'title' => 'Unauthorized',
      'detail' => 'You are not logged in',
      'status' => Response::HTTP_UNAUTHORIZED,
    ], Response::HTTP_UNAUTHORIZED);
  }

  /**
   * Retrieve a session auth token
   * @Rest\Post("/session-auth", name="user_session_auth_token_create")
   *
   * @OA\Response(
   *     response=200,
   *     description="Create an anonymous user and retreive auth token",
   *     @OA\JsonContent(
   *       type="object",
   *       @OA\Property(property="token", type="string"),
   *     ),
   * )
   *
   * @OA\Tag(name="session-auth")
   *
   * @return View
   */
  public function createAnonymousSessionAuthToken()
  {
    $user = $this->cpsUserProvider->createAnonymousUser(true);

    return $this->view(['token' => $this->JWTTokenManager->create($user)]);
  }

}
