<?php

namespace App\Controller\Rest;

use App\BackOffice\CalendarsBackOffice;
use App\Entity\Calendar;
use App\Entity\CPSUser;
use App\Entity\Meeting;
use App\Entity\User;
use App\Security\Voters\BackofficeVoter;
use App\Security\Voters\MeetingVoter;
use App\Services\InstanceService;
use App\Services\Manager\UserManager;
use App\Services\MeetingService;
use App\Utils\ApiUtils;
use App\Utils\FormUtils;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\NotFoundProblem;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use DateTime;
use App\Form\MeetingType;

/**
 * Class MeetingsAPIController
 * @property EntityManagerInterface em
 * @property InstanceService is
 * @package App\Controller
 * @Route("/meetings")
 */
class MeetingsAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $em;

  private TranslatorInterface $translator;

  private LoggerInterface $logger;
  private MeetingService $meetingService;


  /**
   * @param TranslatorInterface $translator
   * @param EntityManagerInterface $em
   * @param LoggerInterface $logger
   * @param MeetingService $meetingService
   */
  public function __construct(TranslatorInterface $translator, EntityManagerInterface $em, LoggerInterface $logger, MeetingService $meetingService)
  {
    $this->translator = $translator;
    $this->em = $em;
    $this->logger = $logger;
    $this->meetingService = $meetingService;
  }


  /**
   * List all Meetings
   * @Rest\Get("", name="meetings_api_list")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of meetings",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=Meeting::class, groups={"read"}))
   *     )
   * )
   *
   * @OA\Parameter(
   *       name="version",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Version of Api, default 1. From version 2 have paginated API"
   *  )
   *
   * @OA\Parameter(
   *      name="created_at[after|before|strictly_after|strictly_before]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="created_at filter allows to filter a collection by date or intervals.<br>Syntax: <code>?created_at[after|before|strictly_after|strictly_before]=value</code></p><p>Value can take this supported format <code>yyyy-mm-dd</code> or <code>yyyy-mm-ddTHH:ii:ssP</code></p><p>The after and before filters will filter including the value, strictly_after and strictly_before will filter excluding the value.</p>
   *  You can filter meetings by date with the following query: <code>/meetings?created_at[after]=2023-07-14</code>, it will return all meetings where created_at is superior or equal to 2023-07-14.
   *  You can filter meetings by range with the following query: <code>/meetings?created_at[after]=2023-07-01&created_at[before]=2023-07-30</code>, it will return all meetings where created_at is beetwen 2023-07-01 and 2023-07-30."
   * )
   *
   * @OA\Parameter(
   *      name="updated_at[after|before|strictly_after|strictly_before]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="updated_at filter allows to filter a collection by date or intervals.<br>Syntax: <code>?updated_at[after|before|strictly_after|strictly_before]=value</code></p><p>Value can take this supported format <code>yyyy-mm-dd</code> or <code>yyyy-mm-ddTHH:ii:ssP</code></p><p>The after and before filters will filter including the value, strictly_after and strictly_before will filter excluding the value.</p>
   *  You can filter meetings by date with the following query: <code>/meetings?updated_at[after]=2023-07-14</code>, it will return all meetings where created_at is superior or equal to 2023-07-14.
   *  You can filter meetings by range with the following query: <code>/meetings?updated_at[after]=2023-07-01&updated_at[before]=2023-07-30</code>, it will return all meetings where updated_at is beetwen 2023-07-01 and 2023-07-30."
   * )
   *
   * @OA\Parameter(
   *       name="order",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Order field. Default createdAt"
   *  )
   *
   * @OA\Parameter(
   *       name="sort",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Sorting criteria of the order field. Default ASC"
   *  )
   *
   * @OA\Parameter(
   *       name="status",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Status code of meetings, allowed values are: <code>0</code> - Pending, <code>1</code> - Approved, <code>2</code> - Refused, <code>3</code> - Missed, <code>4</code> - Done, <code>5</code> - Cancelled, <code>6</code> - Draft"
   * )
   *
   * @OA\Parameter(
   *        name="from_time[after|before|strictly_after|strictly_before]",
   *        in="query",
   *        @OA\Schema(
   *            type="string"
   *        ),
   *        required=false,
   *        description="from_time filter allows to filter a collection by date or intervals.<br>Syntax: <code>?from_time[after|before|strictly_after|strictly_before]=value</code></p><p>Value can take this supported format <code>yyyy-mm-dd</code> or <code>yyyy-mm-ddTHH:ii:ssP</code></p><p>The after and before filters will filter including the value, strictly_after and strictly_before will filter excluding the value.</p>
   *    You can filter meetings by date with the following query: <code>/meetings?from_time[after]=2023-07-14</code>, it will return all meetings where created_at is superior or equal to 2023-07-14.
   *    You can filter meetings by range with the following query: <code>/meetings?from_time[after]=2023-07-01&from_time[before]=2023-07-30</code>, it will return all meetings where from_time is beetwen 2023-07-01 and 2023-07-30."
   *   )
   *
   * @OA\Parameter(
   *        name="to_time[after|before|strictly_after|strictly_before]",
   *        in="query",
   *        @OA\Schema(
   *            type="string"
   *        ),
   *        required=false,
   *        description="to_time filter allows to filter a collection by date or intervals.<br>Syntax: <code>?to_time[after|before|strictly_after|strictly_before]=value</code></p><p>Value can take this supported format <code>yyyy-mm-dd</code> or <code>yyyy-mm-ddTHH:ii:ssP</code></p><p>The after and before filters will filter including the value, strictly_after and strictly_before will filter excluding the value.</p>
   *    You can filter meetings by date with the following query: <code>/meetings?to_time[after]=2023-07-14</code>, it will return all meetings where created_at is superior or equal to 2023-07-14.
   *    You can filter meetings by range with the following query: <code>/meetings?to_time[after]=2023-07-01&to_time[before]=2023-07-30</code>, it will return all meetings where to_time is beetwen 2023-07-01 and 2023-07-30."
   *   )
   *
   * @OA\Parameter(
   *       name="offset",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Offset of the query"
   *  )
   *
   * @OA\Parameter(
   *       name="limit",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Limit of the query"
   *  )
   *
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="meetings")
   * @throws ApiProblemException
   */
  public function getMeetingsAction(Request $request): View
  {

    /** @var User $user */
    $user = $this->getUser();
    $statusParameter = $request->get('status', false);
    $createdAtParameter = $request->get('created_at', false);
    $updatedAtParameter = $request->get('updated_at', false);
    $offset = $request->query->getInt('offset', 0);
    $limit = $request->query->getInt('limit', 10);
    $version = $request->query->getInt('version', 1);
    $orderParameter = $request->get('order', false);
    $sortParameter = $request->get('sort', false);
    $fromTimeParameter = $request->get('from_time', false);
    $toTimeParameter = $request->get('to_time', false);
    $queryParameters = [];


    $dateFormat = 'Y-m-d';

    if ($limit > 100) {
      throw new ApiProblemException(
        new BadRequestProblem('Limit parameter is too high, maximum value is 99')
      );
    }

    $queryParameters = ['offset' => $offset, 'limit' => $limit, 'version' => $version];

    if ($orderParameter) {
      $queryParameters['order'] = $orderParameter;
    }
    if ($sortParameter) {
      $queryParameters['sort'] = $sortParameter;
    }

    if ($statusParameter) {
      $queryParameters['status'] = $statusParameter;
    }

    // Validate createdAt parameter
    if ($createdAtParameter) {
      try {
        $queryParameters =  array_merge($queryParameters, ApiUtils::validateDateTimeRangeParameter($createdAtParameter, 'created_at'));
      } catch (\Exception $e) {
        throw new ApiProblemException(
          new BadRequestProblem($e->getMessage())
        );
      }
    }

    // Validate updateAt parameter
    if ($updatedAtParameter) {
      try {
        $queryParameters =  array_merge($queryParameters, ApiUtils::validateDateTimeRangeParameter($updatedAtParameter, 'updated_at'));
      } catch (\Exception $e) {
        throw new ApiProblemException(
          new BadRequestProblem($e->getMessage())
        );
      }
    }

    // Validate from_time parameter
    if ($fromTimeParameter) {
      try {
        $queryParameters =  array_merge($queryParameters, ApiUtils::validateDateTimeRangeParameter($fromTimeParameter, 'from_time'));
      } catch (\Exception $e) {
        throw new ApiProblemException(
          new BadRequestProblem($e->getMessage())
        );
      }
    }

    // Validate to_time parameter
    if ($toTimeParameter) {
      try {
        $queryParameters =  array_merge($queryParameters, ApiUtils::validateDateTimeRangeParameter($toTimeParameter, 'to_time'));
      } catch (\Exception $e) {
        throw new ApiProblemException(
          new BadRequestProblem($e->getMessage())
        );
      }
    }


    $result = [];
    $result['meta']['parameter'] = $queryParameters;
    $repoApplications = $this->em->getRepository(Meeting::class);

    try {
      $count = $repoApplications->getMeetingsUser($queryParameters, true);
    } catch (NoResultException $e) {
      $count = 0;
    } catch (NonUniqueResultException $e) {
      return $this->view($e->getMessage(), Response::HTTP_I_AM_A_TEAPOT);
    }

    $result['links']['prev'] = null;
    $result['links']['next'] = null;
    $result ['data'] = [];
    $result['meta']['count'] = $count;
    $result['links']['self'] = $this->generateUrl(
      'meetings_api_list',
      $queryParameters,
      UrlGeneratorInterface::ABSOLUTE_URL
    );


    if ($offset !== 0) {
      $queryParameters['offset'] = $offset - $limit;
      $result['links']['prev'] = $this->generateUrl(
        'meetings_api_list',
        $queryParameters,
        UrlGeneratorInterface::ABSOLUTE_URL
      );
    }

    $order = $orderParameter ?: "createdAt";
    $sort = $sortParameter ?: "ASC";

    if ($offset + $limit < $count) {
      $queryParameters['offset'] = $offset + $limit;
      $result['links']['next'] = $this->generateUrl(
        'meetings_api_list',
        $queryParameters,
        UrlGeneratorInterface::ABSOLUTE_URL
      );
    }

    $context = new Context();
    $context->setGroups(['read']);

    // Dalla versione 2 restituisco API paginata
    // Todo: spostare in controller specifico
    if ($version > 1) {
      try {
        $applications = $repoApplications->getMeetingsUser($queryParameters, false, $order, $sort, $offset, $limit, $user);
        foreach ($applications as $s) {
          $result ['data'][] = $s;
        }
        return $this->view($result, Response::HTTP_OK)->setContext($context);
      } catch (\Exception $exception) {
        return $this->view($exception->getMessage(), Response::HTTP_BAD_REQUEST);
      }
    }

    try {
      $applications = $repoApplications->getMeetingsUser($queryParameters, false, $order, $sort, $offset, $limit, $user);
      $result = $applications;
      return $this->view($result, Response::HTTP_OK)->setContext($context);
    } catch (\Exception $exception) {
      return $this->view($exception->getMessage(), Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * Retrieve a Meeting
   * @Rest\Get("/{id}", name="meeting_api_get")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a Meeting",
   *     @Model(type=Meeting::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Meeting not found"
   * )
   * @OA\Tag(name="meetings")
   *
   * @param Request $request
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function getMeetingAction(Request $request, $id): View
  {

    try {
      $repository = $this->em->getRepository(Meeting::class);
      $meeting = $repository->find($id);
      if (!$meeting instanceof Meeting) {
        throw new ApiProblemException(
          new NotFoundProblem("Meeting {$id} not found")
        );
      }

      $this->denyAccessUnlessGranted(MeetingVoter::VIEW, $meeting);

      $context = new Context();
      $context->setGroups(['read']);
      return $this->view($meeting, Response::HTTP_OK)->setContext($context);

    } catch (ApiProblemException $e) {
      $this->logger->error('meeting_api_get', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('meeting_api_get', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => AbstractApiController::MESSAGE_INTERNAL_SERVER_ERROR]));
    }
  }

  /**
   * Create a Meeting
   * @Rest\Post(name="meetings_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The meeting to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Meeting::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Meeting created"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   * @OA\Tag(name="meetings")
   *
   * @param Request $request
   * @param UserManager $userManager
   * @return View
   */
  public function postMeetingAction(Request $request, UserManager $userManager): View
  {

    $meeting = new Meeting();
    $form = $this->createForm(MeetingType::class, $meeting);
    $this->processForm($request, $form);
    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      if ($this->getUser() instanceof CPSUser) {
        $meeting->setUser($this->getUser());
        $this->checkMeetingStatus($meeting, $request);
      } else {
        if (!$meeting->getFiscalCode() && !$meeting->getUser()) {
          // codice fiscale non fornito
          $user = new CPSUser();
          $user->setEmail($meeting->getEmail() ?: '');
          $user->setEmailContatto($meeting->getEmail() ?: '');
          $user->setNome($meeting->getName() ?: '');
          $user->setCognome('');
          $user->setCodiceFiscale($user->getId() . '-' . time());
          $user->setUsername($user->getId());

          $user->setEnabled(true);

          $userManager->save($user, false);
          $meeting->setUser($user);
        } else if ($meeting->getFiscalCode() && !$meeting->getUser()) {
          $result = $this->em->createQueryBuilder()
            ->select('user.id')
            ->from('App:User', 'user')
            ->where('upper(user.username) = upper(:username)')
            ->setParameter('username', $meeting->getFiscalCode())
            ->getQuery()->getResult();
          if (!empty($result)) {
            $repository = $this->em->getRepository('App\Entity\CPSUser');
            /**
             * @var CPSUser $user
             */
            $user = $repository->find($result[0]['id']);
          } else {
            $user = null;
          }
          if (!$user) {
            $user = new CPSUser();
            $user->setEmail($meeting->getEmail() ?: '');
            $user->setEmailContatto($meeting->getEmail() ?: '');
            $user->setNome($meeting->getName() ?: '');
            $user->setCognome('');
            $user->setCodiceFiscale($meeting->getFiscalCode());
            $user->setUsername($meeting->getFiscalCode());
            $user->setEnabled(true);
            $userManager->save($user, false);
          }
          $meeting->setUser($user);
        }
      }

      // Se ho creato un meeting in bozza aggiungo la scadenza
      if ($meeting->getStatus() === Meeting::STATUS_DRAFT) {
        $meeting->setDraftExpiration(new \DateTime('+' . ($meeting->getCalendar()->getDraftsDuration() ?? Calendar::DEFAULT_DRAFT_DURATION) . 'seconds'));
      }

      $this->meetingService->save($meeting, false);
      $this->em->flush();

      //Todo: da rimuovere, va gestito lato client
      if ($meeting->getUser()->getEmail()) {
        $this->addFlash('feedback', $this->translator->trans('meetings.email.success'));
      }

    } catch (ValidatorException $e) {
      $data = [
        'type' => 'error',
        'title' => $e->getMessage(),
        'description' => $e->getMessage(),
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($meeting, Response::HTTP_CREATED)->setContext($context);
  }

  /**
   * Edit full Meeting
   * @Rest\Put("/{id}", name="meetings_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The meeting to edit",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Meeting::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Edit full Meeting"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="meetings")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function putMeetingAction($id, Request $request): View
  {

    $repository = $this->em->getRepository(Meeting::class);
    $meeting = $repository->find($id);
    if (!$meeting) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(MeetingVoter::EDIT, $meeting);
    $oldMeeting = clone $meeting;

    $form = $this->createForm(MeetingType::class, $meeting);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'put_validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {

      if ($this->getUser() instanceof CPSUser) {
        $this->checkMeetingStatus($meeting, $request);
      } else {
        $dateChanged = $oldMeeting->getFromTime() != $meeting->getFromTime();
        // Auto approve meeting when changing date
        if ($dateChanged && $oldMeeting->getStatus() == Meeting::STATUS_PENDING) {
          $meeting->setStatus(Meeting::STATUS_APPROVED);
        }
      }

      $this->meetingService->save($meeting);

      // Todo: da rimuovere e gestire tramite client
      if ($meeting->getUser()->getEmail()) {
        $this->addFlash('feedback', $this->translator->trans('meetings.email.success'));
      }

    } catch (ValidatorException $e) {
      $data = [
        'type' => 'error',
        'title' => $e->getMessage(),
        'description' => $e->getMessage(),
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Modified Successfully"], Response::HTTP_NO_CONTENT);
  }

  /**
   * Patch a Meeting
   * @Rest\Patch("/{id}", name="meetings_api_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The meeting to patch",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Meeting::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Patch a Meeting"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="meetings")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function patchMeetingAction($id, Request $request): View
  {

    $repository = $this->getDoctrine()->getRepository('App\Entity\Meeting');
    $meeting = $repository->find($id);
    $oldMeeting = clone $meeting;

    if (!$meeting) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(MeetingVoter::EDIT, $meeting);

    $form = $this->createForm('App\Form\MeetingType', $meeting);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {

      if ($this->getUser() instanceof CPSUser) {
        $this->checkMeetingStatus($meeting, $request);
      } else {
        $dateChanged = $oldMeeting->getFromTime() != $meeting->getFromTime();
        // Auto approve meeting when changing date
        if ($dateChanged && $oldMeeting->getStatus() == Meeting::STATUS_PENDING) {
          $meeting->setStatus(Meeting::STATUS_APPROVED);
        }
      }

      $this->meetingService->save($meeting);

      if ($meeting->getUser()->getEmail()) {
        $this->addFlash('feedback', $this->translator->trans('meetings.email.success'));
      }

    } catch (ValidatorException $e) {
      $data = [
        'type' => 'error',
        'title' => $e->getMessage(),
        'description' => $e->getMessage(),
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_NO_CONTENT);
  }

  /**
   * Delete a Meeting
   * @Rest\Delete("/{id}", name="meetings_api_delete")
   *
   * @Security(name="Bearer")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="meetings")
   *
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function deleteAction($id): View
  {
    try {
      $repository = $this->em->getRepository(Meeting::class);
      $meeting = $repository->find($id);
      if (!$meeting instanceof Meeting) {
        throw new ApiProblemException(
          new NotFoundProblem("Meeting {$id} not found")
        );
      }
      $this->denyAccessUnlessGranted(MeetingVoter::DELETE, $meeting);
      $this->em->remove($meeting);
      $this->em->flush();

      $this->addFlash('feedback', $this->translator->trans('meetings.email.success'));
      return $this->view(null, Response::HTTP_NO_CONTENT);

    } catch (ApiProblemException $e) {
      $this->logger->error('meetings_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('meetings_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => AbstractApiController::MESSAGE_INTERNAL_SERVER_ERROR]));
    }
  }

  private function checkMeetingStatus(Meeting $meeting, Request $request): void
  {
    if ($meeting->getStatus() !== Meeting::STATUS_DRAFT && ($meeting->getOpeningHour()->getIsModerated() || $meeting->getCalendar()->getIsModerated())) {
      $meeting->setStatus(Meeting::STATUS_PENDING);
    } else {
      $status = $request->request->getInt('status', null) ?? Meeting::STATUS_APPROVED;
      $meeting->setStatus($status);
    }
  }


  /**
   * @param Request $request
   * @param FormInterface $form
   */
  private function processForm(Request $request, FormInterface $form): void
  {
    $data = json_decode($request->getContent(), true);

    $clearMissing = $request->getMethod() != 'PATCH';
    $form->submit($data, $clearMissing);
  }
}
