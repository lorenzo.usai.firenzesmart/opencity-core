<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\EntityRepository;

/**
 * MeetingRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class MeetingRepository extends EntityRepository
{
  public function countFutureMeetingsByCalendar(Calendar $calendar)
  {
    $date = new DateTime();

    return $this->createQueryBuilder('meeting')
      ->select('count(meeting.id)')
      ->where('meeting.calendar = :calendar')
      ->andWhere('meeting.status = :pending or meeting.status = :approved or meeting.status = :draft')
      ->andWhere('meeting.fromTime >= :now or meeting.toTime >= :now')
      ->setParameter('calendar', $calendar->getId())
      ->setParameter('draft', Meeting::STATUS_DRAFT)
      ->setParameter('pending', Meeting::STATUS_PENDING)
      ->setParameter('approved', Meeting::STATUS_APPROVED)
      ->setParameter('now', $date->format('Y-m-d h:i:s'))
      ->getQuery()->getSingleScalarResult();
  }

  public function countModeratedMeetingsByCalendar(Calendar $calendar)
  {
    $date = new DateTime();

    return $this->createQueryBuilder('meeting')
      ->select('count(meeting.id)')
      ->where('meeting.calendar = :calendar')
      ->andWhere('meeting.status = :pending')
      ->andWhere('meeting.fromTime >= :now or meeting.toTime >= :now')
      ->setParameter('calendar', $calendar->getId())
      ->setParameter('pending', Meeting::STATUS_PENDING)
      ->setParameter('now', $date->format('Y-m-d h:i:s'))
      ->getQuery()->getSingleScalarResult();
  }

  public function getMeetings($parameters = [],$onlyCount = false,$order = 'createdAt', $sort = 'ASC',  $offset = 0, $limit = 10) {
    $qb = $this->createQueryBuilder('meeting')->join('meeting.openingHour', 'openingHour');

    if (isset($parameters['status'])) {
      $qb->where('meeting.status = :status')->setParameter('status', $parameters['status']);
    } else {
      $qb->where('meeting.status != :status')->setParameter('status', Meeting::STATUS_DRAFT);
    }

    if ($onlyCount) {
      $qb->select('COUNT(meeting.id)');
      return $qb->getQuery()->getSingleScalarResult();
    } else {
      $qb
        ->orderBy('meeting.' . $order, $sort)
        ->setFirstResult($offset)
        ->setMaxResults($limit);
    }

    return $qb->getQuery()->execute();
  }

  public function getMeetingsUser($parameters = [], $onlyCount = false, $order = 'createdAt', $sort = 'ASC', $offset = 0, $limit = 10, $user = null)
  {

    $qb = $this->createQueryBuilder('meeting')
      ->leftJoin('meeting.calendar', 'calendar');

    if ($user instanceof CPSUser) {
      $qb
        ->andWhere('meeting.user = :user')
        ->setParameter('user', $user);
    } elseif ($user instanceof OperatoreUser) {
      $qb
        ->andWhere(':user MEMBER OF calendar.moderators or calendar.owner = :user')
        ->setParameter('user', $user);
    }

    if (isset($parameters['status'])) {
      $qb->andWhere('meeting.status = :status')->setParameter('status', $parameters['status']);
    } else {
      $qb->andWhere('meeting.status != :status')->setParameter('status', Meeting::STATUS_DRAFT);
    }

    // after|before|strictly_after|strictly_before
    if (isset($parameters['created_at'])) {
      if (isset($parameters['created_at']['strictly_after'])) {
        $qb->andWhere('meeting.createdAt > :created_at')->setParameter('created_at', $parameters['created_at']['strictly_after']);
      } else if (isset($parameters['created_at']['after'])) {
        $qb->andWhere('meeting.createdAt >= :created_at')->setParameter('created_at', $parameters['created_at']['after']);
      }

      if (isset($parameters['created_at']['strictly_before'])) {
        $qb->andWhere('meeting.createdAt < :created_at')->setParameter('created_at', $parameters['created_at']['strictly_before']);
      } else if (isset($parameters['created_at']['before'])) {
        $qb->andWhere('meeting.createdAt <= :created_at')->setParameter('created_at', $parameters['created_at']['before']);
      }
    }

    if (isset($parameters['updated_at'])) {
      if (isset($parameters['updated_at']['strictly_after'])) {
        $qb->andWhere('meeting.updatedAt > :updated_at')->setParameter('updated_at', $parameters['updated_at']['strictly_after']);
      } else if (isset($parameters['updated_at']['after'])) {
        $qb->andWhere('meeting.updatedAt >= :updated_at')->setParameter('updated_at', $parameters['updated_at']['after']);
      }

      if (isset($parameters['updated_at']['strictly_before'])) {
        $qb->andWhere('meeting.updatedAt < :updated_at')->setParameter('updated_at', $parameters['updated_at']['strictly_before']);
      } else if (isset($parameters['updated_at']['before'])) {
        $qb->andWhere('meeting.updatedAt <= :updated_at')->setParameter('updated_at', $parameters['updated_at']['before']);
      }
    }

    if (isset($parameters['from_time'])) {
      if (isset($parameters['from_time']['strictly_after'])) {
        $qb->andWhere('meeting.fromTime > :from_time')->setParameter('from_time', $parameters['from_time']['strictly_after']);
      } else if (isset($parameters['from_time']['after'])) {
        $qb->andWhere('meeting.fromTime >= :from_time')->setParameter('from_time', $parameters['from_time']['after']);
      }

      if (isset($parameters['from_time']['strictly_before'])) {
        $qb->andWhere('meeting.fromTime < :from_time')->setParameter('from_time', $parameters['from_time']['strictly_before']);
      } else if (isset($parameters['from_time']['before'])) {
        $qb->andWhere('meeting.fromTime <= :from_time')->setParameter('from_time', $parameters['from_time']['before']);
      }
    }

    if (isset($parameters['to_time'])) {
      if (isset($parameters['to_time']['strictly_after'])) {
        $qb->andWhere('meeting.toTime > :to_time')->setParameter('to_time', $parameters['to_time']['strictly_after']);
      } else if (isset($parameters['to_time']['after'])) {
        $qb->andWhere('meeting.toTime >= :to_time')->setParameter('to_time', $parameters['to_time']['after']);
      }

      if (isset($parameters['to_time']['strictly_before'])) {
        $qb->andWhere('meeting.toTime < :to_time')->setParameter('to_time', $parameters['to_time']['strictly_before']);
      } else if (isset($parameters['to_time']['before'])) {
        $qb->andWhere('meeting.toTime <= :to_time')->setParameter('to_time', $parameters['to_time']['before']);
      }
    }

    if ($onlyCount) {
      $qb->select('COUNT(meeting.id)');
      return $qb->getQuery()->getSingleScalarResult();
    } else {
        $qb
          ->orderBy('meeting.' . $order, $sort)
          ->setFirstResult($offset)
          ->setMaxResults($limit);
    }

    return $qb->getQuery()->execute();
  }

}
