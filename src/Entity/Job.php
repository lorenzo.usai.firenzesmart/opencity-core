<?php

namespace App\Entity;

use App\Repository\JobRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Model\File as FileModel;

/**
 * @ORM\Entity(repositoryClass=JobRepository::class)
 * @Vich\Uploadable()
 */
class Job
{
  /** Job is inserted, and might be started */
  const STATUS_PENDING = 'pending';

  /** Job was never started, and will never be started */
  const STATUS_CANCELLED = 'cancelled';

  /** Job was started and has not exited, yet */
  const STATUS_RUNNING = 'running';

  /** Job exits with a successful exit code */
  const STATUS_FINISHED = 'finished';

  /** Job exits with a non-successful exit code */
  const STATUS_FAILED = 'failed';

  use TimestampableEntity;

  /**
   * @ORM\Column(type="guid")
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @OA\Property(description="Job Uuid")
   * @Groups({"read"})
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank(message="type")
   * @Assert\NotNull()
   * @OA\Property(description="Job type")
   * @Groups({"read", "write"})
   */
  private $type;

  /**
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Status of the job")
   * @Groups({"read"})
   */
  private $status;

  /**
   * @ORM\Column(type="json", nullable=true)
   * @Serializer\Type("array<string, string>")
   * @OA\Property(description="Input Args of the job")
   * @Groups({"read", "write"})
   */
  private $args;

  /**
   * @ORM\ManyToOne(targetEntity=Ente::class)
   * @ORM\JoinColumn(nullable=false)
   * @Serializer\Exclude()
   * @Groups({"read", "write"})
   */
  private $tenant;

  /**
   * @var File
   * @Vich\UploadableField(mapping="job", fileNameProperty="fileName", size="fileSize", mimeType="mimeType")
   * @Serializer\Exclude()
   */
  private $file;

  /**
   * @var string
   * @ORM\Column(type="string", nullable=true)
   * @Serializer\Exclude()
   */
  private $originalFileName;

  /**
   * @var string
   * @ORM\Column(type="string", nullable=true)
   * @Serializer\Exclude()
   */
  private $fileName;

  /**
   * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
   * @Serializer\Exclude()
   */
  private $fileSize;

  /**
   * @var string
   * @ORM\Column(type="string", nullable=true)
   * @Serializer\Exclude()
   */
  private $mimeType;

  /**
   * @ORM\Column(type="json", nullable=true)
   * @Serializer\Exclude()
   */
  private $metadata = [];

  /**
   * @ORM\Column(type="json", nullable=true)
   * @Serializer\Exclude()
   */
  private $errors = [];

  /**
   * @ORM\Column(type="text", nullable=true)
   * @OA\Property(description="Output of the job when execution is complete")
   * @Groups({"read"})
   */
  private $output;

  /**
   * @ORM\Column(type="text", nullable=true)
   * @OA\Property(description="Output of the job during execution")
   * @Groups({"read"})
   */
  private $runningOutput;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   * @Serializer\Type("DateTime")
   * @OA\Property(description="When execution is started", type="string", format="date-time")
   * @Groups({"read"})
   */
  private $startedAt;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   * @Serializer\Type("DateTime")
   * @OA\Property(description="When execution is finished", type="string", format="date-time")
   * @Groups({"read"})
   */
  private $terminatedAt;


  /**
   * @var DateTime
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(type="datetime")
   * @Groups({"read"})
   */
  protected $createdAt;

  /**
   * @var DateTime
   * @Gedmo\Timestampable(on="update")
   * @ORM\Column(type="datetime")
   * @Groups({"read"})
   */
  protected $updatedAt;

  public static function isNonSuccessfulFinalState($state): bool
  {
    return in_array($state, array(self::STATUS_CANCELLED, self::STATUS_FAILED), true);
  }

  public static function getStates(): array
  {
    return array(
      self::STATUS_PENDING,
      self::STATUS_CANCELLED,
      self::STATUS_RUNNING,
      self::STATUS_FINISHED,
      self::STATUS_FAILED
    );
  }


  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
  }


  public function getId()
  {
    return $this->id;
  }

  public function getType(): ?string
  {
    return $this->type;
  }

  public function setType(string $type): self
  {
    $this->type = $type;

    return $this;
  }

  public function getStatus(): ?string
  {
    return $this->status;
  }

  public function setStatus(string $status): self
  {
    $this->status = $status;

    return $this;
  }

  public function getArgs()
  {
    return $this->args;
  }

  public function getArgsAsArray(): array
  {
    if ($this->args) {
      return \json_decode($this->args, true);
    }
    return [];
  }

  public function setArgs($args): self
  {
    $this->args = $args;

    return $this;
  }

  public function getTenant(): ?Ente
  {
    return $this->tenant;
  }

  public function setTenant(?Ente $tenant): self
  {
    $this->tenant = $tenant;

    return $this;
  }

  /**
   * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
   * of 'UploadedFile' is injected into this setter to trigger the  update. If this
   * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
   * must be able to accept an instance of 'File' as the bundle will inject one here
   * during Doctrine hydration.
   *
   * @param File|null $file
   *
   * @return Job
   */
  public function setFile(File $file = null):self
  {
    $this->file = $file;
    if ($file instanceof UploadedFile) {
      $this->originalFileName = $file->getClientOriginalName();
    }
    if (null !== $file) {
      // It is required that at least one field changes if you are using doctrine
      // otherwise the event listeners won't be called and the file is lost
      $this->updatedAt = new \DateTime();
    }

    return $this;
  }

  /**
   * @return File
   */
  public function getFile()
  {
    return $this->file;
  }

  /**
   * Attachment
   *
   * @Serializer\VirtualProperty(name="read_attachment")
   * @OA\Property(property="attachment", type="object",
   *   @OA\Property(property="name", type="string", description="Name of the file"),
   *   @OA\Property(property="mime_type", type="string", description="Mime type of the file", example="text/csv", default="text/csv")
   * )
   * @Groups({"read"})
   */
  public function getAttachment(): ?array
  {
    if ($this->originalFileName) {
      return [
        'name' => $this->originalFileName,
        'mime_type' => $this->mimeType,
      ];
    }
    return null;
  }

  /**
   * Attachment
   *
   * @Serializer\VirtualProperty(name="write_attachment")
   * @OA\Property(property="attachment", type="object",
   *   @OA\Property(property="name", type="string", description="Name of the file"),
   *   @OA\Property(property="mime_type", type="string", description="Mime type of the file", example="text/csv", default="text/csv"),
   *   @OA\Property(property="file", type="string", description="This property could be the content of the file in base64 or the Url to get the file", example="ZXNlbXBpbw== OR www.example.com")
   * )
   * @Groups({"write"})
   */
  public function setAttachment(): array
  {
    return [];
  }

  /**
   * @return string
   */
  public function getOriginalFileName(): string
  {
    return $this->originalFileName;
  }

  /**
   * @param string $originalFileName
   */
  public function setOriginalFileName(string $originalFileName): void
  {
    $this->originalFileName = $originalFileName;
  }

  /**
   * @return string
   */
  public function getFileName(): ?string
  {
    return $this->fileName;
  }

  /**
   * @param string|null $fileName
   */
  public function setFileName(?string $fileName): void
  {
    $this->fileName = $fileName;
  }

  /**
   * @return mixed
   */
  public function getFileSize()
  {
    return $this->fileSize;
  }

  /**
   * @param mixed $fileSize
   */
  public function setFileSize($fileSize): void
  {
    $this->fileSize = $fileSize;
  }

  /**
   * @return string
   */
  public function getMimeType(): ?string
  {
    return $this->mimeType;
  }

  /**
   * @param string|null $mimeType
   */
  public function setMimeType(?string $mimeType): void
  {
    $this->mimeType = $mimeType;
  }

  public function getMetadata(): ?array
  {
    return $this->metadata;
  }

  public function setMetadata(?array $metadata): self
  {
    $this->metadata = $metadata;

    return $this;
  }

  public function getOutput(): ?string
  {
    return $this->output;
  }

  public function setOutput(string $output): self
  {
    $this->output = $output;

    return $this;
  }

  public function getRunningOutput(): ?string
  {
    return $this->runningOutput;
  }

  public function setRunningOutput(?string $runningOutput): self
  {
    $this->runningOutput = $runningOutput;

    return $this;
  }

  public function getStartedAt(): ?\DateTimeInterface
  {
    return $this->startedAt;
  }

  public function setStartedAt(?\DateTimeInterface $startedAt): self
  {
    $this->startedAt = $startedAt;

    return $this;
  }

  public function getTerminatedAt(): ?\DateTimeInterface
  {
    return $this->terminatedAt;
  }

  public function setTerminatedAt(?\DateTimeInterface $terminatedAt): self
  {
    $this->terminatedAt = $terminatedAt;

    return $this;
  }

  /**
   * @return array
   */
  public function getErrors(): array
  {
    return $this->errors;
  }

  /**
   * @param array $errors
   */
  public function setErrors(array $errors): void
  {
    $this->errors = $errors;
  }

  public function setRunning()
  {
    $this->startedAt = new DateTime();
    $this->status = self::STATUS_RUNNING;
  }

}
