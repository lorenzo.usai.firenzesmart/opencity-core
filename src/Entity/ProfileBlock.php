<?php

namespace App\Entity;

use App\Repository\ProfileBlockRepository;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="profile_block", uniqueConstraints={
 *        @UniqueConstraint(name="unique_profile_block",
 *            columns={"user_id", "key", "subform_url"})
 *    }))
 * @ORM\Entity(repositoryClass=ProfileBlockRepository::class)
 */
class ProfileBlock
{

  use TimestampableEntity;

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity=CPSUser::class, inversedBy="profileBlocks")
   * @ORM\JoinColumn(nullable=false)
   */
  private ?CPSUser $user;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private ?string $key;

  /**
   * @ORM\Column(type="json", options={"jsonb":true}, nullable=true)
   */
  private ?array $value;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private ?string $subformUrl;

  /**
   * @ORM\Column(type="json", options={"jsonb":true})
   */
  private array $origin = [];

  /**
   * @ORM\Column(type="string", length=255)
   */
  private ?string $hash;



  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
  }

  public function getId()
  {
    return $this->id;
  }

  public function getUser(): ?CPSUser
  {
    return $this->user;
  }

  public function setUser(?CPSUser $user): self
  {
    $this->user = $user;

    return $this;
  }

  public function getKey(): ?string
  {
    return $this->key;
  }

  public function setKey(string $key): self
  {
    $this->key = $key;

    return $this;
  }

  public function getValue(): ?array
  {
    return $this->value;
  }

  public function setValue(array $value): self
  {
    $this->value = $value;

    return $this;
  }

  public function getSubformUrl(): ?string
  {
    return $this->subformUrl;
  }

  public function setSubformUrl(string $subformUrl): self
  {
    $this->subformUrl = $subformUrl;

    return $this;
  }

  public function getOrigin(): ?array
  {
      return $this->origin;
  }

  public function setOrigin(array $origin): self
  {
      $this->origin = $origin;

      return $this;
  }

  public function getHash(): ?string
  {
      return $this->hash;
  }

  public function setHash(string $hash): self
  {
      $this->hash = $hash;

      return $this;
  }
}
