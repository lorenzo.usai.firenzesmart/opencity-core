<?php

namespace App\FormIO;

use App\Entity\Servizio;

interface SchemaFactoryInterface
{
  /**
   * @param $formIOId
   * @return Schema
   */
  public function createFromFormId($formIOId): Schema;


  public function createFromService(Servizio $servizio): Schema;
}
