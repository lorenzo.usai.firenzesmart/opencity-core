<?php

namespace App\FormIO;

class Schema
{
  private $id;

  private $server;

  private $serverPublicUrl;

  private $sources = [];

  /**
   * @return SchemaComponent[]
   */
  private array $components = [];

  private array $profileBlocks = [];

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getServer()
  {
    return $this->server;
  }

  /**
   * @param mixed $server
   */
  public function setServer($server): void
  {
    $this->server = $server;
  }

  /**
   * @return mixed
   */
  public function getServerPublicUrl()
  {
    return $this->serverPublicUrl;
  }

  /**
   * @param mixed $serverPublicUrl
   */
  public function setServerPublicUrl($serverPublicUrl): void
  {
    $this->serverPublicUrl = $serverPublicUrl;
  }

  public function addComponent($name, $type, $options)
  {
    $this->components[$name] = new SchemaComponent($name, $type, $options);
  }

  public function hasComponents()
  {
    return count($this->components);
  }

  public function getComponentsColumns($column)
  {
    return array_column($this->components, $column);
  }

  /**
   * @return SchemaComponent[]
   */
  public function getComponents()
  {
    return array_values($this->components);
  }

  public function hasComponent($name)
  {
    return isset($this->components[$name]);
  }

  public function countComponents()
  {
    return count($this->components);
  }

  /**
   * @param $name
   * @return array|SchemaComponent
   */
  public function getComponent($name)
  {
    return $this->components[$name];
  }

  public function addSource($id, $data)
  {
    $this->sources[$id] = $data;
  }

  public function getDataBuilder()
  {
    return new DataBuilder($this);
  }

  public function getRequiredComponents(): array
  {
    $required = [];
    foreach ($this->getComponents() as $component){
      if ($component->isRequired()){
        $required[] = $component;
      }
    }

    return $required;
  }

  public function getFileComponents(): array
  {
    $fileComponents = [];
    foreach ($this->getComponents() as $component){
      if ($component->getType() === 'file' || $component->getType() === 'sdcfile') {
        $fileComponents[] = $component;
      }
    }

    return $fileComponents;
  }

  /**
   * @return array
   */
  public function getProfileBlocks(): array
  {
    return $this->profileBlocks;
  }

  /**
   * @param array $profileBlocks
   */
  public function setProfileBlocks(array $profileBlocks): void
  {
    $this->profileBlocks = $profileBlocks;
  }

  public function addComponentToProfileBlocks($component): void
  {
    $id = $component['form'];
    $key = $component['key'];
    $label = $component['label'];
    $url = $this->serverPublicUrl . '/form/' . $id;
    $eservice = null;
    if (!empty($component['properties']['eservice'])) {
      $eservice = $component['properties']['eservice'];
    }
    if (!isset($this->profileBlocks[$key]) || (isset($this->profileBlocks[$key]) && $this->profileBlocks[$key] !== $url) ) {
      $this->profileBlocks[$key]= [
        'id' => $id,
        'key' => $key,
        'label' => $label,
        'url' => $url,
        'eservice' => $eservice
      ];
    }
  }

  public function getPdndBlocks(): array
  {
    if (empty($this->profileBlocks)) {
      return $this->profileBlocks;
    }
    $blocks = [];
    foreach ($this->profileBlocks as $block) {
      if (!empty($block['eservice'])) {
        $blocks[]= $block;
      }
    }
    return  $blocks;
  }
}
