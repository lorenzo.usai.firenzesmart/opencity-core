<?php

namespace App\DependencyInjection\Compiler;

use App\Handlers\Job\JobHandlerRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class JobHandlerPass implements CompilerPassInterface
{
  private $tagName;

  public function __construct($tagName = 'ocsdc.job.handler')
  {
    $this->tagName = $tagName;
  }

  public function process(ContainerBuilder $container)
  {
    if (!$container->has(JobHandlerRegistry::class)) {
      return;
    }

    $definition = $container->findDefinition(JobHandlerRegistry::class);
    $taggedServices = $container->findTaggedServiceIds($this->tagName);
    foreach ($taggedServices as $id => $tags) {
      foreach ($tags as $attributes) {
        if (isset($attributes['alias'])) {
          $definition->addMethodCall('registerHandler', [
            new Reference($id),
            $attributes['alias']
          ]);
          break;
        }
      }
    }
  }
}
