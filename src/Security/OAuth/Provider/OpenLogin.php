<?php

namespace App\Security\OAuth\Provider;

use App\Entity\CPSUser;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ConfigurationAwareTrait;
use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\LogoutUrlTrait;
use App\Security\OAuth\ResourceOwner;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;

class OpenLogin extends GenericProvider implements LogoutProviderInterface,
                                                   ConfigurableProviderInterface
{
  use LogoutUrlTrait;

  use ConfigurationAwareTrait;

  public const IDENTIFIER = 'openlogin';

  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return "OpenCity Italia - OpenLogin";
  }

  protected function createResourceOwner(array $response, AccessToken $token)
  {
    $dataNascita = null;
    $dateOfBirth = $response['dateofbirth'] ?? null;
    if ($dateOfBirth) {
      $dateTime = \DateTime::createFromFormat('Y-m-d', $dateOfBirth);
      if ($dateTime instanceof \DateTime) {
        $dataNascita = $dateTime->format('d/m/Y');
      }
    }

    $digitalAddress = $response['digitaladdress'] ?? null;
    if (!filter_var($digitalAddress, FILTER_VALIDATE_EMAIL)) {
      $digitalAddress = null;
    }

    $email = $response['email'] ?? null;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email = null;
    }

    return (new ResourceOwner($response))
      ->setCodiceFiscale(str_replace('TINIT-', '', $response['fiscalnumber']))
      ->setNome($response['name'])
      ->setCognome($response['familyname'])
      ->setLuogoNascita($response['placeofbirth'] ?? null)
      ->setProvinciaNascita($response['countyofbirth'] ?? null)
      ->setDataNascita($dataNascita)
      ->setSesso($response['gender'] ?? null)
      ->setIndirizzoResidenza($response['registeredoffice'] ?? null)
      ->setIndirizzoDomicilio($response['address'] ?? null)
      ->setIdCard($response['idcard'] ?? null)
      ->setCellulare($response['mobilephone'] ?? null)
      ->setEmailAddress($digitalAddress ?? $email)
      ->setEmailAddressPersonale($email)
      ->setProvider($response['provider'] ?? null)
      ->setAuthenticationMethod(CPSUser::IDP_SPID)
      ->setSpidCode($response['spidcode'])
      ->setSpidLevel($response['spid-level'] ?? 2)
      ->setInstant((new \DateTime())->format(DATE_ISO8601))
      ->setSessionId($response['session'] ?? null)
      ->setSessionIndex($response['session'] ?? null);
  }

}
