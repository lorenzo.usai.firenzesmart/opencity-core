<?php

namespace App\Security\OAuth;

use App\Entity\UserSession;

trait LogoutUrlTrait
{
  protected string $logoutUrl = '';

  public function getUrlLogout(UserSession $userSession): string
  {
    return $this->logoutUrl;
  }

  public function setUrlLogout(string $logoutUrl): void
  {
    $this->logoutUrl = $logoutUrl;
  }
}
