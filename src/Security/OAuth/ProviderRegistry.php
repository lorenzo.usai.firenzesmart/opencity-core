<?php

namespace App\Security\OAuth;

use App\Security\OAuth\Provider\Arpa;
use App\Security\OAuth\Provider\MicrosoftEntraID;
use App\Security\OAuth\Provider\FirenzeSmartOperator;
use App\Security\OAuth\Provider\OpenLogin;
use App\Security\OAuth\Provider\FirenzeSmartCitizen;
use League\OAuth2\Client\Provider\GenericProvider;

class ProviderRegistry
{
  //@todo refactor (e.g. using tagged service)
  private array $providers = [
    OpenLogin::IDENTIFIER => OpenLogin::class,
    Arpa::IDENTIFIER => Arpa::class,
    FirenzeSmartCitizen::IDENTIFIER => FirenzeSmartCitizen::class,
    FirenzeSmartOperator::IDENTIFIER => FirenzeSmartOperator::class,
    MicrosoftEntraID::IDENTIFIER => MicrosoftEntraID::class,
  ];

  /**
   * @return array
   */
  public function getProviders(): array
  {
    return $this->providers;
  }

  public function getProviderByIdentifier($identifier): string
  {
    return $this->providers[$identifier] ?? GenericProvider::class;
  }
}
