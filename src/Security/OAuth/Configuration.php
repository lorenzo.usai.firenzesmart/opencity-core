<?php

namespace App\Security\OAuth;

class Configuration
{
  // Example: https://my.example.com/your-redirect-url/
  private string $redirectUrl;

  // The authorize url in your providers documentation
  // Example: https://github.com/login/oauth/authorize for github
  private string $urlAuthorize;

  // Example: https://service.example.com/token
  private string $urlAccessToken;

  // Example: https://service.example.com/resource
  private string $urlResourceOwnerDetails;

  // The client ID assigned to you by the provider
  private string $clientId;

  // The client password assigned to you by the provider
  private string $clientSecret;

  private string $urlLogout;

  /**
   * @return string
   */
  public function getRedirectUrl(): string
  {
    return $this->redirectUrl;
  }

  /**
   * @param string $redirectUrl
   * @return Configuration
   */
  public function setRedirectUrl(string $redirectUrl): Configuration
  {
    $this->redirectUrl = $redirectUrl;
    return $this;
  }

  /**
   * @return string
   */
  public function getUrlAuthorize(): string
  {
    return $this->urlAuthorize;
  }

  /**
   * @param string $urlAuthorize
   * @return Configuration
   */
  public function setUrlAuthorize(string $urlAuthorize): Configuration
  {
    $this->urlAuthorize = $urlAuthorize;
    return $this;
  }

  /**
   * @return string
   */
  public function getUrlAccessToken(): string
  {
    return $this->urlAccessToken;
  }

  /**
   * @param string $urlAccessToken
   * @return Configuration
   */
  public function setUrlAccessToken(string $urlAccessToken): Configuration
  {
    $this->urlAccessToken = $urlAccessToken;
    return $this;
  }

  /**
   * @return string
   */
  public function getUrlResourceOwnerDetails(): string
  {
    return $this->urlResourceOwnerDetails;
  }

  /**
   * @param string $urlResourceOwnerDetails
   * @return Configuration
   */
  public function setUrlResourceOwnerDetails(string $urlResourceOwnerDetails): Configuration
  {
    $this->urlResourceOwnerDetails = $urlResourceOwnerDetails;
    return $this;
  }

  /**
   * @return string
   */
  public function getClientId(): string
  {
    return $this->clientId;
  }

  /**
   * @param string $clientId
   * @return Configuration
   */
  public function setClientId(string $clientId): Configuration
  {
    $this->clientId = $clientId;
    return $this;
  }

  /**
   * @return string
   */
  public function getClientSecret(): string
  {
    return $this->clientSecret;
  }

  /**
   * @param string $clientSecret
   * @return Configuration
   */
  public function setClientSecret(string $clientSecret): Configuration
  {
    $this->clientSecret = $clientSecret;
    return $this;
  }

  /**
   * @return string
   */
  public function getUrlLogout(): string
  {
    return $this->urlLogout;
  }

  /**
   * @param string $urlLogout
   * @return Configuration
   */
  public function setUrlLogout(string $urlLogout): Configuration
  {
    $this->urlLogout = $urlLogout;
    return $this;
  }

}
