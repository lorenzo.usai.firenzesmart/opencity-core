<?php

namespace App\Security\OAuth;

interface ConfigurableProviderInterface
{
  public function getName(): string;

  public function getIdentifier(): string;

  public function getConfiguration(): Configuration;

  public function setConfiguration(Configuration $configuration): void;
}
