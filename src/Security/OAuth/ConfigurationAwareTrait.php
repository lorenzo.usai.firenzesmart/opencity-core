<?php

namespace App\Security\OAuth;

trait ConfigurationAwareTrait
{
  protected Configuration $configuration;

  public function getConfiguration(): Configuration
  {
    return $this->configuration;
  }

  public function setConfiguration(Configuration $configuration): void
  {
    $this->configuration = $configuration;
  }
}
