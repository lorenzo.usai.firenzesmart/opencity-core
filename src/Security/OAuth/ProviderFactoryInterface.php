<?php

namespace App\Security\OAuth;

use League\OAuth2\Client\Provider\AbstractProvider;

interface ProviderFactoryInterface
{
  public function getConfiguration(): Configuration;

  public function getProviderIdentifier(): string;

  public function instanceProvider(): AbstractProvider;
}
