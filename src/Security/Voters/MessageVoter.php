<?php


namespace App\Security\Voters;


use App\Entity\Message;
use App\Entity\OperatoreUser;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageVoter extends Voter
{
  const EDIT = 'edit';
  const VIEW = 'view';
  const VIEW_DETAILS = 'viewDetails';
  const SET_AUTHOR = 'setAuthor';


  private Security $security;

  public function __construct(Security $security)
  {
    $this->security = $security;
  }

  protected function supports($attribute, $subject): bool
  {
    // if the attribute isn't one we support, return false
    if (!in_array($attribute, [self::EDIT, self::VIEW, self::VIEW_DETAILS, self::SET_AUTHOR])) {
      return false;
    }

    // only vote on `Message` objects
    if ($subject && !$subject instanceof Message) {
      return false;
    }


    return true;
  }

  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    // you know $subject is a Message object, thanks to `supports()`
    /** @var Message $message */
    $message = $subject;

    switch ($attribute) {
      case self::EDIT:
        return $this->canEdit($message, $user);
      case self::VIEW:
        return $this->canView($message, $user);
      case self::VIEW_DETAILS:
        return $this->canViewDetails($message, $user);
      case self::SET_AUTHOR:
        return $this->canSetAuthor($user);

    }

    throw new \LogicException('This code should not be reached!');
  }

  private function canView(Message $message, UserInterface $user): bool
  {
    $pratica = $message->getApplication();
    // if they can edit, they can view
    if ($this->canEdit($message, $user)) {
      return true;
    }
    return $user === $pratica->getUser();
  }

  private function canEdit(Message $message, UserInterface $user): bool
  {
    $pratica = $message->getApplication();
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    // L'operatore puo modificare un messaggio se è un operatore di sistema, altrimenti se è abilitato al servizio,
    // se l'uffico a cui appartiene ha in carico la pratica o se ce l ha in carico
    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      $isAssigned = $pratica->getOperatore() && $pratica->getOperatore() === $user;
      $isUserGroupAssigned = $pratica->getUserGroup() && $user->getUserGroups()->contains($pratica->getUserGroup());
      if ($user->isSystemUser() || in_array($pratica->getServizio()->getId(), $user->getServiziAbilitati()->toArray()) || $isAssigned || $isUserGroupAssigned) {
        return true;
      }
    }
    return false;
  }

  private function canViewDetails(Message $message, UserInterface $user): bool
  {
    // Un utente può vedere i dettagli di un messaggio se lo può modificare oppure se ne è l'autore
    if ($this->canEdit($message, $user)) {
      return true;
    }

    return $message->getAuthor() === $user;
  }

  private function canSetAuthor(UserInterface $user): bool
  {
    return $this->security->isGranted('ROLE_ADMIN') || ($user instanceof OperatoreUser && $user->isSystemUser());
  }
}
