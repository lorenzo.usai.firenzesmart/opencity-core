<?php


namespace App\Security\Voters;


use App\Entity\Meeting;
use App\Entity\OperatoreUser;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class MeetingVoter extends Voter
{
  public const EDIT = 'edit';
  public const VIEW = 'view';
  public const DELETE = 'delete';


  private Security $security;

  public function __construct(Security $security)
  {
    $this->security = $security;
  }

  protected function supports($attribute, $subject): bool
  {
    // if the attribute isn't one we support, return false
    if (!in_array($attribute, [self::EDIT, self::VIEW, self::DELETE])) {
      return false;
    }

    // only vote on `Meeting` objects
    if ($subject && !$subject instanceof Meeting) {
      return false;
    }

    return true;
  }

  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    // you know $subject is a Meeting object, thanks to `supports()`
    /** @var Meeting $meeting */
    $meeting = $subject;

    switch ($attribute) {
      case self::EDIT:
        return $this->canEdit($meeting, $user);
      case self::VIEW:
        return $this->canView($meeting, $user);
      case self::DELETE:
        return $this->canDelete($meeting, $user);
    }

    throw new \LogicException('This code should not be reached!');
  }

  private function canView(Meeting $meeting, UserInterface $user)
  {
    // if they can edit, they can view
    if ($this->canEdit($meeting, $user)) {
      return true;
    }
    return $user->getId() === $meeting->getUserId();
  }

  private function canEdit(Meeting $meeting, UserInterface $user)
  {
    $calendar = $meeting->getCalendar();
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      if ($user->getId() === $calendar->getOwnerId() || in_array($user->getId(), $calendar->getModeratorsId())) {
        return true;
      }
    }

    if ($this->security->isGranted('ROLE_CPS_USER') &&
        $meeting->getStatus() === Meeting::STATUS_DRAFT &&
        (empty($meeting->getUserId()) || $meeting->getUserId() === $user->getId())
    ) {
      return true;
    }

    return false;
  }

  private function canDelete(Meeting $meeting, UserInterface $user)
  {
    // if they can edit, they can delete
    if ($this->canEdit($meeting, $user)) {
      return true;
    }
    return false;
  }
}
