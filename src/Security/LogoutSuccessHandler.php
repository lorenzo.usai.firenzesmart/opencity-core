<?php

namespace App\Security;

use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\ProviderFactoryInterface;
use App\Services\UserSessionService;
use League\OAuth2\Client\Provider\AbstractProvider;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
  private string $singleLogoutUrl;
  private ?AbstractProvider $provider = null;
  private UserSessionService $userSessionService;
  private UrlGeneratorInterface $router;

  public function __construct(
    UrlGeneratorInterface $router,
    ProviderFactoryInterface $oauthConfigurationProvider,
    UserSessionService $userSessionService,
    $singleLogoutUrl,
    $loginRoute
  ) {

    if ($loginRoute === OAuthAuthenticator::LOGIN_ROUTE) {
      $this->provider = $oauthConfigurationProvider->instanceProvider();
      $this->userSessionService = $userSessionService;
    }
    $this->router = $router;
    $this->singleLogoutUrl = $singleLogoutUrl;
  }

  public function onLogoutSuccess(Request $request): RedirectResponse
  {
    if ($this->provider instanceof LogoutProviderInterface){
      $this->singleLogoutUrl = $this->provider->getUrlLogout($this->userSessionService->getCurrentUserSessionData());
    }

    if (empty($this->singleLogoutUrl)) {
      $this->singleLogoutUrl = $this->router->generate('home');
    }
    return new RedirectResponse($this->singleLogoutUrl);
  }
}
