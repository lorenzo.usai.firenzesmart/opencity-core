<?php

namespace App\Security;

use App\Dto\UserAuthenticationData;
use App\Entity\CPSUser;
use App\Services\UserSessionService;
use DateTime;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Security\Cohesion\Cohesion2Client;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CohesionAuthenticator extends AbstractAuthenticator
{
  public const LOGIN_ROUTE = 'login_cohesion';

  private LoggerInterface $logger;

  private Cohesion2Client $authHandler;

  public function __construct(
    UrlGeneratorInterface $urlGenerator,
    $loginRoute = '',
    $siteId = 'TEST',
    UserSessionService $userSessionService,
    JWTTokenManagerInterface $JWTTokenManager,
    LoggerInterface $logger
  ) {
    $this->urlGenerator = $urlGenerator;
    $this->loginRoute = $loginRoute;
    $this->userSessionService = $userSessionService;
    $this->JWTTokenManager = $JWTTokenManager;
    $this->logger = $logger;

    $this->authHandler = new Cohesion2Client($siteId);
    $this->authHandler->useSAML20(true);
  }

  public function supports(Request $request): bool
  {

    try {
      $this->checkLoginRoute();
    } catch (\Exception $e) {
      return false;
    }

    return $request->attributes->get('_route') === self::LOGIN_ROUTE && $request->get('auth');
  }

  protected function getLoginRouteSupported(): array
  {
    return [self::LOGIN_ROUTE];
  }

  protected function getRequestDataToStoreInUserSession(Request $request): array
  {
    return $this->createUserDataFromRequest($request);
  }

  /**
   * @param Request $request
   * @return array
   * @throws \Exception
   */
  protected function createUserDataFromRequest(Request $request): array
  {

    $this->authHandler->verify($request->get('auth'));

    $dateOfBirth = null;
    $dateTime = DateTime::createFromFormat('Y-m-d', $this->authHandler->profile['dateOfBirth']);
    if ($dateTime instanceof DateTime) {
      $dateOfBirth = $dateTime->format('d/m/Y');
    }

    $data = [
      'spidCode' => $this->authHandler->profile['spidCode'] ?? '',
      'nome' => $this->authHandler->profile['name'] ?? '',
      'cognome' => $this->authHandler->profile['familyName'] ?? '',
      'codiceFiscale' => $this->authHandler->profile['codice_fiscale'] ?? '',
      'luogoNascita' => $this->authHandler->profile['placeOfBirth'] ?? '',
      'provinciaNascita' => $this->authHandler->profile['countyOfBirth'] ?? '',
      'dataNascita' => $dateOfBirth,
      'sesso' => $this->authHandler->profile['gender'] ?? '',
      'emailAddress' => $this->authHandler->profile['email'] ?? '',
      'emailAddressPersonale' => $this->authHandler->profile['email'] ?? '',
    ];

    return $data;
  }

  /**
   * @param Request $request
   * @param UserInterface $user
   * @return UserAuthenticationData
   * @throws \Exception
   */
  protected function getUserAuthenticationData(Request $request, UserInterface $user): UserAuthenticationData
  {

    $data = [
      'authenticationMethod' => $this->authHandler->profile['spidCode'] ? CPSUser::IDP_SPID : CPSUser::IDP_CIE,
      'sessionId' => $this->authHandler->id_sso,
      'spidCode' => $request->server->get($this->authHandler->profile['spidCode'] ?? ''),
      'instant' => $this->authHandler->profile['sessionInstand'] ?? '',
      'sessionIndex' => $this->authHandler->id_aspnet,
      'spidLevel' => $request->server->get($this->shibboletServerVarNames['spidLevel'] ?? ''),
    ];
    return UserAuthenticationData::fromArray($data);


    throw new \Exception('MarcheAuthenticator:getUserAuthenticationData - insufficient authentication data');
  }

  /**
   * @throws \Exception
   */
  public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
  {
    $authorizationUrl = $this->authHandler->generateAuthRequest();

    return new RedirectResponse($authorizationUrl);
  }
}
