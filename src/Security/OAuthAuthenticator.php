<?php

namespace App\Security;

use App\Dto\UserAuthenticationData;
use App\Security\OAuth\ProviderFactoryInterface;
use App\Services\UserSessionService;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

class OAuthAuthenticator extends AbstractAuthenticator
{
  public const LOGIN_ROUTE = 'login_oauth';

  private ProviderFactoryInterface $configurationProvider;

  private AbstractProvider $provider;

  private SessionInterface $session;

  public function __construct(
    UrlGeneratorInterface $urlGenerator,
    ProviderFactoryInterface $providerFactory,
    SessionInterface $session,
    UserSessionService $userSessionService,
    JWTTokenManagerInterface $tokenManager,
    string $loginRoute
  ) {
    $this->urlGenerator = $urlGenerator;
    $this->loginRoute = $loginRoute;
    $this->session = $session;
    $this->provider = $providerFactory->instanceProvider();
    $this->userSessionService = $userSessionService;
    $this->JWTTokenManager = $tokenManager;
  }

  protected function getLoginRouteSupported(): array
  {
    return ['login_oauth'];
  }

  protected function createUserDataFromRequest(Request $request): array
  {
    try {
      $this->provider->setPkceCode($this->session->get('oauth2pkceCode'));

      $accessToken = $this->provider->getAccessToken('authorization_code', [
        'code' => $request->query->get('code'),
      ]);

      $resourceOwner = $this->provider->getResourceOwner($accessToken);
      $data = $resourceOwner->toArray();
      $data['token'] = $accessToken->getToken();
      $data['refresh_token'] = $accessToken->getRefreshToken();
      $request->attributes->set('auth-data', $data);

      return $data;
    } catch (IdentityProviderException $e) {
      throw new AuthenticationException($e->getMessage());
    }
  }

  protected function getRequestDataToStoreInUserSession(Request $request)
  {
    return $request->attributes->get('auth-data');
  }

  protected function getUserAuthenticationData(Request $request, UserInterface $user): UserAuthenticationData
  {
    $authData = $request->attributes->get('auth-data');
    $data = [
      'authenticationMethod' => $authData['authenticationMethod'],
      'sessionId' => $authData['sessionId'],
      'spidCode' => $authData['spidCode'],
      'spidLevel' => $authData['spidLevel'],
      'instant' => $authData['instant'],
      'sessionIndex' => $authData['sessionIndex'],
    ];

    return UserAuthenticationData::fromArray($data);
  }

  public function supports(Request $request): bool
  {
    try {
      $this->checkLoginRoute();
    } catch (\Exception $e) {
      return false;
    }

    $supports = $request->attributes->get('_route') === self::LOGIN_ROUTE && $request->get('code');

    if ($supports
      && !empty($request->get('state'))
      && $this->session->has('oauth2state')
      && $request->get('state') !== $this->session->get('oauth2state')
    ) {
      $this->session->remove('oauth2state');
      return false;
    }

    if ($this->session->has('request.return-url')){
      $request->query->set('return-url', $this->session->get('request.return-url'));
      $this->session->remove('request.return-url');
    }

    return $supports;
  }

  public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
  {
    if ($this->loginRoute !== self::LOGIN_ROUTE) {
      throw new UnauthorizedHttpException("Something went wrong in authenticator");
    }

    $returnUrl = $request->query->get('return-url', false);
    if ($returnUrl) {
      $this->session->set('request.return-url', $returnUrl);
    }

    $authorizationUrl = $this->provider->getAuthorizationUrl();
    $this->session->set('oauth2state', $this->provider->getState());
    $this->session->set('oauth2pkceCode', $this->provider->getPkceCode());

    return new RedirectResponse($authorizationUrl);
  }

}
