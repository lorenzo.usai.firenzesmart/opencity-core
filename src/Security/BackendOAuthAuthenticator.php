<?php

namespace App\Security;

use App\Dto\UserAuthenticationData;
use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Entity\User;
use App\Security\OAuth\BackendConfigurationProviderFactory;
use App\Security\OAuth\Configuration;
use App\Security\OAuth\ProviderFactoryInterface;
use App\Services\InstanceService;
use App\Services\Manager\UserManager;
use App\Services\UserSessionService;
use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class BackendOAuthAuthenticator extends AbstractGuardAuthenticator
{
  public const LOGIN_ROUTE = 'security_login';

  private ?AbstractProvider $provider = null;

  private SessionInterface $session;
  private UrlGeneratorInterface $urlGenerator;
  private LoggerInterface $logger;
  private TranslatorInterface $translator;
  private EntityManagerInterface $entityManager;
  private string $loginRoute;
  private UserPasswordEncoderInterface $passwordEncoder;
  private UserManager $userManager;
  private InstanceService $instanceService;

  private array $parameters;

  public function __construct(
    UrlGeneratorInterface $urlGenerator,
    BackendConfigurationProviderFactory $providerFactory,
    SessionInterface $session,
    LoggerInterface $logger,
    TranslatorInterface $translator,
    EntityManagerInterface $entityManager,
    UserPasswordEncoderInterface $passwordEncoder,
    UserManager $userManager,
    InstanceService $instanceService,
    string $loginRoute = 'security_login'
  ) {
    $this->urlGenerator = $urlGenerator;
    $this->provider = $providerFactory->instanceProvider();
    $this->loginRoute = $loginRoute;
    $this->session = $session;
    $this->logger = $logger;
    $this->translator = $translator;
    $this->entityManager = $entityManager;
    $this->passwordEncoder = $passwordEncoder;
    $this->userManager = $userManager;
    $this->instanceService = $instanceService;

  }

  protected function getLoginRouteSupported(): array
  {
    return ['security_login'];
  }

  protected function createUserDataFromRequest(Request $request): array
  {
    try {
      $this->provider->setPkceCode($this->session->get('oauth2pkceCode'));

      $accessToken = $this->provider->getAccessToken('authorization_code', [
        'code' => $request->query->get('code'),
      ]);

      $resourceOwner = $this->provider->getResourceOwner($accessToken);
      $data = $resourceOwner->toArray();
      $data['token'] = $accessToken->getToken();
      $data['refresh_token'] = $accessToken->getRefreshToken();
      $request->attributes->set('auth-data', $data);

      return $data;
    } catch (IdentityProviderException $e) {
      throw new AuthenticationException($e->getMessage());
    }
  }

  public function supports(Request $request): bool
  {
    if (!$this->provider instanceof AbstractProvider || !in_array($this->loginRoute, $this->getLoginRouteSupported())) {
      return false;
    }

    $supports = $request->attributes->get('_route') === self::LOGIN_ROUTE && $request->get('code');

    if ($supports
      && !empty($request->get('state'))
      && $this->session->has('oauth2state')
      && $request->get('state') !== $this->session->get('oauth2state')
    ) {
      $this->session->remove('oauth2state');
      return false;
    }

    return $supports;
  }



  public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
  {
    if ($this->loginRoute !== self::LOGIN_ROUTE) {
      throw new UnauthorizedHttpException("Something went wrong in authenticator");
    }

    $authorizationUrl = $this->provider->getAuthorizationUrl();
    $this->session->set('oauth2state', $this->provider->getState());
    $this->session->set('oauth2pkceCode', $this->provider->getPkceCode());

    return new RedirectResponse($authorizationUrl);
  }

  public function getCredentials(Request $request)
  {
    $credentials = $this->createUserDataFromRequest($request);

    if ($credentials['emailAddress'] === null) {
      return null;
    }

    return $credentials;
  }

  public function getUser($credentials, UserProviderInterface $userProvider)
  {
    $user = $this->entityManager->getRepository(OperatoreUser::class)->findOneBy(['username' => $credentials['emailAddress']]);

    if ($user instanceof OperatoreUser && $user->isSystemUser()) {
      // fail authentication api user
      $this->logger->error("Login error: System user {$user->getUsername()} can not login");
      throw new CustomUserMessageAuthenticationException($this->translator->trans('security.login_failed'));
    }

    // Se non esiste l'utente lo creo
    if (!$user) {
      $user = new OperatoreUser();

      $password = uniqid('', true);

      $user->setUsername($credentials['emailAddress'])
        ->setPlainPassword($password)
        ->setEmail($credentials['emailAddress'])
        ->setNome($credentials['nome'])
        ->setEnte($this->instanceService->getCurrentInstance())
        ->setCognome($credentials['cognome'])
        ->setEnabled(true)
        ->setLastChangePassword(new \DateTime());

      try {
        $user->setPassword(
          $this->passwordEncoder->encodePassword(
            $user,
            $password
          )
        );
        $this->userManager->save($user);
      } catch (\Exception $e) {
        throw new CustomUserMessageAuthenticationException($this->translator->trans('security.login_failed'));
      }
    }
    return $user;

  }

  public function checkCredentials($credentials, UserInterface $user): bool
  {
    return true;
  }

  public function onAuthenticationFailure(Request $request, AuthenticationException $exception): RedirectResponse
  {
    return new RedirectResponse($this->urlGenerator->generate('security_login'));
  }

  public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): RedirectResponse
  {
    return new RedirectResponse($this->urlGenerator->generate('security_login'));
  }

  public function supportsRememberMe(): bool
  {
    return false;
  }
}
