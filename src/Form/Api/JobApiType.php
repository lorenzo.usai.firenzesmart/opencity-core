<?php

namespace App\Form\Api;

use App\Entity\Job;
use App\Form\Rest\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobApiType extends AbstractType
{


  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('type', TextType::class, [
        'required' => true
      ])
      ->add('args', TextType::class, [
        'required' => false
      ]);

    $attachmentSubform = $builder->create('attachment', FormType::class, [
      'label' => false,
      'required' => false,
      'mapped' => false
    ])
      ->add('name', TextType::class, [
        'required' => false
      ])
      ->add('mime_type', TextType::class, [
        'required' => false
      ])
      ->add('file', TextType::class, [
        'required' => false
      ]);

    $builder->add($attachmentSubform);
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Job::class,
      'csrf_protection' => false
    ));
  }
}
