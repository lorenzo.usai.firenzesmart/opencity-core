<?php

namespace App\Form\FormIO;

use App\Entity\FormIO;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Form\Base\PaymentGatewayType;
use App\Form\Base\PraticaFlow;
use App\Form\Base\RecaptchaType;
use App\Form\Base\SelectPaymentGatewayType;
use App\Form\Base\SelezionaEnteType;
use App\Form\Base\StampsCollectionType;
use App\Form\Base\SummaryType;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Exception;

class FormIOFlow extends PraticaFlow
{

  const STEP_MODULO_FORMIO = 1;

  protected $allowDynamicStepNavigation = true;

  protected $revalidatePreviousSteps = false;

  public function onFlowCompleted(Pratica $pratica): void
  {
    if ($pratica->isFormIOType()) {
      $schema = $this->formIOFactory->createFromService($pratica->getServizio());
      if (!empty($pratica->getDematerializedForms()['data'])) {
        $data = $schema->getDataBuilder()->setDataFromArray($pratica->getDematerializedForms()['data'])->toFullFilledFlatArray();
        if (isset($data['related_applications']) && $data['related_applications']) {
          $parentId = trim($data['related_applications']);
          $parent = $this->em->getRepository('App\Entity\Pratica')->find($parentId);
          if ($parent instanceof Pratica) {
            $pratica->setParent($parent);
            $pratica->setServiceGroup($parent->getServizio()->getServiceGroup());
            $pratica->setFolderId($parent->getFolderId());
          }
        }
      }
    }
    parent::onFlowCompleted($pratica);
  }

  protected function loadStepsConfig()
  {
    /** @var FormIO $pratica */
    $pratica = $this->getFormData();
    $servizio = $pratica->getServizio();

    $steps = array(
      self::STEP_MODULO_FORMIO => array(
        'label' => '',
        'form_type' => FormIORenderType::class,
        'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
          return $flow->getFormData()->getStatus() != Pratica::STATUS_DRAFT;
        },
      ),
    );

    $showSummary = true;
    if ($servizio->isPaymentRequired() || (!$this->serviceManager->hasGatewayWithDigitalStamps($servizio) && $pratica->hasStampsToPay())) {
      $steps[] = array(
        'label' => 'steps.common.conferma.label',
        'form_type' => SummaryType::class,
        'form_options' => [
          'validation_groups' => 'recaptcha',
        ],
        'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
          return in_array($flow->getFormData()->getStatus(), [Pratica::STATUS_PAYMENT_PENDING, Pratica::STATUS_STAMPS_PAYMENT_PENDING])
            || ($flow->getFormData()->getStatus() == Pratica::STATUS_PAYMENT_PENDING && $flow->getFormData()->getPaymentType() && !empty($flow->getFormData()->getPaymentData()));
        },
      );
      $showSummary = false;
    }

    // Bolli
    if (!$this->serviceManager->hasGatewayWithDigitalStamps($servizio) && $pratica->getStamps()) {
      $steps[] = array(
        'label' => 'steps.common.stamps.label',
        'form_type' => StampsCollectionType::class,
        'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
          return $flow->getFormData()->getStatus() !== Pratica::STATUS_STAMPS_PAYMENT_PENDING && !$flow->getFormData()->hasStampsToPay();
        },
      );
    }


    // Attivo gli step di pagamento solo se è richiesto nel servizio o c'è il payment data configurato
    if ($pratica->getServizio()->isPaymentRequired()
        || (!empty($pratica->getPaymentData()) && isset($pratica->getPaymentData()['amount']) && $pratica->getPaymentData()['amount'] > 0)
        || ($pratica->getStatus() == Pratica::STATUS_PAYMENT_PENDING && isset($pratica->getDematerializedForms()['flattened'][PaymentDataType::PAYMENT_AMOUNT]) && $pratica->getDematerializedForms()['flattened'][PaymentDataType::PAYMENT_AMOUNT])
    ) {
      $steps[] = array(
        'label' => 'steps.common.payment_gateway.label',
        'form_type' => PaymentGatewayType::class,
        'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
          return $flow->getFormData()->isPaymentExempt();
        },
      );
      /*$steps[] = array(
        'label' => $this->translator->trans('payment.check_payment'),
        'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
          return $flow->getFormData()->isPaymentExempt();
        },
      );*/
    }

    if ($showSummary && $pratica->getStatus() === Pratica::STATUS_DRAFT) {
      // Step conferma
      if ($pratica->getUser() != null) {
        $steps[] = array(
          'label' => 'steps.common.conferma.label',
        );
      } else {
        $steps[] = array(
          'label' => 'steps.common.conferma.label',
          'form_type' => RecaptchaType::class,
          'form_options' => [
            'validation_groups' => 'recaptcha',
          ],
        );
      }

    }

    return $steps;
  }
}
