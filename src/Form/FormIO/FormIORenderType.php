<?php

namespace App\Form\FormIO;

use App\Entity\Allegato;
use App\Entity\CPSUser;
use App\Entity\DematerializedFormPratica;
use App\Entity\FormIO;
use App\Entity\Pratica;
use App\Form\Extension\TestiAccompagnatoriProcedura;
use App\FormIO\Schema;
use App\FormIO\SchemaFactoryInterface;
use App\Services\FormServerApiAdapterService;
use App\Services\Manager\UserManager;
use App\Services\UserSessionService;
use App\Utils\StringUtils;
use App\Validator\Constraints\ExpressionBasedFormIOConstraint;
use DateTime;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use function json_encode;
use App\Services\Manager\PraticaManager;


class FormIORenderType extends AbstractType
{

  private EntityManagerInterface $em;

  private FormServerApiAdapterService $formServerService;

  private array $schema = [];

  private SchemaFactoryInterface $schemaFactory;

  private LoggerInterface $logger;

  private PraticaManager $praticaManager;

  private array $constraintGroups = ['flow_formIO_step1', 'flow_FormIOAnonymous_step1', 'Default'];

  private static array $applicantUserMap = [
    'applicant.completename.name' => 'getNome',
    'applicant.completename.surname' => 'getCognome',
    'applicant.Born.natoAIl' => 'getDataNascita',
    'applicant.Born.place_of_birth' => 'getLuogoNascita',
    'applicant.fiscal_code.fiscal_code' => 'getCodiceFiscale',
    'applicant.address.address' => 'getIndirizzoResidenza',
    'applicant.address.house_number' => 'getCivicoResidenza',
    'applicant.address.municipality' => 'getCittaResidenza',
    'applicant.address.postal_code' => 'getCapResidenza',
    'applicant.address.county' => 'getProvinciaResidenza',
    'applicant.email_address' => 'getEmail',
    'applicant.email_repeat' => 'getEmail',
    'applicant.cell_number' => 'getCellulare',
    'applicant.phone_number' => 'getTelefono',
    'applicant.gender.gender' => 'getSessoAsString',
    'cell_number' => 'getCellulare'
  ];

  private TranslatorInterface $translator;


  /**
   * FormIORenderType constructor.
   * @param EntityManagerInterface $entityManager
   * @param FormServerApiAdapterService $formServerService
   * @param SchemaFactoryInterface $schemaFactory
   * @param LoggerInterface $logger
   * @param PraticaManager $praticaManager
   * @param TranslatorInterface $translator
   */
  public function __construct(
    EntityManagerInterface      $entityManager,
    FormServerApiAdapterService $formServerService,
    SchemaFactoryInterface $schemaFactory,
    LoggerInterface $logger,
    PraticaManager $praticaManager,
    TranslatorInterface $translator
  )
  {
    $this->em = $entityManager;
    $this->formServerService = $formServerService;
    $this->schemaFactory = $schemaFactory;
    $this->logger = $logger;
    $this->translator = $translator;
    $this->praticaManager = $praticaManager;
  }

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    /** @var FormIO $pratica */
    $pratica = $builder->getData();
    $formID = $pratica->getServizio()->getFormIoId();

    $result = $this->formServerService->getSchema($pratica->getServizio());
    if ($result['status'] === 'success') {
      // Questo schema viene recuperato dal formserver, da non confondere con quello restituito da SchemaFactory
      $this->praticaManager->setSchema($result['schema']);
      $this->schema = $result['schema'];
    }

    /** @var TestiAccompagnatoriProcedura $helper */
    $helper = $options["helper"];
    $helper->setStepTitle('steps.scia.modulo_default.label', true);

    // Serve per precompilare i dati dell'applicant
    $data = $this->prefillSubmissionFromKnownUserData($pratica);

    $notEmptyConstraint = new NotEqualTo([
      'value' => '[]',
      'groups' => $this->constraintGroups,
    ]);
    $notEmptyConstraint->message = $this->translator->trans('steps.formio.generic_violation_message');

    $notNullConstraint = new NotEqualTo([
      'value' => '',
      'groups' => $this->constraintGroups,
    ]);
    $notNullConstraint->message = $this->translator->trans('steps.formio.generic_violation_message');

    $expressionBasedConstraint = new ExpressionBasedFormIOConstraint([
      'service' => $pratica->getServizio(),
      'groups' => $this->constraintGroups,
    ]);

    $builder
      ->add(
        'form_id',
        HiddenType::class,
        [
          'attr' => ['value' => $formID],
          'mapped' => false,
          'required' => false,
        ]
      )
      ->add(
        'dematerialized_forms',
        HiddenType::class,
        [
          'attr' => ['value' => $data],
          'mapped' => false,
          'required' => false,
          'constraints' => [
            $notEmptyConstraint,
            $notNullConstraint,
            $expressionBasedConstraint,
          ],
        ]
      )
      ->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'))
      ->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmit'));

  }

  public function getBlockPrefix(): string
  {
    return 'formio_render';
  }

  /**
   * @param FormEvent $event
   * @throws Exception
   */
  public function onPreSubmit(FormEvent $event): void
  {
    /** @var Pratica|DematerializedFormPratica $pratica */
    $pratica = $event->getForm()->getData();

    if ($pratica->getStatus() != Pratica::STATUS_DRAFT) {
      $event->getForm()->addError(new FormError($this->translator->trans('steps.formio.already_submitted_error')));
      $this->logger->error("Submission not allowed for application {$pratica->getId()} with status {$pratica->getStatus()}");
      return;
    }

    $compiledData = [];
    $flattenedData = [];
    $flattenedSchema = $this->praticaManager->flatSchema($this->schema);
    // Todo: move in pratica manager
    $this->praticaManager->setFlatSchema($flattenedSchema);

    if (isset($event->getData()['dematerialized_forms'])) {
      $compiledData = (array)json_decode($event->getData()['dematerialized_forms'], true, 512, JSON_THROW_ON_ERROR);
      $compiledData = StringUtils::cleanData($compiledData);
      $flattenedData = $this->praticaManager->arrayFlat($compiledData);
    }

    $formData = [
      'data' => $compiledData,
      'flattened' => $flattenedData,
      'schema' => $flattenedSchema
    ];

    try {

      $this->praticaManager->validateDematerializedData($formData, $pratica);

      // Check su conformità utente se non è Anonimo
      if ($pratica->getUser() instanceof CPSUser && !$pratica->getUser()->isAnonymous()) {
        try {
          $this->praticaManager->validateUserData($flattenedData, $pratica->getUser(), $pratica->getId());
        } catch (\Exception $e) {
          $event->getForm()->addError(new FormError($e->getMessage()));
        }
      }
      // Verifica la presenza di aree collegate al servizio, validata le coordinate dell'utente ed assegna un'area
      // Solleva un errore nel caso in cui le coordinate dell'utente non sono collegate in nessuna area del servizio
      $this->praticaManager->addApplicationToGeographicAreas($pratica, $formData);

      $pratica->setDematerializedForms($formData);
      $this->praticaManager->addAttachmentsToApplication($pratica, $flattenedData);

    } catch (\Exception $e) {
      $this->logger->error("Received invalid data for application {$pratica->getId()}: {$e->getMessage()}");
      $event->getForm()->addError(new FormError($e->getMessage()));
    }

    if ($pratica->getUser() instanceof CPSUser) {
      $this->em->persist($pratica);
    }
  }

  /**
   * @param FormEvent $event
   */
  public function onPostSubmit(FormEvent $event): void
  {
    /** @var Pratica|DematerializedFormPratica $pratica */
    $pratica = $event->getForm()->getData();
    $user = $pratica->getUser();
    $this->praticaManager->updateUser($pratica->getDematerializedForms(), $user);
  }

  /**
   * @param FormIO $pratica
   * @description Try to prefill submission using user profile data and profile blocks data
   * @return false|string
   */
  private function prefillSubmissionFromKnownUserData(FormIO $pratica)
  {
    $data = $pratica->getDematerializedForms();

    /** @var CPSUser $user */
    $user = $pratica->getUser();

    // Precompilo i campi dell'applicant solo se user è un CPSUser
    if (empty($data) && $user instanceof CPSUser && !$user->isAnonymous()) {
      $schema = $this->schemaFactory->createFromService($pratica->getServizio());
      $cpsUserData = $this->praticaManager->getMappedFormDataWithUserData($schema, $user);
      $profileBlocksData = $this->praticaManager->getProfileBlockManager()->generateSubmissionFromProfileBlocks($user, $schema->getProfileBlocks());

      return json_encode(['data' => array_merge($cpsUserData, $profileBlocksData)]);
    }
    return json_encode($data);
  }
}
