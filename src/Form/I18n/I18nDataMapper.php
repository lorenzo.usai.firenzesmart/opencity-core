<?php
/**
 * Created by Asier Marqués <asiermarques@gmail.com>
 * Date: 17/5/16
 * Time: 20:58
 */

namespace App\Form\I18n;


use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Translatable\TranslatableListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Gedmo\Translatable\Entity\Repository\TranslationRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Exception;

class I18nDataMapper implements I18nDataMapperInterface
{

  private EntityManagerInterface $em;

  private TranslationRepository $repository;

  private FormBuilderInterface $builder;

  private array $translations = [];

  private array $locales = [];

  private $required_locale;

  private array $property_names = [];


  public function __construct(EntityManagerInterface $entityManager, TranslationRepository $repository = null)
  {

    $this->em = $entityManager;

    if (!$repository) {
      $repository = 'Gedmo\Translatable\Entity\Translation';
    }

    $this->repository = $this->em->getRepository($repository);

  }

  public function setBuilder(FormBuilderInterface $builderInterface): void
  {
    $this->builder = $builderInterface;
  }

  public function setRequiredLocale($locale): void
  {
    $this->required_locale = $locale;
  }

  public function setLocales(array $locales): void
  {
    $this->locales = $locales;
  }

  public function getLocales(): array
  {
    return $this->locales;
  }

  public function getTranslations($entity): array
  {
    if (!count($this->translations)) {
      $this->translations = $this->repository->findTranslations($entity);
    }
    return $this->translations;
  }


  /**
   * @param $name
   * @param $type
   * @param array $options
   * @return I18nDataMapper
   * @throws \Exception
   */
  public function add($name, $type, $options = []): I18nDataMapper
  {
    $this->property_names[] = $name;

    $parentOptions = [];
    if (!empty($options['property_path'])) {
      $parentOptions['property_path'] = $options['property_path'];
    }

    $field = $this->builder
      ->add($name, $type, $parentOptions)
      ->get($name);

    if (!$field->getType()->getInnerType() instanceof I18nFieldInterface) {
      throw new \RuntimeException("{$name} must implement I18nFieldInterface");
    }

    foreach ($this->locales as $iso) {
      $fieldOptions = [
        "label" => $options["label"] ?? $iso,
        "attr" => $options["attr"] ?? [],
        "purify_html" => $options["purify_html"] ?? false,
        "required" => ($iso === $this->required_locale && (isset($options["required"]) && $options["required"])),
      ];

      $field->add($iso, get_class($field->getType()->getParent()->getInnerType()), $fieldOptions);
    }

    return $this;

  }


  /**
   * Maps properties of some data to a list of forms.
   *
   * @param mixed $data Structured data.
   * @param FormInterface[] $forms A list of {@link FormInterface} instances.
   *
   * @throws Exception\UnexpectedTypeException if the type of the data parameter is not supported.
   */
  public function mapDataToForms($data, $forms): void
  {
    if (!$data) {
      return;
    }

    $accessor = PropertyAccess::createPropertyAccessor();
    $this->translations = [];
    $translations = $this->getTranslations($data);

    foreach ($forms as $form) {

      if (false !== in_array($form->getName(), $this->property_names, true)) {

        $fieldName = $form->getPropertyPath() ? $form->getPropertyPath()->__toString() : $form->getName();
        // Fix per traduzione vecchi oggetti
        if (empty($translations[$this->required_locale][$fieldName]) && $accessor->isReadable($data, $form->getName()) && !empty($accessor->getValue($data, $fieldName))) {
          $translations[$this->required_locale][$fieldName] = $accessor->getValue($data, $fieldName);
        }

        $values = [];
        foreach ($this->getLocales() as $iso) {
          if (isset($translations[$iso])) {
            $values[$iso] = $translations[$iso][$fieldName] ?? '';
            // Don't decode default language
            if ($iso !== $this->required_locale && $form->getConfig()->getType()->getInnerType() instanceof I18nJsonType) {
              $values[$iso] = json_decode($values[$iso]);
            }
          }
        }
        $form->setData($values);

      } else {
        if (false === $form->getConfig()->getOption("mapped") || null === $form->getConfig()->getOption("mapped")) {
          continue;
        }
        $form->setData($accessor->getValue($data, $form->getName()));
      }
    }
  }

  /**
   * Maps the data of a list of forms into the properties of some data.
   *
   * @param FormInterface[] $forms A list of {@link FormInterface} instances.
   * @param mixed $data Structured data.
   *
   * @throws \Exception
   */
  public function mapFormsToData($forms, &$data): void
  {
    foreach ($forms as $form) {

      $entityInstance = $data;

      if (false !== in_array($form->getName(), $this->property_names, true)) {

        $meta = $this->em->getClassMetadata(get_class($entityInstance));
        $listener = new TranslatableListener();
        $listener->loadMetadataForObjectClass($this->em, $meta);
        $config = $listener->getConfiguration($this->em, $meta->name);
        $translations = $form->getData();
        $fieldName = $form->getPropertyPath() ? $form->getPropertyPath()->__toString() : $form->getName();
        foreach ($this->getLocales() as $iso) {
          if (isset($translations[$iso])) {
            if (isset($config['translationClass'])) {
              $this->em->getRepository($config['translationClass'])->translate($entityInstance, $fieldName, $iso, $translations[$iso]);
            } else {
              $this->repository->translate($entityInstance, $fieldName, $iso, $translations[$iso]);
            }
            $this->em->persist($entityInstance);
          }
        }
      } else {
        if (false === $form->getConfig()->getOption("mapped") || null === $form->getConfig()->getOption("mapped")) {
          continue;
        }
        $accessor = PropertyAccess::createPropertyAccessor();
        $accessor->setValue($entityInstance, $form->getName(), $form->getData());
      }
    }
  }
}
