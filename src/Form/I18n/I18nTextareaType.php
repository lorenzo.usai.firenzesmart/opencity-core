<?php

namespace App\Form\I18n;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class I18nTextareaType extends AbstractType implements I18nFieldInterface
{

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(
      [
        "compound" => true,
        "purify_html" => false
      ]
    );
    $resolver->setRequired(["compound"]);
    $resolver->setAllowedValues("compound", true);
  }

  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    if (isset($options['purify_html'])) {
      $view->vars['purify_html'] = true;
    }
  }

  public function getParent()
  {
    return TextareaType::class;
  }


}
