<?php

namespace App\Form\Operatore\Base;

use App\Dto\ApplicationOutcome;
use App\Entity\Pratica;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Form\Base\StampType;
use App\Services\Manager\ServiceManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ApplicationOutcomeType extends AbstractType
{

  private EntityManagerInterface $entityManager;

  private ServiceManager $serviceManager;

  /**
   * ApplicationOutcomeType constructor.
   * @param EntityManagerInterface $entityManager
   * @param ServiceManager $serviceManager
   */
  public function __construct(EntityManagerInterface $entityManager, ServiceManager $serviceManager)
  {
    $this->entityManager = $entityManager;
    $this->serviceManager = $serviceManager;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    /** @var ApplicationOutcome $outcome */
    $outcome = $builder->getData();

    $repo = $this->entityManager->getRepository('App\Entity\Pratica');
    /** @var Pratica $application */
    $application = $repo->find($outcome->getApplicationId());

    $helper = $options["helper"];
    $helper->setGuideText('operatori.flow.approva_o_rigetta.guida_alla_compilazione', true);
    $helper->setVueApp('outcome_attachments');
    $helper->setVueBundledData(json_encode([
      'applicationId' => $outcome->getApplicationId(),
      'attachments' => [],
      'prefix' => $helper->getPrefix(),
    ]));

    $builder->add(
      "outcome",
      ChoiceType::class,
      [
        "label" => 'operatori.flow.approva_o_rigetta.motivazione.esito_label',
        "required" => true,
        "expanded" => true,
        "multiple" => false,
        "choices" => [
          "Approva" => true,
          "Rigetta" => false,
        ],
      ]
    )->add(
      "message",
      TextareaType::class,
      [
        "label" => 'operatori.flow.approva_o_rigetta.motivazione.esito_label',
        "required" => false,
      ]
    )->add(
      "attachments",
      HiddenType::class,
      [
        "label" => 'operatori.flow.approva_o_rigetta.motivazione.allega_label',
        "required" => false,
      ]
    );

    if ($application->getServizio()->needsPayments()) {
      $builder->add(PaymentDataType::PAYMENT_AMOUNT, MoneyType::class, [
        'required' => false,
        'data' => $application->getServizio()->isPaymentDeferred() ? $application->getPaymentAmount() : 0,
        'empty_data' => 0,
        'attr' => ['class' => 'form-control-sm'],
        'label' => 'operatori.flow.approva_o_rigetta.importo_da_pagare'
      ]);
    }

    if (!$this->serviceManager->hasGatewayWithDigitalStamps($application->getServizio())) {
      $builder->add('stamps', CollectionType::class, [
        'label' => false,
        'data' => $application->getServizio()->getReleaseStamps(),
        'entry_type' => StampType::class,
        'entry_options' => ['view' => StampType::VIEW_BACKEND],
        'allow_add' => true,
        'allow_delete' => true
      ]);
    }

  }

  public function getBlockPrefix(): string
  {
    return 'outcome';
  }
}
