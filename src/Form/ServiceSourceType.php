<?php

namespace App\Form;

use App\Model\FlowStep;

use App\Model\ServiceSource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceSourceType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('id')
      ->add('url')
      ->add('updated_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('md5')
      ->add('version')
      ->add('identifier');
    }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => ServiceSource::class,
      'csrf_protection' => false,
      //'allow_extra_fields' => true,
    ));
  }
}
