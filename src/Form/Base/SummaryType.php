<?php


namespace App\Form\Base;


use App\Entity\FormIO;
use App\Entity\Pratica;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Services\Manager\PraticaManager;
use App\Services\PaymentService;
use App\Services\PraticaStatusService;
use Doctrine\ORM\EntityManagerInterface;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class MessageType
 */
class SummaryType extends AbstractType
{

  private PraticaStatusService $statusService;

  private PraticaManager $praticaManager;

  /**
   * @param PraticaStatusService $statusService
   * @param PraticaManager $praticaManager
   */
  public function __construct(PraticaStatusService $statusService, PraticaManager $praticaManager)
  {
    $this->statusService = $statusService;
    $this->praticaManager = $praticaManager;
  }

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    /** @var Pratica $pratica */
    $pratica = $builder->getData();

    // Add recaptcha if user is anonymous
    if ($pratica->getUser() === null) {
      $constraint = new RecaptchaTrue();
      $constraint->message = 'Questo valore non è un captcha valido.';
      $constraint->groups = ['recaptcha'];


      $builder
        ->add('recaptcha', EWZRecaptchaType::class,
          [
            'label' => false,
            'mapped' => false,
            'constraints' => [$constraint]
          ]);
    }

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event): void
  {

    /** @var FormIO $application */
    $application = $event->getForm()->getData();

    if ($application->hasStampsToPay() || $application->getPaymentAmount() > 0) {

      $this->praticaManager->collectProfileBlocks($application);

      if ($application->hasStampsToPay()) {
        $this->statusService->setNewStatus($application, Pratica::STATUS_STAMPS_PAYMENT_PENDING);
      } else if ($application->getPaymentAmount() > 0) {
        // Se la pratica ha un pagamento in fase di richiesta configurato
        $this->praticaManager->selectPaymentGateway($application);
      }
    }

  }

  public function getBlockPrefix(): string
  {
    return 'pratica_summary';
  }
}
