<?php

namespace App\Form\Base;

use App\Entity\Pratica;
use App\Form\Extension\TestiAccompagnatoriProcedura;
use App\Payment\AbstractPaymentData;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Payment\GatewayCollection;
use App\Payment\PaymentDataInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;


class PaymentGatewayType extends AbstractType
{

  private GatewayCollection $gatewayCollection;

  public function __construct(GatewayCollection $gatewayCollection)
  {
    $this->gatewayCollection = $gatewayCollection;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    /** @var Pratica $pratica */
    $pratica = $builder->getData();
    $availableGateways = $this->gatewayCollection->getAvailablePaymentGateways();
    $gatewayClassHandler = $availableGateways[$pratica->getPaymentType()]['handler'] ?? null;

    $paymentData = $pratica->getPaymentData() ?? [];
    if ($gatewayClassHandler instanceof Bollo) {
      $builder
        ->add('payment_data', HiddenType::class,
          [
            'attr' => ['value' => json_encode($paymentData)],
            'mapped' => true,
            'required' => false,
          ]
        );
    } elseif ($gatewayClassHandler instanceof MyPay) {
      $pratica->setPaymentData(AbstractPaymentData::getSanitizedPaymentData($pratica));
      $builder
        ->add('payment_data', HiddenType::class,
          [
            'mapped' => false,
            'required' => false,
          ]
        );
    }

    // Imposto un default a generic-external nel caso non sia riuscito a recuperare l'handler
    // Questo step andrà eliminato appena verranno eliminati i metodi di pagmanto mypay e bollo(legacy)
    if (!$gatewayClassHandler instanceof PaymentDataInterface)
    {
      $handlers = $this->gatewayCollection->getHandlers();
      $gatewayClassHandler = $handlers['generic-external'];
    }

    $builder->addEventSubscriber($gatewayClassHandler);
  }

  public function getBlockPrefix(): string
  {
    return 'pratica_payment_gateway';
  }
}
