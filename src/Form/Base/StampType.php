<?php


namespace App\Form\Base;


use App\Model\Stamp;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Contracts\Translation\TranslatorInterface;

class StampType extends AbstractType
{

  public const VIEW_BACKEND = 'backend';
  public const VIEW_FRONTEND = 'frontend';

  private TranslatorInterface $translator;

  public function __construct(TranslatorInterface $translator)
  {
    $this->translator = $translator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {


    if ($options['view'] === self::VIEW_BACKEND) {
      $builder
        ->add('reason', TextType::class, [
          'label' => 'servizio.stamps.reason',
          'required' => true,
          'attr' => ['class' => 'reason'],
        ])
        ->add('identifier', TextType::class, [
          'label' => 'servizio.stamps.identifier',
          'required' => true,
          'attr' => ['class' => 'identifier'],
        ])
        ->add('amount', MoneyType::class, [
          'label' => 'servizio.stamps.amount',
          'required' => true,
        ])
        ->add('phase', ChoiceType::class, [
          'label' => 'servizio.stamps.phase.label',
          'choices' => [
            $this->translator->trans('servizio.stamps.phase.data.request') => Stamp::PHASE_REQUEST,
            $this->translator->trans('servizio.stamps.phase.data.release') => Stamp::PHASE_RELEASE
          ],
          'required' => true,
        ])
      ;
    } else {
      $builder
        ->add('reason', TextType::class, [
          'label' => 'servizio.stamps.reason',
          'required' => true
        ])
        ->add('identifier', HiddenType::class, [
          'required' => true,
        ])
        ->add('amount', MoneyType::class, [
          'label' => 'servizio.stamps.amount',
          'required' => true
        ])
        ->add('phase', HiddenType::class, [
          'required' => true,
        ])
        ->add('paid', HiddenType::class, [
          'required' => false
        ])
        ->add('number', TextType::class, [
          'label' => 'servizio.stamps.number',
          'attr' => ['maxlength' => 14],
          'required' => true,
          'empty_data' => '',
          'constraints' => [
            new Regex([
              'pattern'=> '/^[0-9]{14}$/'
            ])
          ],
        ])
        ->add('emittedAt', DateTimeType::class, [
          'widget' => 'single_text',
          'required' => true,
          'label' => 'servizio.stamps.emission_date',
          'constraints' => [
            new LessThan([
              'value' => 'now',
            ])
          ],
        ]);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver):void
  {
    $resolver->setDefaults(array(
      'data_class' => Stamp::class,
      'csrf_protection' => false,
      'view' => self::VIEW_FRONTEND,
    ));
  }
}
