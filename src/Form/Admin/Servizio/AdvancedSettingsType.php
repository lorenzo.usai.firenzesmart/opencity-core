<?php


namespace App\Form\Admin\Servizio;

use App\Form\I18n\AbstractI18nType;
use App\Form\I18n\I18nDataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdvancedSettingsType extends AbstractI18nType
{

  /**
   * @var TranslatorInterface
   */
  private $translator;

  /**
   * @param I18nDataMapperInterface $dataMapper
   * @param $locale
   * @param $locales
   * @param TranslatorInterface $translator
   */
  public function __construct(I18nDataMapperInterface $dataMapper, $locale, $locales, TranslatorInterface $translator) 
  {
    parent::__construct($dataMapper, $locale, $locales);
    $this->translator = $translator;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('disable_message_notifications', CheckboxType::class, [
        'label' => "messages.disable_message_notifications",
        'help' => $this->translator->trans("messages.disable_message_notifications_help") . "<p>",
        'help_html' => true,
        'required' => false
      ])
      ->add('allow_reopening', CheckboxType::class,[
        'label' => 'servizio.consenti_riapertura',
        'required' => false
      ]);

  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Entity\Servizio'
    ));

    $this->configureTranslationOptions($resolver);
  }

  public function getBlockPrefix()
  {
    return 'advanced_settings';
  }
}
