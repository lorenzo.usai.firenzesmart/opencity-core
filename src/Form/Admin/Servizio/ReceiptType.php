<?php


namespace App\Form\Admin\Servizio;

use App\Entity\Servizio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ReceiptType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('receipt', ChoiceType::class, [
        'choices' => [
          'Modulo compilato: su richiesta' => Servizio::RECEIPT_COMPILED_MODULE_ON_DEMAND,
          'Modulo compilato: automatico' => Servizio::RECEIPT_COMPILED_MODULE,
          'F24' => Servizio::RECEIPT_F24
        ],
        'multiple' => false,
        'expanded' => true
      ])
      ;
  }

  public function getBlockPrefix()
  {
    return 'receipt';
  }
}
