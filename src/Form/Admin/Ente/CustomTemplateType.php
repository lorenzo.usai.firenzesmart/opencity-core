<?php


namespace App\Form\Admin\Ente;


use App\Model\DefaultProtocolSettings;
use App\Model\Mailer;
use App\Model\CustomTemplate;
use App\Model\Tenant\ThemeOptions;
use App\Utils\StringUtils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomTemplateType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('header', TextareaType::class, [
        'label' => 'ente.custom_template.header',
        'required' => false,
        'attr' => ['class' => 'custom-template-header'],
        'empty_data' => null,
      ])
      ->add('footer', TextareaType::class, [
        'label' => 'ente.custom_template.footer',
        'attr' => ['class' => 'custom-template-footer'],
        'required' => false,
        'empty_data' => null,
      ])
    ;

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event): void
  {
    $data = $event->getData();
    if (!empty($data['header'])) {
      $data['header'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['header']);
    }
    if (!empty($data['footer'])) {
      $data['footer'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['footer']);
    }
    $event->setData($data);
  }


  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => CustomTemplate::class,
      'csrf_protection' => false
    ));
  }
}
