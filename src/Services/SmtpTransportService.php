<?php

namespace App\Services;

use App\Entity\ScheduledAction;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Swift_Message;


class SmtpTransportService implements ScheduledActionHandlerInterface
{
  const ACTION_SEND_EMAIL = 'send_email';
  const ACTION_SEND_PEC = 'send_pec';

  private ScheduleActionService $scheduleActionService;
  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;
  private Swift_Mailer $mailer;
  private $defaultSender;


  /**
   * @param ScheduleActionService $scheduleActionService
   * @param EntityManagerInterface $entityManager
   * @param Swift_Mailer $mailer
   * @param LoggerInterface $logger
   * @param $defaultSender
   */
  public function __construct(ScheduleActionService $scheduleActionService, EntityManagerInterface $entityManager, Swift_Mailer $mailer, LoggerInterface $logger, $defaultSender)
  {
    $this->scheduleActionService = $scheduleActionService;
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->mailer = $mailer;
    $this->defaultSender = $defaultSender;
  }


  public function sendEmailAsync(Swift_Message $message)
  {

    $params = [
      'subject' => $message->getSubject(),
      'from' => $message->getFrom(),
      'to' => $message->getTo(),
      'cc' => $message->getCc(),
      'bcc' => $message->getBcc(),
      'body' => $message->getBody()
    ];

    try {
      $this->scheduleActionService->appendAction(
        'ocsdc.smtp_transport_service',
        self::ACTION_SEND_EMAIL,
        serialize($params)
      );
    } catch (AlreadyScheduledException $e) {
      $this->logger->error('Mail send is already scheduled', $params);
    }
  }

  /**
   * @throws Exception
   */
  public function executeScheduledAction(ScheduledAction $action)
  {

    if ($action->getType() == self::ACTION_SEND_EMAIL) {
      $params = unserialize($action->getParams());
      $from = !empty($params['from']) ? $params['from'] : $this->defaultSender;
      $to = $params['to'];
      if (substr($to, 0, 1) === '-') {
        $to = substr($to, 1); // Remove the hyphen at the beginning
      }

      $message = (new Swift_Message())
        ->setSubject($params['subject'])
        ->setFrom($from)
        ->setTo($to)
        ->setBody($params['body'], 'text/html')
        ->addPart(strip_tags($params['body']), 'text/plain');

      $failed = [];
      $this->mailer->send($message, $failed);
      if (count($failed) > 0) {
        throw new Exception(implode(',', $failed));
      }
    }
  }
}
