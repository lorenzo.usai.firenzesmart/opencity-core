<?php

namespace App\Services\Manager;

use App\Entity\CPSUser;
use App\Entity\Pratica;
use App\Entity\ProfileBlock;
use App\Entity\Servizio;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ProfileBlockManager
{

  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  public function save(Pratica $application, $key, $value, $subformUrl): void
  {
    // Per adesso non gestiamo l'applicant
    if ($key === 'applicant') {
      return;
    }

    $user = $application->getUser();
    $profileBlockRepo = $this->entityManager->getRepository(ProfileBlock::class);
    $profileBlock = $profileBlockRepo->findOneBy(['user' => $user, 'key' => $key, "subformUrl" => $subformUrl]);

    $hash = md5(json_encode($value));

    if ($profileBlock instanceof ProfileBlock) {
      if ($profileBlock->getHash() === $hash) {
        return;
      }
      $profileBlock
        ->setValue($value)
        ->setHash($hash);
    } else {
      $profileBlock = new ProfileBlock();
      $profileBlock
        ->setUser($user)
        ->setKey($key)
        ->setValue($value)
        ->setSubformUrl($subformUrl)
        ->setOrigin($this->generateOrigin($application))
        ->setHash($hash);
    }

    $this->entityManager->persist($profileBlock);
    $this->entityManager->flush();

  }


  public function generateSubmissionFromProfileBlocks(CPSUser $user, ?array $profileBlocks): array
  {
    $submission = [];
    if (!$profileBlocks) {
      return $submission;
    }
    $profileBlockRepo = $this->entityManager->getRepository(ProfileBlock::class);
    foreach ($profileBlocks as $key => $value) {
      $profileBlock = $profileBlockRepo->findOneBy(['user' => $user, 'key' => $key, "subformUrl" => $value['url']]);
      if ($profileBlock instanceof ProfileBlock) {
        $submission[$key] = $profileBlock->getValue();
      }
    }

    return $submission;
  }

  private function generateOrigin(Pratica $application): array
  {
    $service = $application->getServizio();
    $origin = [
      'service_id' => $service->getId(),
      'service_name' => $service->getName(),
      'application_id' => $application->getId(),
      'session_id' => $application->getSessionData()->getId()
    ];
    return $origin;
  }
}
