<?php


namespace App\Services\Manager;


use App\Entity\AdminUser;
use App\Entity\Allegato;
use App\Entity\AllegatoMessaggio;
use App\Entity\CPSUser;
use App\Entity\FormIO;
use App\Entity\GeographicArea;
use App\Entity\Message;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use App\Entity\RichiestaIntegrazioneDTO;
use App\Entity\RispostaIntegrazione;
use App\Entity\RispostaIntegrazioneRepository;
use App\Entity\ScheduledAction;
use App\Entity\Servizio;
use App\Entity\StatusChange;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Event\GenerateApplicationReceiptEvent;
use App\FormIO\ExpressionValidator;
use App\FormIO\Schema;
use App\FormIO\SchemaComponent;
use App\FormIO\SchemaFactoryInterface;
use App\Logging\LogConstants;
use App\Model\StampSettings;
use App\Model\Transition;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Protocollo\ProtocolloEvents;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\Services\BackOfficeCollection;
use App\Services\CPSUserProvider;
use App\Services\GeoService;
use App\Services\InstanceService;
use App\Services\ModuloPdfBuilderService;
use App\Services\PaymentService;
use App\Services\PraticaStatusService;
use App\Utils\FormIOUtils;
use App\Utils\FormUtils;
use App\Utils\UploadedBase64File;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class PraticaManager
{

  // Mappa una chiave "human redable" su quelle pessime usate da formio
  public const APPLICANT_KEYS = [
    'name' => 'applicant.data.completename.data.name',
    'surname' => 'applicant.data.completename.data.surname',
    'email' => 'applicant.data.email_address',
    'fiscal_code' => 'applicant.data.fiscal_code.data.fiscal_code',
  ];

  // Serve per mappare i dati dello schema con quelli dell'utente
  public const APPLICATION_USER_MAP = [
    'applicant.completename.name' => 'getNome',
    'applicant.completename.surname' => 'getCognome',
    'applicant.Born.natoAIl' => 'getDataNascita',
    'applicant.Born.place_of_birth' => 'getLuogoNascita',
    'applicant.fiscal_code.fiscal_code' => 'getCodiceFiscale',
    'applicant.address.address' => 'getIndirizzoResidenza',
    'applicant.address.house_number' => 'getCivicoResidenza',
    'applicant.address.municipality' => 'getCittaResidenza',
    'applicant.address.postal_code' => 'getCapResidenza',
    'applicant.address.county' => 'getProvinciaResidenza',
    'applicant.email_address' => 'getEmail',
    'applicant.email_repeat' => 'getEmail',
    'applicant.cell_number' => 'getCellulare',
    'applicant.phone_number' => 'getTelefono',
    'applicant.gender.gender' => 'getSessoAsString',
    'cell_number' => 'getCellulare',
  ];

  /** @var */
  private $schema = null;

  private $flatSchema = null;

  private ModuloPdfBuilderService $moduloPdfBuilderService;

  private PraticaStatusService $praticaStatusService;

  private LoggerInterface $logger;

  private EntityManagerInterface $entityManager;

  private InstanceService $is;

  private TranslatorInterface $translator;

  private SchemaFactoryInterface $schemaFactory;

  private MessageManager $messageManager;
  private PaymentService $paymentService;

  private ExpressionValidator $expressionValidator;

  private UserManager $userManager;
  private GeoService $geoService;
  private ProfileBlockManager $profileBlockManager;
  private CPSUserProvider $cpsUserProvider;
  private EventDispatcherInterface $dispatcher;
  private PdndManager $pdndManager;
  /**
   * @var BackOfficeCollection
   */
  private $backOfficeCollection;

  /**
   * PraticaManagerService constructor.
   * @param EntityManagerInterface $entityManager
   * @param InstanceService $instanceService
   * @param ModuloPdfBuilderService $moduloPdfBuilderService
   * @param PraticaStatusService $praticaStatusService
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   * @param SchemaFactoryInterface $schemaFactory
   * @param MessageManager $messageManager ,
   * @param PaymentService $paymentService ,
   * @param ExpressionValidator $expressionValidator
   * @param UserManager $userManager
   * @param GeoService $geoService
   * @param ProfileBlockManager $profileBlockManager
   * @param CPSUserProvider $cpsUserProvider
   * @param EventDispatcherInterface $dispatcher
   * @param PdndManager $pdndManager
   */
  public function __construct(
    EntityManagerInterface   $entityManager,
    InstanceService          $instanceService,
    ModuloPdfBuilderService  $moduloPdfBuilderService,
    PraticaStatusService     $praticaStatusService,
    TranslatorInterface      $translator,
    LoggerInterface          $logger,
    SchemaFactoryInterface   $schemaFactory,
    MessageManager           $messageManager,
    PaymentService           $paymentService,
    ExpressionValidator      $expressionValidator,
    UserManager              $userManager,
    GeoService               $geoService,
    ProfileBlockManager      $profileBlockManager,
    CPSUserProvider          $cpsUserProvider,
    EventDispatcherInterface $dispatcher,
    PdndManager              $pdndManager,
    BackOfficeCollection     $backOfficeCollection
  )
  {
    $this->moduloPdfBuilderService = $moduloPdfBuilderService;
    $this->praticaStatusService = $praticaStatusService;
    $this->logger = $logger;
    $this->entityManager = $entityManager;
    $this->is = $instanceService;
    $this->translator = $translator;
    $this->schemaFactory = $schemaFactory;
    $this->messageManager = $messageManager;
    $this->paymentService = $paymentService;
    $this->expressionValidator = $expressionValidator;
    $this->userManager = $userManager;
    $this->geoService = $geoService;
    $this->profileBlockManager = $profileBlockManager;
    $this->cpsUserProvider = $cpsUserProvider;
    $this->dispatcher = $dispatcher;
    $this->pdndManager = $pdndManager;
    $this->backOfficeCollection = $backOfficeCollection;
  }

  /**
   * @return mixed
   */
  public function getSchema()
  {
    return $this->schema;
  }

  /**
   * @param mixed $schema
   */
  public function setSchema($schema): void
  {
    $this->schema = $schema;
  }

  /**
   * @return null
   */
  public function getFlatSchema()
  {
    return $this->flatSchema;
  }

  /**
   * @param null $flatSchema
   */
  public function setFlatSchema($flatSchema): void
  {
    $this->flatSchema = $flatSchema;
  }


  /**
   * @return ProfileBlockManager
   */
  public function getProfileBlockManager(): ProfileBlockManager
  {
    return $this->profileBlockManager;
  }

  public function save(Pratica $pratica): void
  {
    $this->entityManager->persist($pratica);
    $this->entityManager->flush();
  }

  /**
   * @param Pratica $pratica
   * @throws Exception
   */
  public function finalizeSubmission(Pratica $pratica): void
  {

    /** @var PraticaRepository $repo */
    $repo = $this->entityManager->getRepository(Pratica::class);

    // Per non sovrascrivere comportamento in formio flow
    if ($pratica->getFolderId() == null) {
      $pratica->setServiceGroup($pratica->getServizio()->getServiceGroup());
      $pratica->setFolderId($repo->getFolderForApplication($pratica));
    }

    if ($pratica->getStatus() == Pratica::STATUS_DRAFT) {

      $pratica->setSubmissionTime(time());
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_PRE_SUBMIT);

      // Emette l'evento per la generazione del pdf
      $this->dispatcher->dispatch(new GenerateApplicationReceiptEvent($pratica), GenerateApplicationReceiptEvent::NAME);

    } elseif ($pratica->getStatus() == Pratica::STATUS_DRAFT_FOR_INTEGRATION) {

      // Creo il file principale per le integrazioni
      $integrationsAnswer = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRispostaIntegrazione($pratica);
      $pratica->addAllegato($integrationsAnswer);
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION);
    }
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param null $message
   * @throws Exception
   */
  public function finalizePaymentCompleteSubmission(Pratica $pratica, User $user, $message = null): void
  {

    $statusChange = new StatusChange();
    $statusChange->setEvento('Pagamento completato');
    $statusChange->setOperatore($user->getFullName());
    $this->praticaStatusService->setNewStatus(
      $pratica,
      Pratica::STATUS_PAYMENT_SUCCESS,
      $statusChange
    );

    if ($message['message'] !== null) {
      $this->generateStatusMessage($pratica, $message['message'], $message['subject'], [], $message['visibility']);
    }

    // Stiamo forzando lo stato della pratica in pagata, dobbiamo eliminare i pagamenti pendenti sul proxy di pagamento se presenti
    $this->paymentService->cancelApplicationPendingPayment($pratica);

  }

  /**
   * @param Pratica $pratica
   * @param User $author
   * @param OperatoreUser|null $assignedUser
   * @param UserGroup|null $userGroup
   * @param string|null $message
   * @param DateTime|null $assignedAt
   * @throws Exception
   */
  public function assign(Pratica $pratica, User $author, OperatoreUser $assignedUser = null, UserGroup $userGroup = null, ?string $message = null, ?DateTime $assignedAt = null): void
  {
    // Assegnazione automatica all'autore se non è stato specificato un gruppo o un ufficio
    if (!$assignedUser && !$userGroup) {
      $assignedUser = $author;
    }

    if ($assignedUser && $pratica->getOperatore() && $pratica->getOperatore()->getId() === $assignedUser->getId() && $pratica->getUserGroup() === $userGroup) {
      throw new BadRequestHttpException(
        $this->translator->trans('pratica.already_assigned', ['%operator_fullname%' => $pratica->getOperatore()->getFullName()])
      );
    }

    if ($pratica->getServizio()->isProtocolRequired() && $pratica->getNumeroProtocollo() === null) {
      throw new BadRequestHttpException($this->translator->trans('pratica.no_protocol_number'));
    }

    if ($assignedUser && $userGroup && !$assignedUser->getUserGroups()->contains($userGroup)) {
      throw new BadRequestHttpException($this->translator->trans('operatori.user_not_in_user_group', [
        '%fullname%' => $assignedUser->getFullName(),
        '%user_group%' => $userGroup->getName(),
      ]));
    }

    // Assegnazioni automatiche del gruppo
    if ($assignedUser && !$userGroup) {
      if ($pratica->getUserGroup() && $pratica->getUserGroup()->getUsers()->contains($assignedUser)) {
        // Se non è specificato il gruppo tra i parametri, ma l'operatore appartiene al gruppo a cui è assegnata
        // la pratica mantengo l'assegnazione del gruppo
        $userGroup = $pratica->getUserGroup();
      } else {
        // Ricerco tutti gli uffici a cui appartiene l'operatore
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('user_group')
          ->from('App:UserGroup', 'user_group')
          ->andWhere(':user MEMBER OF user_group.users')
          ->setParameter('user', $assignedUser)
          ->orderBy('user_group.name', 'ASC');

        $userGroups = $qb->getQuery()->getResult();
        $preferredUserGroup = null;

        if (!empty($userGroups)) {
          // Fixme: inserire nell'ordinamento della query precedente: attenzione però perché la pratica può essere associata ad un ufficio che non è incaricato del servizio
          foreach ($userGroups as $u) {
            if (!$preferredUserGroup && $u->getServices()->contains($pratica->getServizio())) {
              // Utilizzo il primo ufficio in ordine alfabetico che sia associato al servizio della pratica
              $preferredUserGroup = $u;
            }
          }

          // Se nessun ufficio gestisce il servizio allora prendo il primo in ordine alfabetico
          $userGroup = $preferredUserGroup ?? reset($userGroups);
        }
      }
    }

    $pratica->setOperatore($assignedUser);
    $pratica->setUserGroup($userGroup);
    $this->entityManager->persist($pratica);
    $this->entityManager->flush($pratica);

    $statusChange = new StatusChange();
    $statusChange->setEvento('Presa in carico');
    $statusChange->setResponsabile($author->getFullName());

    if ($assignedUser) {
      $statusChange->setOperatore($assignedUser->getFullName());
    }

    if ($userGroup) {
      $statusChange->setUserGroup($userGroup->getName());
    }

    if ($message) {
      $statusChange->setMessage($message);
    }

    if ($assignedAt) {
      $statusChange->setTimestamp($assignedAt->getTimestamp());
    }

    try {
      $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_PENDING);
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_PENDING, $statusChange);
    } catch (\Exception $e) {
      $this->logger->error("Invalid status change for application {$pratica->getId()}: set previous status {$pratica->getStatus()}");
    }

    $this->logger->info(
      LogConstants::PRATICA_ASSIGNED,
      [
        'pratica' => $pratica->getId(),
        'user' => $pratica->getUser()->getId(),
        'author' => $author->getId(),
      ]
    );
  }


  /**
   * @param Pratica $pratica
   * @param User $user
   * @param float|null $paymentAmount
   * @param array $stamps
   * @throws Exception
   */
  public function finalize(Pratica $pratica, User $user, ?float $paymentAmount = 0, array $stamps = []): void
  {
    if ($pratica->getStatus() == Pratica::STATUS_COMPLETE
      || $pratica->getStatus() == Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE
      || $pratica->getStatus() == Pratica::STATUS_CANCELLED
      || $pratica->getStatus() == Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE) {
      throw new BadRequestHttpException($this->translator->trans('pratica.already_processed'));
    }

    if ($pratica->getRispostaOperatore() === null) {
      $signedResponse = $this->moduloPdfBuilderService->createSignedResponseForPratica($pratica);
      $pratica->addRispostaOperatore($signedResponse);
    }

    $protocolloIsRequired = $pratica->getServizio()->isProtocolRequired();
    $statusChange = new StatusChange();
    $statusChange->setOperatore($user->getFullName());

    if ($pratica->getEsito()) {
      $statusChange->setEvento('Approvazione pratica');
      $statusChange->setOperatore($user->getFullName());

      if (($pratica->getServizio()->needsPayments() && $paymentAmount > 0) || !empty($stamps)) {

        // Se sono stati configurati dei bolli
        if (!empty($stamps)) {
          $this->addDeferredStampsPayment($pratica, $stamps);
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_STAMPS_PAYMENT_PENDING,
            $statusChange
          );
        }

        // Se è stato configurato un pagamento
        if ($pratica->getServizio()->needsPayments() && $paymentAmount > 0) {
          $this->addDeferedPayment($pratica, $paymentAmount);
          if ($pratica->getStatus() != Pratica::STATUS_STAMPS_PAYMENT_PENDING) {
            $this->praticaStatusService->setNewStatus(
              $pratica,
              Pratica::STATUS_PAYMENT_PENDING,
              $statusChange
            );
          }
        }

      } else {

        if ($protocolloIsRequired) {
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE,
            $statusChange
          );
        } else {
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_COMPLETE,
            $statusChange
          );
        }
      }

      $this->logger->info(
        LogConstants::PRATICA_APPROVED,
        [
          'pratica' => $pratica->getId(),
          'user' => $pratica->getUser()->getId(),
        ]
      );
    } else {

      $statusChange->setEvento('Rifiuto pratica');
      $statusChange->setOperatore($user->getFullName());

      if ($protocolloIsRequired) {
        $this->praticaStatusService->setNewStatus(
          $pratica,
          Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE,
          $statusChange
        );
      } else {
        $this->praticaStatusService->setNewStatus(
          $pratica,
          Pratica::STATUS_CANCELLED,
          $statusChange
        );
      }

      $this->logger->info(
        LogConstants::PRATICA_CANCELLED,
        [
          'pratica' => $pratica->getId(),
          'user' => $pratica->getUser()->getId(),
        ]
      );
    }
  }

  public function addDeferredStampsPayment(Pratica $pratica, array $stamps): void
  {
    $stamps = array_merge($pratica->getStamps(), $stamps);
    $pratica->setStamps($stamps);
  }

  /**
   * @throws Exception
   */
  public function addDeferedPayment(Pratica $pratica, $paymentAmount): void
  {
    // Seleziono il primo gateway disponibile
    $selectedGateways = $pratica->getServizio()->getPaymentParameters()['gateways'] ?? [];
    if (!$selectedGateways) {
      throw new BadRequestHttpException($this->translator->trans('payment.no_selected_gateways'));
    }
    $identifier = array_keys($selectedGateways)[0];

    // Vecchi gateway, da deprecare
    if (in_array($identifier, [Bollo::IDENTIFIER, MyPay::IDENTIFIER])) {
      $pratica->setPaymentAmount($paymentAmount);
    } else {
      $pratica->setPaymentData($this->paymentService->createDefferedPaymentData($pratica, $paymentAmount));
    }
    $pratica->setPaymentType($identifier);

    $this->entityManager->persist($pratica);
    $this->entityManager->flush();
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @throws Exception
   */
  public function withdrawApplication(Pratica $pratica, User $user): void
  {

    if ($pratica->getStatus() == Pratica::STATUS_WITHDRAW) {
      throw new BadRequestHttpException($this->translator->trans('pratica.already_processed'));
    }

    if ($pratica->getWithdrawAttachment() === null) {
      $withdrawAttachment = $this->moduloPdfBuilderService->createWithdrawForPratica($pratica);
      $pratica->addAllegato($withdrawAttachment);
    }

    $statusChange = new StatusChange();
    $this->praticaStatusService->setNewStatus(
      $pratica,
      Pratica::STATUS_WITHDRAW,
      $statusChange
    );

    $this->logger->info(
      LogConstants::PRATICA_WITHDRAW,
      [
        'pratica' => $pratica->getId(),
        'user' => $pratica->getUser()->getId(),
      ]
    );
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $data
   * @throws Exception
   */
  public function requestIntegration(Pratica $pratica, User $user, $data): void
  {

    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);

    $message = new Message();
    $message->setApplication($pratica);
    $message->setProtocolRequired(false);
    $message->setVisibility(Message::VISIBILITY_APPLICANT);
    $message->setMessage($data['message']);
    $message->setSubject($this->translator->trans('pratica.messaggi.oggetto', ['%pratica%' => $message->getApplication()]));
    $message->setAuthor($user);

    $requestAttachmentsIds = $requestAttachments = [];
    foreach ($data['attachments'] as $attachment) {
      $base64Content = $attachment->getFile();
      $file = new UploadedBase64File($base64Content, $attachment->getMimeType(), $attachment->getName());
      $allegato = new AllegatoMessaggio();
      $allegato->setFile($file);
      $allegato->setOwner($pratica->getUser());
      $allegato->setDescription('Allegato richiesta integrazione');
      $allegato->setOriginalFilename($attachment->getName());
      //$allegato->setIdRichiestaIntegrazione($integration->getId());
      $this->entityManager->persist($allegato);
      $message->addAttachment($allegato);
      $requestAttachments[] = $allegato;
      $requestAttachmentsIds[] = $allegato->getId();
    }

    // Creo il file di richiesta integrazione
    $richiestaIntegrazione = new RichiestaIntegrazioneDTO([], null, $data['message']);
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);
    $integration = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRichiestaIntegrazione(
      $pratica,
      $richiestaIntegrazione,
      $requestAttachments
    );
    if (!empty($requestAttachmentsIds)) {
      $integration->setAttachments($requestAttachmentsIds);
      $this->entityManager->persist($integration);
    }
    $pratica->addRichiestaIntegrazione($integration);


    $this->entityManager->persist($pratica);
    $this->entityManager->flush();
    $this->messageManager->save($message);

    $statusChange = new StatusChange();
    $statusChange->setOperatore($user->getFullName());
    $statusChange->setMessageId($message->getId());
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @return void
   * @throws FileExistsException
   * @throws Exception
   */
  public function cancelIntegration(Pratica $pratica, User $user): void
  {
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION);
    $integrationsAnswer = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRispostaIntegrazione(
      $pratica,
      [],
      true
    );
    $pratica->addAllegato($integrationsAnswer);

    if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
      $statusChange = new StatusChange();
      $statusChange->setOperatore($user->getFullName());
    }
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $messages
   * @return void
   * @throws FileExistsException
   * @throws Exception
   */
  public function acceptIntegration(Pratica $pratica, User $user, $messages = null): void
  {
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION);
    $integrationsAnswer = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRispostaIntegrazione(
      $pratica,
      $messages
    );
    $pratica->addAllegato($integrationsAnswer);

    if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
      $statusChange = new StatusChange();
      $statusChange->setOperatore($user->getFullName());
    }
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $data
   * @throws Exception
   */
  public function registerIntegrationRequest(Pratica $pratica, UserInterface $user, $data): void
  {
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_DRAFT_FOR_INTEGRATION);
    $integrationRequest = $pratica->getRichiestaDiIntegrazioneAttiva();
    $integrationRequest->setNumeroProtocollo($data['integration_outbound_protocol_number']);
    $integrationRequest->setIdDocumentoProtocollo($data['integration_outbound_protocol_document_id']);
    $this->entityManager->persist($integrationRequest);
    $this->entityManager->flush();

    if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
      $statusChange = new StatusChange();
      $statusChange->setOperatore($user->getFullName());
    }
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_DRAFT_FOR_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $data
   * @throws Exception
   */
  public function registerIntegrationAnswer(Pratica $pratica, UserInterface $user, $data): void
  {
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_REGISTERED_AFTER_INTEGRATION);

    $integrationAnswer = $this->getIntegrationAnswer($pratica);

    if ($integrationAnswer instanceof RispostaIntegrazione) {


      $integrationAnswer->setNumeroProtocollo($data['integration_inbound_protocol_number']);
      $integrationAnswer->setIdDocumentoProtocollo($data['integration_inbound_protocol_document_id']);

      $this->entityManager->persist($integrationAnswer);
      $this->entityManager->flush();

      if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
        $statusChange = new StatusChange();
        $statusChange->setOperatore($user->getFullName());
      }
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_REGISTERED_AFTER_INTEGRATION, $statusChange);
    }
  }

  /**
   * @param Pratica $pratica
   * @return RispostaIntegrazione|null
   */
  public function getIntegrationAnswer(Pratica $pratica): ?RispostaIntegrazione
  {

    $integrationRequest = $pratica->getRichiestaDiIntegrazioneAttiva();

    /** @var RispostaIntegrazioneRepository $integrationAnswerRepo */
    $integrationAnswerRepo = $this->entityManager->getRepository('App\Entity\RispostaIntegrazione');

    $integrationAnswerCollection = $integrationAnswerRepo->findByIntegrationRequest($integrationRequest->getId());

    if (!empty($integrationAnswerCollection)) {
      /** @var RispostaIntegrazione $answer */
      return $integrationAnswerCollection[0];
    }

    return null;
  }

  /**
   * @param Pratica $pratica
   * @param string $text
   * @param string $subject
   * @param array $callToActions
   * @param string|null $visibility
   * @return Message
   */
  public function generateStatusMessage(
    Pratica $pratica,
    string  $text,
    string  $subject,
    array   $callToActions = [],
    ?string $visibility = Message::VISIBILITY_APPLICANT
  ): Message
  {
    $message = new Message();
    $message->setApplication($pratica);
    $message->setProtocolRequired(false);
    $message->setVisibility($visibility);
    $message->setMessage($text);
    $message->setSubject($subject);
    $message->setCallToAction($callToActions);
    $message->setEmail($pratica->getUser()->getEmailContatto());
    $message->setSentAt(time());

    try {
      $this->messageManager->save($message, false);
      $this->entityManager->persist($pratica);
      $this->entityManager->flush();
    } catch (Exception $e) {
      $this->logger->error("Impossible to generate status message for application {$pratica->getId()}: {$e->getMessage()}");
    }
    return $message;
  }

  public function createDraftApplication(Servizio $servizio, CPSUser $user, array $additionalDematerializedData): FormIO
  {
    $pratica = new FormIO();
    $pratica->setUser($user);
    $pratica->setServizio($servizio);
    $pratica->setStatus(Pratica::STATUS_DRAFT);
    $pratica->setEnte($this->is->getCurrentInstance());

    $cpsUserData = [
      'applicant' => [
        'data' => [
          'completename' => [
            'data' => [
              'name' => $user->getNome(),
              'surname' => $user->getCognome(),
            ],
          ],
          'gender' => [
            'data' => [
              'gender' => $user->getSessoAsString(),
            ],
          ],
          'Born' => [
            'data' => [
              'natoAIl' => $user->getDataNascita() ? $user->getDataNascita()->format('d/m/Y') : '',
              'place_of_birth' => $user->getLuogoNascita(),
            ],
          ],
          'fiscal_code' => [
            'data' => [
              'fiscal_code' => $user->getCodiceFiscale(),
            ],
          ],
          'address' => [
            'data' => [
              'address' => $user->getIndirizzoResidenza(),
              'house_number' => '',
              'municipality' => $user->getCittaResidenza(),
              'postal_code' => $user->getCapResidenza(),
              'county' => $user->getProvinciaResidenza(),
            ],
          ],
          'email_address' => $user->getEmail(),
          'email_repeat' => $user->getEmail(),
          'cell_number' => $user->getCellulare(),
          'phone_number' => $user->getTelefono(),
        ],
      ],
      'cell_number' => $user->getCellulare(),
      'phone_number' => $user->getTelefono(),
    ];

    $pratica->setDematerializedForms([
      "data" => array_merge(
        $additionalDematerializedData,
        $cpsUserData
      ),
    ]);

    $this->entityManager->persist($pratica);
    $this->entityManager->flush();

    return $pratica;
  }

  public function flatSchema(array $schema, ?string $prefix = ''): array
  {
    $result = [];
    foreach ($schema as $key => $value) {

      if ($key === 'metadata' || $key === 'state') {
        continue;
      }

      $newKey = $prefix . (empty($prefix) ? '' : '.') . $key;

      if (is_array($value)) {
        $result = array_merge($result, $this->flatSchema($value, $newKey));
      } else {
        $result[$newKey] = $value;
      }
    }
    return $result;
  }


  /**
   * @param $array
   * @param array|null $prefix
   * @return array
   */
  public function arrayFlat($array, ?array $prefix = []): array
  {
    $result = array();
    foreach ($array as $key => $value) {
      $tempPrefix = $prefix;
      if ($key === 'metadata' || $key === 'state') {
        continue;
      }

      $isFile = false;
      $tempPrefix []= $key;
      // Nel caso dei datagrid lo schema ha sempre e solo l'indice 0 mentre la submission può avere più valori
      // Va confrontato quindi sempre con l'elemento con indice 0 dello schema per capire il tipo
      $checkKeyParts = [];
      foreach ($tempPrefix as $v) {
        if (is_int($v) && $v > 0) {
          $checkKeyParts []= 0;
          continue;
        }
        $checkKeyParts []= $v;
      }
      $new_key = implode('.', $tempPrefix);
      $checkKey = implode('.', $checkKeyParts) . '.type';

      if (isset($this->flatSchema[$checkKey]) &&
        ($this->flatSchema[$checkKey] === 'file' || $this->flatSchema[$checkKey] === 'sdcfile' || $this->flatSchema[$checkKey] === 'financial_report')) {
        $isFile = true;
      }

      if (is_array($value) && !$isFile) {
        $result = array_merge($result, $this->arrayFlat($value, $tempPrefix));
      } else {
        $result[$new_key] = $value;
      }
    }
    return $result;
  }

  /**
   * @param array $data
   * @return CPSUser
   */
  public function checkUser(array $data): CPSUser
  {
    $cf = $data['flattened']['applicant.data.fiscal_code.data.fiscal_code'] ?? false;

    $user = null;
    if ($cf) {
      $qb = $this->entityManager->createQueryBuilder()
        ->select('u')
        ->from('App:CPSUser', 'u')
        ->andWhere('UPPER(u.username) = :username')
        ->setParameter('username', strtoupper($cf));
      try {
        $user = $qb->getQuery()->getSingleResult();
      } catch (\Exception $e) {
      }
    }

    if (!$user instanceof CPSUser) {
      $user = $this->cpsUserProvider->createAnonymousUser();
    }
    $this->updateUser($data, $user);
    return $user;
  }

  /**
   * @param array $data
   * @param CPSUser $user
   */
  public function updateUser(array $data, CPSUser $user): void
  {
    $email = $this->getUserEmail($data, $user);

    if ($user->isAnonymous()) {
      $cf = $data['flattened']['applicant.data.fiscal_code.data.fiscal_code'] ?? false;
      $birthDay = null;
      if (isset($data['flattened']['applicant.data.Born.data.natoAIl']) && !empty($data['flattened']['applicant.data.Born.data.natoAIl'])) {
        $birthDay = DateTime::createFromFormat('d/m/Y', $data['flattened']['applicant.data.Born.data.natoAIl']);
      }

      $user
        ->setSessoAsString($data['flattened']['applicant.gender.gender'] ?? '')
        ->setCellulareContatto($data['flattened']['applicant.data.cell_number'] ?? '')
        ->setCpsTelefono($data['flattened']['applicant.data.phone_number'] ?? '')
        ->setEmail($email)
        ->setEmailContatto($email)
        ->setNome($data['flattened']['applicant.data.completename.data.name'] ?? '')
        ->setCognome($data['flattened']['applicant.data.completename.data.surname'] ?? '')
        ->setDataNascita($birthDay)
        ->setLuogoNascita(isset($data['flattened']['applicant.data.Born.data.place_of_birth']) && !empty($data['flattened']['applicant.data.Born.data.place_of_birth']) ? $data['flattened']['applicant.data.Born.data.place_of_birth'] : '')
        ->setSdcIndirizzoResidenza(isset($data['flattened']['applicant.data.address.data.address']) && !empty($data['flattened']['applicant.data.address.data.address']) ? $data['flattened']['applicant.data.address.data.address'] : '')
        ->setSdcCivicoResidenza(isset($data['flattened']['applicant.data.address.data.house_number']) && !empty($data['flattened']['applicant.data.address.data.house_number']) ? $data['flattened']['applicant.data.address.data.house_number'] : '')
        ->setSdcCittaResidenza(isset($data['flattened']['applicant.data.address.data.municipality']) && !empty($data['flattened']['applicant.data.address.data.municipality']) ? $data['flattened']['applicant.data.address.data.municipality'] : '')
        ->setSdcCapResidenza(isset($data['flattened']['applicant.data.address.data.postal_code']) && !empty($data['flattened']['applicant.data.address.data.postal_code']) ? $data['flattened']['applicant.data.address.data.postal_code'] : '')
        ->setSdcProvinciaResidenza(isset($data['flattened']['applicant.data.address.data.county']) && !empty($data['flattened']['applicant.data.address.data.county']) ? $data['flattened']['applicant.data.address.data.county'] : '');
    } else {
      $user
        ->setEmailContatto($email)
        ->setSdcIndirizzoResidenza(isset($data['flattened']['applicant.data.address.data.address']) && !empty($data['flattened']['applicant.data.address.data.address']) ? $data['flattened']['applicant.data.address.data.address'] : '')
        ->setSdcCivicoResidenza(isset($data['flattened']['applicant.data.address.data.house_number']) && !empty($data['flattened']['applicant.data.address.data.house_number']) ? $data['flattened']['applicant.data.address.data.house_number'] : '')
        ->setSdcCittaResidenza(isset($data['flattened']['applicant.data.address.data.municipality']) && !empty($data['flattened']['applicant.data.address.data.municipality']) ? $data['flattened']['applicant.data.address.data.municipality'] : '')
        ->setSdcCapResidenza(isset($data['flattened']['applicant.data.address.data.postal_code']) && !empty($data['flattened']['applicant.data.address.data.postal_code']) ? $data['flattened']['applicant.data.address.data.postal_code'] : '')
        ->setSdcProvinciaResidenza(isset($data['flattened']['applicant.data.address.data.county']) && !empty($data['flattened']['applicant.data.address.data.county']) ? $data['flattened']['applicant.data.address.data.county'] : '');
      if (!empty($data['flattened']['applicant.data.cell_number'])) {
        $user->setCellulareContatto($data['flattened']['applicant.data.cell_number']);
      }
      if (!empty($data['flattened']['applicant.data.phone_number'])) {
        $user->setCpsTelefono($data['flattened']['applicant.data.phone_number']);
      }
    }

    $this->userManager->save($user);
  }

  /**
   * @param array $data
   * @param CPSUser $user
   * @param null $applicationId
   * @throws Exception
   */
  public function validateUserData(array $data, CPSUser $user, $applicationId = null): void
  {
    if (strcasecmp($data['applicant.data.fiscal_code.data.fiscal_code'], $user->getCodiceFiscale()) != 0) {
      $this->logger->error("Fiscal code Mismatch", [
          'pratica' => $applicationId ?? '-',
          'cps' => $user->getCodiceFiscale(),
          'form' => $data['applicant.data.fiscal_code.data.fiscal_code'],
        ]
      );
      throw new Exception($this->translator->trans('steps.formio.fiscalcode_violation_message'));
    }

    if (strcasecmp($data['applicant.data.completename.data.name'], $user->getNome()) != 0) {
      $this->logger->error("Name Mismatch", [
          'pratica' => $applicationId ?? '-',
          'cps' => $user->getCodiceFiscale(),
          'form' => $data['applicant.data.completename.data.name'],
        ]
      );
      throw new Exception($this->translator->trans('steps.formio.name_violation_message'));
    }

    if (strcasecmp($data['applicant.data.completename.data.surname'], $user->getCognome()) != 0) {
      $this->logger->error("Surname Mismatch", [
          'pratica' => $applicationId ?? '-',
          'cps' => $user->getCodiceFiscale(),
          'form' => $data['applicant.data.completename.data.surname'],
        ]
      );
      throw new Exception($this->translator->trans('steps.formio.surname_violation_message'));
    }
  }

  /**
   * @param array $data
   * @param Pratica $pratica
   * @return void
   */
  public function validateDematerializedData(array $data, Pratica $pratica): void
  {
    // Verifico che la submission non sia vuota
    if (empty($data['data']) || empty($data['flattened'])) {
      $this->logger->error("Received empty dematerialized data");
      throw new ValidatorException($this->translator->trans('steps.formio.empty_data_violation_message'));
    }

    // Check sulla presenza del codice fiscale (per pratiche vuote)
    if (!isset($data['flattened']['applicant.data.fiscal_code.data.fiscal_code'])) {
      $this->logger->error("Dematerialized form not well formed", ['pratica' => $pratica->getId()]);
      throw new ValidatorException($this->translator->trans('steps.formio.generic_violation_message'));
    }

    // Check sulla univocità di un campo
    $errors = $this->expressionValidator->validateData($pratica->getServizio(), json_encode($data['data']));
    if (!empty($errors)) {
      $this->logger->error("Received duplcated unique_id");
      throw new ValidatorException($this->translator->trans('steps.formio.duplicated_unique_id'));
    }

    // Verifica fascicolazione della pratica
    if (isset($data['flattened']['related_applications'])) {
      $parentId = trim($data['flattened']['related_applications']);
      if ($parentId == $pratica->getId()) {
        $this->logger->error("The application {$parentId} cannot be linked to itself");
        throw new ValidatorException($this->translator->trans('steps.formio.parent_id_error'));
      }
    }

    // Verifica campi necessari alle integrazioni
    $integrations = $pratica->getServizio()->getIntegrations();
    if ($integrations) {
      $backOfficeHandler = $this->backOfficeCollection->getBackOffice(reset($integrations));
      if (!$backOfficeHandler->isSubmissionValid($data['flattened'])) {
        $this->logger->error("The application {$pratica->getId()} data is not valid for integration with the {$backOfficeHandler->getIdentifier()} backoffice");
        throw new ValidatorException($this->translator->trans("steps.formio.{$backOfficeHandler->getIdentifier()}_integration_error"));
      }
    }
  }

  /**
   * @param Pratica $pratica
   * @param array $data
   * @return void
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function addApplicationToGeographicAreas(Pratica $pratica, array $data): void
  {
    $currentAreas = $pratica->getGeographicAreas();
    $areaIds = [];
    $areas = $this->validateApplicationCoordinates($data, $pratica);

    foreach ($areas as $area) {
      $pratica->addGeographicArea($area);
      $areaIds[] = $area->getId();
    }

    // Rimuovo le aree che non devono più essere associate alla pratica
    foreach ($currentAreas as $currentArea) {
      if (!in_array($currentArea->getId(), $areaIds)) {
        $pratica->removeGeographicArea($currentArea);
      }
    }
  }

  /**
   * @param array $data
   * @param Pratica $pratica
   * @return array
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  private function validateApplicationCoordinates(array $data, Pratica $pratica): array
  {
    $result = [];
    // Se non sono definite aree geografiche per il servizio non devo procedere
    // Se la submission non contiene un campo address o non è valorizzato correttamente non devo procedere
    if ($pratica->getServizio()->getGeographicAreas()->isEmpty() || !isset($data['data']['address']) ||
      empty($data['data']['address']['lon']) || empty($data['data']['address']['lat'])) {
      return $result;
    }

    $lon = $data['data']['address']['lon'];
    $lat = $data['data']['address']['lat'];

    /** @var GeographicArea $area */
    foreach ($pratica->getServizio()->getGeographicAreas() as $area) {
      if ($area->getGeofence() && $this->geoService->geoFenceContainsPoint($lon, $lat, $area->getGeofence())) {
        $result [] = $area;
      }
    }

    if (empty($result)) {
      $this->logger->error("There is no geographic area that contains the specified point", [
        'lon' => $lon,
        'lat' => $lat,
        'service_id' => $pratica->getServizio()->getId(),
      ]);
      throw new ValidatorException($this->translator->trans('steps.formio.geographic_area_violation_message'));
    }
    return $result;
  }

  public function collectProfileBlocks(Pratica $pratica): void
  {
    if ($pratica->getServizio()->isLegacy()) {
      return;
    }

    $schema = $this->schemaFactory->createFromService($pratica->getServizio());
    // Todo: I profile block del servizio non vengono più recuperati dal db, valutare impatto performance ed in caso eliminare salvataggio nel database
    //$profileBlocks = $pratica->getServizio()->getProfileBlocks();
    $profileBlocks = $schema->getProfileBlocks();

    if (empty($profileBlocks)) {
      return;
    }

    $saveApplication = false;
    $data = $pratica->getDematerializedForms();
    foreach ($profileBlocks as $key => $value) {
      if (isset($data['data'][$key]) && !FormUtils::isArrayEmpty($data['data'][$key])) {
        try {
          if (!empty($value['eservice'])) {
            $data['data'][$key]['meta']['is_valid'] = $this->pdndManager->isValidData($data['data'][$key]);
            $saveApplication =  true;
          }
          $this->profileBlockManager->save($pratica, $key, $data['data'][$key], $value['url']);
        } catch (\Throwable $e) {
          $this->logger->error('Unable to save profile block: ' . $e->getMessage(), [
            'application_id' => $pratica->getId(),
            'profile_block_key' => $key,
          ]);
        }
      }
    }

    if ($saveApplication) {
      $pratica->setDematerializedForms($data);
      $this->save($pratica);
    }

  }


  /**
   * @param Pratica $pratica
   * @param $selectedProfileBlocks
   * @description Funzione che salva solo i dati per i profile block selezionati, attualmente non viene chiamata perché tutti i profile block vengono salvati
   * @return void
   */
  public function collectSelectedProfileBlocks(Pratica $pratica, $selectedProfileBlocks): void
  {

    if ($pratica->getServizio()->isLegacy() || empty($selectedProfileBlocks)) {
      return;
    }

    $selectedProfileBlocks = explode(',', $selectedProfileBlocks);
    $data = $pratica->getDematerializedForms();
    foreach ($pratica->getServizio()->getProfileBlocks() as $key => $value) {
      if (in_array($key, $selectedProfileBlocks) && isset($data['data'][$key])) {
        try {
          $this->profileBlockManager->save($pratica, $key, $data['data'][$key], $value['url']);
        } catch (\Throwable $e) {
          $this->logger->error('Unable to save profile block: ' . $e->getMessage(), [
            'application_id' => $pratica->getId(),
            'profile_block_key' => $key,
          ]);
        }
      }
    }
  }


  /**
   * @param Schema $schema
   * @param CPSUser $user
   * @return mixed
   */
  public function getMappedFormDataWithUserData(Schema $schema, CPSUser $user)
  {
    $data = $schema->getDataBuilder();
    if ($schema->hasComponents()) {
      foreach (self::APPLICATION_USER_MAP as $schemaFlatName => $userMethod) {
        try {
          if ($schema->hasComponent($schemaFlatName) && method_exists($user, $userMethod)) {
            $component = $schema->getComponent($schemaFlatName);
            $value = $user->{$userMethod}();
            // se il campo è datatime popola con iso8601 altrimenti testo
            if ($value instanceof DateTime) {
              if ($component['form_type'] == DateTimeType::class) {
                $value = $value->format(\DateTimeInterface::W3C);
              } else {
                $value = $value->format('d/m/Y');
              }
            }
            if ($component['form_type'] == ChoiceType::class
              && isset($component['form_options']['choices'])
              && !empty($component['form_options']['choices'])) {
              if ($schemaFlatName !== 'applicant.gender.gender') {
                $value = strtoupper($value);
              }
              if (!in_array($value, $component['form_options']['choices'])) {
                $value = null;
              }
            }
            if ($value) {
              $data->set($schemaFlatName, $value);
            }
          }
        } catch (\Exception $e) {
          $this->logger->error($e->getMessage());
        }
      }
    }

    return $data->toArray();
  }

  /**
   * @param Pratica $pratica
   * @param $flattenedData
   * @throws Exception
   */
  public function addAttachmentsToApplication(Pratica $pratica, $flattenedData): void
  {
    $repo = $this->entityManager->getRepository(Allegato::class);
    $currentAttachments = $pratica->getAllegati();
    $attachments = [];
    foreach ($flattenedData as $value) {
      if (is_array($value)) {
        foreach ($value as $file) {
          if (isset($file['storage']) && !empty($file['data']['id'])) {
            $id = $file['data']['id'];
            $attachment = $repo->find($id);
            if ($attachment instanceof Allegato) {
              if (!empty($file['fileType'])) {
                $attachment->setDescription($file['fileType']);
                $this->entityManager->persist($attachment);
              }

              // Imposto il proprietario per gli allegati appena creati se non presente
              if (!$attachment->getOwner() instanceof CPSUser) {
                $attachment->setOwner($pratica->getUser());
                $this->entityManager->persist($attachment);
              }

              $attachments[] = $id;
              $pratica->addAllegato($attachment);
            } else {
              $msg = "The file present in form schema doesn't exist in database";
              $this->logger->error($msg, ['pratica' => $pratica->getId(), 'allegato' => $id]);
              throw new \Exception($this->translator->trans('steps.formio.attachments_violation_message'));
            }
          }
        }
      }
    }

    // Rimuovo gli allegati che non sono più presenti nella pratica
    foreach ($currentAttachments as $attachment) {
      if (!in_array($attachment->getId(), $attachments, true)) {
        $pratica->removeAllegato($attachment);
      }
    }

    // Verifico che il numero degli allegati associati alla pratica sia uguale a quello passato nel form
    if ($pratica->getAllegati()->count() !== count($attachments)) {
      $msg = 'The number of files in form data is not equal to those linked to the application';
      $this->logger->error($msg, ['pratica' => $pratica->getId()]);
      throw new \Exception($this->translator->trans('steps.formio.attachments_violation_message'));
    }
  }

  /**
   * @param Pratica $pratica
   * @return int
   */
  public function countAttachments(Pratica $pratica): int
  {
    $count = 0;
    $count += $pratica->getModuliCompilati()->count();
    // Include sia allegati che risposte a integrazione
    $count += $pratica->getAllegati()->count();

    $count += $pratica->getRichiesteIntegrazione()->count();

    if ($pratica->getRispostaOperatore()) {
      $count++;
    }
    $count += $pratica->getAllegatiOperatore()->count();

    return $count;
  }

  /**
   * @param Pratica $pratica
   * @return array
   */
  public function getGroupedModuleFiles(Pratica $pratica): array
  {
    $files = [];
    if ($pratica->getServizio()->isLegacy()) {
      return $files;
    }
    $attachments = $pratica->getAllegatiWithIndex();
    $schema = $this->schemaFactory->createFromService($pratica->getServizio());
    $filesComponents = $schema->getFileComponents();
    $data = $pratica->getDematerializedForms();
    $flatData = $data['flattened'];

    $componentsByName = [];
    foreach ($filesComponents as $component) {
      $componentsByName[$component->getName()]= $component;
    }

    foreach ($flatData as $key => $data) {
      if (!is_array($data) || empty($data)) {
        continue;
      }
      // Todo: lo schema factory crea delle chiavi senza 'data' mentre il flat data lo contiene
      // Todo: Fix veloce bonifichiamo le chiavi del flat data per fare il match, da migliorare
      $simplifiedKey = FormIOUtils::simplifyFlattenedKey($key);
      if (isset($componentsByName[$simplifiedKey])) {
        $component = $componentsByName[$simplifiedKey];
        $componentOptions = $component->getFormOptions();
        $labelParts = explode('/', $componentOptions['label']);
        $page = $labelParts[0];
        unset($labelParts[0]);
        $label = implode(' / ', $labelParts);
        foreach ($data as $f) {
          if (isset($f['storage'])) {
            $id = $f['data']['id'];
            $files [$page][$label][] = $attachments[$id];
          }
        }
      }
    }

    return $files;
  }

  public function getUserEmail(array $data, CPSUser $user)
  {
    return $data['flattened']['applicant.data.email_address'] ?? $data['flattened']['email_address']
      ?? $data['flattened']['email'] ?? $user->getEmailContatto();
  }

  /**
   * @throws AlreadyScheduledException
   */
  public function regenerateModule(Pratica $pratica): void
  {
    // Programmo la rigenerazione del modulo pdf
    $this->moduloPdfBuilderService->updateForPraticaAsync($pratica);
  }

  /**
   * @param Pratica $pratica
   * @return bool
   */
  public function isModuleUpdating(Pratica $pratica): bool
  {
    // Verifico se c'è una scheduled action per rigenerare il pdf
    $isModuleUpdating = false;
    try {
      $sql = "select id from scheduled_action where type = '" . ModuloPdfBuilderService::SCHEDULED_UPDATE_FOR_PRATICA . "' AND params::text LIKE '%" . $pratica->getId() . "%' and status = '" . ScheduledAction::STATUS_PENDING . "'";
      $stmt = $this->entityManager->getConnection()->prepare($sql);
      $results = $stmt->executeQuery()->rowCount();
      $isModuleUpdating = $results > 0;
    } catch (\Doctrine\DBAL\Driver\Exception $e) {
    } catch (\Doctrine\DBAL\Exception $e) {
      $this->logger->error("Error retrieving scheduled action to regenerate pdf for application {$pratica->getId()}: {$e->getMessage()}");
    }
    return $isModuleUpdating;
  }

  /**
   * @param Pratica $pratica
   * @param bool|null $completeHistory
   * @return array
   */
  public function getApplicationHistory(Pratica $pratica, ?bool $completeHistory = true): array
  {
    $history = [];

    $previous = false;

    foreach ($pratica->getStoricoStati() as $timestamp => $v) {
      $transition = new Transition();
      $transition->setDate((new DateTime())->setTimestamp($timestamp));

      foreach ($v as $change) {
        // Da verificare, ci sono state delle history senza gli giusti elementi
        if (isset($change[0])) {
          $statusCode = $change[0];
          $statusChange = new StatusChange($change[1] ?? null);
          $transition->setStatusCode($statusCode);
          $transition->setStatusName(strtolower($pratica->getStatusNameByCode($statusCode)));
          $transition->setMessageId($statusChange->getMessageId());

          if ($completeHistory) {
            $transition->setMessage($statusChange->getMessage());
          }

          // Nome del cambio di stato automatico
          $description = $this->translator->trans('pratica.dettaglio.stato_' . $statusCode);

          if ($statusCode === Pratica::STATUS_PENDING) {
            if (!$completeHistory && $statusChange->getUserGroup()) {
              // Vista cittadino: mostro l'assegnatario solo se è un ufficio
              // Ex: Presa in carico da Ufficio
              $description = $this->translator->trans('pratica.dettaglio.iter.auto_assigned_from', ['%assignee%' => $statusChange->getUserGroup()]);
            } elseif ($statusChange->getAssigner()) {
              // Vista operatore: se ho un assegnatario lo mostro
              // Ex: Assegnata a Operatore A (Ufficio) da Operatore B
              $description = $this->translator->trans('pratica.dettaglio.iter.assigned_to_from', ['%assigner%' => $statusChange->getAssigner(), '%assignee%' => $statusChange->getAssignee()]);
            } elseif ($statusChange->getOperatore() && $completeHistory) {
              // Vista operatore: auto assegnazione da parte di un operatore
              $description = $this->translator->trans('pratica.dettaglio.iter.auto_assigned_from', ['%assignee%' => $statusChange->getAssignee()]);
            }
          }
          $transition->setDescription($description);
        }
      }

      if ($completeHistory || (!$previous or $previous !== $transition->getDescription())) {
        // Se la descrizione del cambio di stato è duplicata non l'aggiungo alla storia
        $history[] = $transition;
      }

      $previous = $transition->getDescription();
    }

    return $history;
  }

  public function selectPaymentGateway(FormIO $application): void
  {
    $service = $application->getServizio();
    $paymentParameters = $service->getPaymentParameters();
    $selectedGateways = $paymentParameters['gateways'] ?? [];
    if (count($selectedGateways) > 0) {
      $identifier = reset($selectedGateways)['identifier'];
      if ($identifier) {
        $application->setPaymentType($identifier);
        $this->entityManager->persist($application);
        $this->entityManager->flush();
        if ($identifier !== Bollo::IDENTIFIER && $application->getStatus() != Pratica::STATUS_PAYMENT_PENDING) {
          if ($identifier !== MyPay::IDENTIFIER) {
            $application->setPaymentData($this->paymentService->createPaymentData($application));
          }
          $this->entityManager->persist($application);
          $this->entityManager->flush();
          $this->praticaStatusService->setNewStatus($application, Pratica::STATUS_PAYMENT_PENDING);
        }
      }
    }
  }

}
