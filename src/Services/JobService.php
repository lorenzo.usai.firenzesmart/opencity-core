<?php

namespace App\Services;

use App\Entity\Job;
use App\Entity\ScheduledAction;
use App\Handlers\Job\JobHandlerInterface;
use App\Handlers\Job\JobHandlerRegistry;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

class JobService implements ScheduledActionHandlerInterface
{
  const ACTION_PROCESS_JOB = 'process_job';

  private ScheduleActionService $scheduleActionService;
  private EntityManagerInterface $entityManager;
  private JobHandlerRegistry $jobHandlerRegistry;
  private LoggerInterface $logger;


  /**
   * @param ScheduleActionService $scheduleActionService
   * @param EntityManagerInterface $entityManager
   * @param JobHandlerRegistry $jobHandlerRegistry
   * @param LoggerInterface $logger
   */
  public function __construct(ScheduleActionService $scheduleActionService, EntityManagerInterface $entityManager, JobHandlerRegistry $jobHandlerRegistry, LoggerInterface $logger)
  {
    $this->scheduleActionService = $scheduleActionService;
    $this->entityManager = $entityManager;
    $this->jobHandlerRegistry = $jobHandlerRegistry;
    $this->logger = $logger;
  }


  public function processJobAsync(Job $job)
  {

    $params = [
      'job' => $job->getId()->toString()
    ];

    try {
      $this->scheduleActionService->appendAction(
        'ocsdc.job_service',
        self::ACTION_PROCESS_JOB,
        serialize($params)
      );
    } catch (AlreadyScheduledException $e) {
      $this->logger->error('Job is already scheduled', $params);
    }
  }

  /**
   * @throws Exception|GuzzleException
   */
  public function executeScheduledAction(ScheduledAction $action)
  {
    $params = unserialize($action->getParams());
    if ($action->getType() == self::ACTION_PROCESS_JOB) {
      /** @var Job $job */
      $job = $this->entityManager->getRepository('App\Entity\Job')->find($params['job']);
      if (!$job instanceof Job) {
        throw new Exception('Not found job with id: ' . $params['job']);
      }

      $handler = $this->jobHandlerRegistry->getByName($job->getType());
      if ($handler instanceof JobHandlerInterface) {

        if ($job->getStatus() === Job::STATUS_PENDING && empty($job->getStartedAt())) {
          $job->setRunning();
          $this->entityManager->persist($job);
          $this->entityManager->flush();
        }
        $handler->processJob($job);
      }
    }
  }
}
