<?php

namespace App\Services;

use App\Entity\AllegatoOperatore;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\ModuloCompilato;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\Subscriber;
use App\Entity\User;
use App\Exception\MessageDisabledException;
use App\Exception\SendMessageException;
use App\Model\FeedbackMessage;
use App\Model\FeedbackMessagesSettings;
use App\Model\Mailer;
use App\Model\SubscriberMessage;
use App\Model\Transition;
use App\Services\FileService\AllegatoFileService;
use App\Utils\LocaleUtils;
use Exception;
use League\Flysystem\FileNotFoundException;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Doctrine\Persistence\ManagerRegistry;
use Swift_Message;
use Twig\Environment;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\Error;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MailerService
{

  /**
   * @var Swift_Mailer $mailer
   */
  private Swift_Mailer $mailer;

  /**
   * @var IOService
   */
  private IOService $ioService;

  /**
   * @var TranslatorInterface $translator
   */
  private TranslatorInterface $translator;

  /**
   * @var Environment
   */
  private Environment $templating;

  /**
   * @var ManagerRegistry
   */
  private ManagerRegistry $doctrine;

  /**
   * @var LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * @var PraticaPlaceholderService
   */
  private PraticaPlaceholderService $praticaPlaceholderService;

  private array $blacklistedStates = [
    Pratica::STATUS_REQUEST_INTEGRATION,
    Pratica::STATUS_PROCESSING,
    Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION,
    Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE,
    Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE,
  ];

  private array $blacklistedNotLegacyStates = [
    Pratica::STATUS_DRAFT_FOR_INTEGRATION,
  ];

  private array $blacklistedDuplicatetates = [
    Pratica::STATUS_PENDING,
  ];

  /**
   * @var AllegatoFileService
   */
  private AllegatoFileService $fileService;

  private SmtpTransportService $smtpTransportService;

  /**
   * MailerService constructor.
   * @param Swift_Mailer $mailer
   * @param TranslatorInterface $translator
   * @param Environment $templating
   * @param ManagerRegistry $doctrine
   * @param LoggerInterface $logger
   * @param IOService $ioService
   * @param PraticaPlaceholderService $praticaPlaceholderService
   * @param AllegatoFileService $fileService
   * @param SmtpTransportService $smtpTransportService
   */
  public function __construct(
    Swift_Mailer $mailer,
    TranslatorInterface $translator,
    Environment $templating,
    ManagerRegistry $doctrine,
    LoggerInterface $logger,
    IOService $ioService,
    PraticaPlaceholderService $praticaPlaceholderService,
    AllegatoFileService $fileService,
    SmtpTransportService $smtpTransportService
  ) {
    $this->mailer = $mailer;
    $this->translator = $translator;
    $this->templating = $templating;
    $this->doctrine = $doctrine;
    $this->logger = $logger;
    $this->ioService = $ioService;
    $this->praticaPlaceholderService = $praticaPlaceholderService;
    $this->fileService = $fileService;
    $this->smtpTransportService = $smtpTransportService;
  }

  /**
   * @param Pratica $pratica
   * @param $fromAddress
   * @param bool $resend
   * @return int
   */
  public function dispatchMailForPratica(Pratica $pratica, $fromAddress, bool $resend = false): int
  {
    $sentAmount = 0;
    if (in_array($pratica->getStatus(), $this->blacklistedStates)) {
      return $sentAmount;
    }

    // Nel caso di pratiche Formio voglio disabilitare anche l'invio in alcuni stati perchè gestite tramite messaggi
    if (!$pratica->getServizio()->isLegacy() && in_array($pratica->getStatus(), $this->blacklistedNotLegacyStates)) {
      return $sentAmount;
    }

    $sendCPSUserMessage = true;

    if (in_array($pratica->getStatus(), $this->blacklistedDuplicatetates)) {
      // Check if current status exists in application history more than once
      foreach ($pratica->getHistory() as $item) {
        /** @var Transition $item */
        if ($item->getStatusCode() == $pratica->getStatus()
          && $item->getDate()->getTimestamp(
          ) !== $pratica->getLatestStatusChangeTimestamp()) {
          $sendCPSUserMessage = false;
        }
      }
    }

    if (($resend || !$this->CPSUserHasAlreadyBeenWarned($pratica)) && $sendCPSUserMessage) {
      try {
        /** @var Swift_Message $CPSUserMessage */
        $CPSUserMessage = $this->setupCPSUserMessage($pratica, $fromAddress);
        $sentAmount += $this->send($CPSUserMessage);
        $pratica->setLatestCPSCommunicationTimestamp(time());

        // Invio via pec
        if ($CPSUserMessage instanceof Swift_Message) {
          $this->dispatchPecEmail($pratica, $CPSUserMessage);
        }

        // Todo: centralizzare creazione del messaggio, separare logica di spedizione
        if ($pratica->getServizio()->isIOEnabled()) {
          $CPSUserMessage = $this->setupCPSUserMessage($pratica, $fromAddress, true);
          $sentAmount += $this->ioService->sendMessageForPratica(
            $pratica,
            $CPSUserMessage,
            $this->translator->trans(
              'pratica.email.status_change.subject',
              ['%id%' => $pratica->getId()],
              null,
              $pratica->getLocale() ?? 'it'
            )
          );
        }
      } catch (SendMessageException $e) {
        $this->logger->error(
          'Error sending message in dispatchMailForPratica  ' . $e->getMessage(),
          ['pratica' => $pratica->getId(), 'email' => $pratica->getUser()->getEmailContatto()]
        );
      } catch (MessageDisabledException $e) {
        $this->logger->info(
          'Notification disabled for current status ' . $e->getMessage(),
          ['pratica' => $pratica->getId(), 'email' => $pratica->getUser()->getEmailContatto()]
        );
      } catch (Exception $e) {
        $this->logger->error(
          'Error in dispatchMailForPratica ' . $e->getMessage(),
          ['pratica' => $pratica->getId(), 'email' => $pratica->getUser()->getEmailContatto()]
        );
      }
    }

    /**
     *Todo: se la pratica è in stato submitted (ancora non ha associato un operatore)
     *  - recuperare indirizzi email degli operatori abilitati alla pratica
     *  - inviare email ad operatori recuperati
     */

    $operatorsToNotify = [];
    if ($pratica->getStatus() == Pratica::STATUS_SUBMITTED || $pratica->getStatus() == Pratica::STATUS_REGISTERED) {
      // Recupero gli operatori abilitati al servizio
      $qb = $this->doctrine->getManager()->createQueryBuilder()
        ->select('operator')
        ->from('App:OperatoreUser', 'operator')
        ->where('operator.serviziAbilitati LIKE :serviceId')
        ->setParameter('serviceId', '%' . $pratica->getServizio()->getId() . '%');
      $operatorsToNotify = $qb->getQuery()->getResult();

      // Recupero tutti gli operatori appartenenti agli uffici associati al servizio
      foreach ($pratica->getServizio()->getUserGroups() as $userGroup) {
        foreach ($userGroup->getUsers() as $user) {
          if (!in_array($user, $operatorsToNotify, true)) {
            $operatorsToNotify[] = $user;
          }
        }
      }
    } elseif ($pratica->getStatus() != Pratica::STATUS_PRE_SUBMIT) {
      if ($pratica->getOperatore() != null && ($resend || !$this->operatoreUserHasAlreadyBeenWarned($pratica))) {
        // Invio email all'operatore che ha in carico la pratica
        $operatorsToNotify[] = $pratica->getOperatore();
      } elseif ($pratica->getUserGroup()) {
        // Invio email a tutti gli operatori appartenenti al gruppo che ha in carico la pratica
        $operatorsToNotify = $pratica->getUserGroup()->getUsers()->toArray();
      }
    }

    if (!empty($operatorsToNotify)) {
      // Imposto destinatario principale e cc.
      // NB: se la pratica è un carico ad un operatore non vengono notificati gli operatori dell'ufficio
      $receiver = array_shift($operatorsToNotify);
      $bccReceivers = $operatorsToNotify;

      try {
        if (empty($fromAddress) && !empty($bccReceivers)) {
          $fromAddress = $bccReceivers[0];
        } elseif (empty($fromAddress)) {
          throw new  SendMessageException('Error no email receiver found for message. ');
        }
        $operatoreUserMessage = $this->setupOperatoreUserMessage($pratica, $fromAddress, $receiver, $bccReceivers);
        $sentAmount += $this->send($operatoreUserMessage);
        $pratica->setLatestOperatoreCommunicationTimestamp(time());
      } catch (SendMessageException $e) {
        $this->logger->error(
          'Error sending message in dispatchMailForPratica (Operators) ' . $e->getMessage(),
          ['pratica' => $pratica->getId(), 'email' => $receiver->getEmail(), 'bcc_count' => count($bccReceivers)]
        );
      } catch (Exception $e) {
        $this->logger->error(
          'Error creating message in dispatchMailForPratica ' . $e->getMessage(),
          ['pratica' => $pratica->getId(), 'email' => $receiver->getEmail(), 'bcc_count' => count($bccReceivers)]
        );
      }
    }

    $this->logger->info($sentAmount . ' emails sent in dispatchMailForPratica - Pratica: ' . $pratica->getId());
    return $sentAmount;
  }

  /**
   * @param Swift_Message $message
   * @return int
   * @throws SendMessageException
   */
  private function send(Swift_Message $message): int
  {
    try {
      $failed = [];
      $count = $this->mailer->send($message, $failed);
      if (count($failed) > 0) {
        throw new Exception(implode(',', $failed));
      }
      return $count;
    } catch (Exception $e) {
      $transport = $this->mailer->getTransport();
      if ($transport instanceof \Swift_Transport_AbstractSmtpTransport){
        $transport->reset();
      }
      $this->smtpTransportService->sendEmailAsync($message);
      throw new SendMessageException($e->getMessage());
    }
  }

  /**
   * @param Pratica $pratica
   * @return bool
   */
  private function CPSUserHasAlreadyBeenWarned(Pratica $pratica): bool
  {
    return $pratica->getLatestCPSCommunicationTimestamp() >= $pratica->getLatestStatusChangeTimestamp();
  }

  /**
   * @param Pratica $pratica
   * @param $fromAddress
   * @param bool $textOnly
   * @return string|Swift_Message
   * @throws FileNotFoundException
   * @throws LoaderError
   * @throws MessageDisabledException
   * @throws RuntimeError
   * @throws SyntaxError
   */
  private function setupCPSUserMessage(Pratica $pratica, $fromAddress, bool $textOnly = false)
  {
    $toEmail = $pratica->getUser()->getEmailContatto();
    $toName = $pratica->getUser()->getFullName();

    $locale = $pratica->getLocale() ?? 'it';

    $ente = $pratica->getEnte();
    $ente->setTranslatableLocale($locale);
    $this->doctrine->getManager()->refresh($ente);

    $fromName = $ente instanceof Ente ? $ente->getName() : null;
    $service = $pratica->getServizio();
    $service->setTranslatableLocale($locale);
    $this->doctrine->getManager()->refresh($service);
    $feedbackMessages = $service->getFeedbackMessages();

    if (!isset($feedbackMessages[$pratica->getStatus()])) {
      return $this->setupCPSUserMessageFallback($pratica, $fromAddress, $textOnly, $locale);
    }

    /** @var FeedbackMessage $feedbackMessage */
    $feedbackMessage = $feedbackMessages[$pratica->getStatus()];
    if (
      (isset($feedbackMessage['isActive']) && !$feedbackMessage['isActive'])
      || (isset($feedbackMessage['is_active']) && !$feedbackMessage['is_active'])) {
      throw new MessageDisabledException('Message for ' . $pratica->getStatus() . ' is not active');
    }

    $placeholders = $this->praticaPlaceholderService->getPlaceholders($pratica);

    if ($textOnly) {
      return strtr($feedbackMessage['message'], $placeholders);
    }

    if (!empty($feedbackMessage['subject'])) {
      $subject = strip_tags(strtr($feedbackMessage['subject'], $placeholders));
    } else {
      $subject = $this->translator->trans('pratica.email.status_change.subject', ['%id%' => $pratica->getId()], null, $locale);
    }

    $textHtml = $this->templating->render(
      'Emails/User/feedback_message.html.twig',
      [
        'pratica' => $pratica,
        'placeholder' => $placeholders,
        'text' => strtr($feedbackMessage['message'], $placeholders),
        'locale' => $locale,
      ]
    );

    $textPlain = strip_tags($textHtml);

    $message = (new Swift_Message())
      ->setSubject($subject)
      ->setFrom($fromAddress, $fromName)
      ->setTo($toEmail, $toName)
      ->setBody($textHtml, 'text/html')
      ->addPart($textPlain, 'text/plain');

    $this->addAttachments($pratica, $message);

    return $message;
  }

  /**
   * @param Pratica $pratica
   * @param $fromAddress
   * @param bool $textOnly
   * @param $locale
   * @return Swift_Message|string
   * @throws FileNotFoundException
   * @throws LoaderError
   * @throws RuntimeError
   * @throws SyntaxError
   */
  private function setupCPSUserMessageFallback(
    Pratica $pratica,
    $fromAddress,
    $textOnly,
    $locale
  ) {
    $toEmail = $pratica->getUser()->getEmailContatto();
    $toName = $pratica->getUser()->getFullName();

    $ente = $pratica->getEnte();
    $ente->setTranslatableLocale($locale);
    $this->doctrine->getManager()->refresh($ente);

    $fromName = $ente instanceof Ente ? $ente->getName() : null;

    $submissionTime = $pratica->getSubmissionTime() ? (new \DateTime())->setTimestamp(
      $pratica->getSubmissionTime()
    ) : null;
    $protocolTime = $pratica->getProtocolTime() ? (new \DateTime())->setTimestamp($pratica->getProtocolTime()) : null;

    $placeholders = [
      'pratica' => $pratica,
      'user_name' => $pratica->getUser()->getFullName(),
      'data_acquisizione' => $submissionTime ? $submissionTime->format('d/m/Y') : "",
      'ora_acquisizione' => $submissionTime ? $submissionTime->format('H:i:s') : "",
      'data_protocollo' => $protocolTime ? $protocolTime->format('d/m/Y') : "",
      'ora_protocollo' => $protocolTime ? $protocolTime->format('H:i:s') : "",
      'data_corrente' => (new \DateTime())->format('d/m/Y'),
      'locale' => $locale,
    ];

    if ($textOnly) {
      return $this->templating->render(
        'Emails/User/pratica_status_change.txt.twig',
        $placeholders
      );
    }

    $message = (new Swift_Message())
      ->setSubject(
        $this->translator->trans('pratica.email.status_change.subject', ['%id%' => $pratica->getId()], null, $locale)
      )
      ->setFrom($fromAddress, $fromName)
      ->setTo($toEmail, $toName)
      ->setBody(
        $this->templating->render(
          'Emails/User/pratica_status_change.html.twig',
          $placeholders
        ),
        'text/html'
      )
      ->addPart(
        $this->templating->render(
          'Emails/User/pratica_status_change.txt.twig',
          $placeholders
        ),
        'text/plain'
      );

    $this->addAttachments($pratica, $message);

    return $message;
  }

  /**
   * @param Pratica $pratica
   * @param $fromAddress
   * @param OperatoreUser|null $operatore
   * @param OperatoreUser[] $bccOperators
   * @return Swift_Message
   * @throws LoaderError
   * @throws RuntimeError
   * @throws SyntaxError
   * @throws Exception
   */
  private function setupOperatoreUserMessage(
    Pratica $pratica,
    $fromAddress,
    OperatoreUser $operatore = null,
    array $bccOperators = []
  ): Swift_Message {
    if (empty($operatore) && empty($bccOperators)) {
      throw new Exception('Both operator and ccOperators are empty in setupOperatoreUserMessage');
    }

    if (empty($operatore)) {
      $operatore = array_shift($bccOperators);
    }

    $addresses = [];
    foreach ($bccOperators as $operator) {
      if (!empty($operator->getEmail()) && !in_array($operator->getEmail(), $addresses)) {
        $addresses[] = $operator->getEmail();
      }
    }

    $locale = $pratica->getLocale() ?? 'it';
    $ente = $pratica->getEnte();
    $ente->setTranslatableLocale($locale);
    $this->doctrine->getManager()->refresh($ente);
    $fromName = $ente instanceof Ente ? $ente->getName() : null;

    $message = (new Swift_Message())
      ->setSubject($this->translator->trans('pratica.email.status_change.subject', ['%id%' => $pratica->getId()], null, $locale))
      ->setFrom($fromAddress, $fromName)
      ->setTo($operatore->getEmail(), $operatore->getFullName())
      ->setBody(
        $this->templating->render(
          'Emails/Operatore/pratica_status_change.html.twig',
          [
            'pratica' => $pratica,
            'user_name' => $pratica->getOperatore() ? $pratica->getOperatore()->getFullName() : null,
            'user_group_name' => $pratica->getUserGroup() ? $pratica->getUserGroup()->getName() : null,
            'locale' => $pratica->getLocale()
          ]
        ),
        'text/html'
      )
      ->addPart(
        $this->templating->render(
          'Emails/Operatore/pratica_status_change.txt.twig',
          [
            'pratica' => $pratica,
            'user_name' => $pratica->getOperatore() ? $pratica->getOperatore()->getFullName() : null,
            'user_group_name' => $pratica->getUserGroup() ? $pratica->getUserGroup()->getName() : null,
            'locale' => $pratica->getLocale()
          ]
        ),
        'text/plain'
      );

    if (!empty($addresses)) {
      $message->setBcc($addresses);
    }

    return $message;
  }

  /**
   * @param Pratica $pratica
   * @return bool
   */
  private function operatoreUserHasAlreadyBeenWarned(
    Pratica $pratica
  ): bool {
    return $pratica->getLatestOperatoreCommunicationTimestamp() >= $pratica->getLatestStatusChangeTimestamp();
  }

  /**
   * @param $fromAddress
   * @param $fromName
   * @param $toAddress
   * @param $toName
   * @param $message
   * @param $subject
   * @param Ente $ente
   * @param array $callToActions
   * @param array $bccReceivers
   * @return int
   */
  public function dispatchMail(
    $fromAddress,
    $fromName,
    $toAddress,
    $toName,
    $message,
    $subject,
    Ente $ente,
    array $callToActions = [],
    array $bccReceivers = [],
    $locale = LocaleUtils::DEFAULT_LOCALE
  ): int {
    $sentAmount = 0;
    if ($this->isValidEmail($toAddress)) {
      try {
        $emailMessage = (new Swift_Message())
          ->setSubject($subject)
          ->setFrom($fromAddress, $fromName)
          ->setTo($toAddress, $toName)
          ->setBcc($bccReceivers)
          ->setBody(
            $this->templating->render(
              'Emails/General/message.html.twig',
              [
                'message' => $message,
                'ente' => $ente,
                'call_to_actions' => $callToActions,
                'locale' => $locale
              ]
            ),
            'text/html'
          )
          ->addPart(
            $this->templating->render(
              'Emails/General/message.txt.twig',
              [
                'message' => $message,
                'ente' => $ente,
              ]
            ),
            'text/plain'
          );
        $sentAmount += $this->send($emailMessage);
      } catch (SendMessageException $e) {
        $this->logger->error('Error sending message in dispatchMail: Email: ' . $toAddress . ' - ' . $e->getMessage());
      } catch (Exception $e) {
        $this->logger->error('Error creating message in dispatchMail: Email: ' . $toAddress . ' - ' . $e->getMessage());
      }
    } else {
      $this->logger->info('Email: ' . $toAddress . ' is not valid.');
    }

    return $sentAmount;
  }

  /**
   * @param Pratica $pratica
   * @param Swift_Message $message
   */
  public function dispatchPecEmail(Pratica $pratica, Swift_Message $message)
  {
    /** @var FeedbackMessagesSettings $feedbackMessageSettings */
    $feedbackMessageSettings = $pratica->getServizio()->getFeedbackMessagesSettings();
    if ($feedbackMessageSettings != null && $feedbackMessageSettings->getPecMailer() != 'disabled') {
      try {
        /** @var Mailer $instanceMailer */
        $instanceMailer = $pratica->getServizio()->getEnte()->getMailer($feedbackMessageSettings->getPecMailer());

        if (!$instanceMailer instanceof Mailer) {
          throw new Exception('There are no mailers on instance');
        }

        $transport = (new \Swift_SmtpTransport($instanceMailer->getHost(), $instanceMailer->getPort()))
          ->setUsername($instanceMailer->getUser())
          ->setPassword($instanceMailer->getPassword())
          ->setEncryption($instanceMailer->getEncription());

        // Create the Mailer using your created Transport
        $pecMailer = new Swift_Mailer($transport);

        $submission = PraticaPlaceholderService::getFlattenedSubmission($pratica);
        // Recupero indirizzo email da campo segnalato in pec_receiver
        if (!isset($submission[$feedbackMessageSettings->getPecReceiver()])) {
          $this->logger->error('Error in dispatchPecEmail: empty pec receiver field');
          return;
        }
        $receiver = $submission[$feedbackMessageSettings->getPecReceiver()];

        if (!$this->isValidEmail($receiver)) {
          $this->logger->error('Error in dispatchPecEmail: pec receiver is not a valid email ' . $receiver);
          return;
        }
        $message->setTo($receiver);
        $message->setFrom($instanceMailer->getSender());
        $failed = [];
        $pecMailer->send($message, $failed);
        if (count($failed) > 0) {
          throw new Exception(implode(',', $failed));
        }
      } catch (Exception $e) {
        $this->logger->error(
          'Error in dispatchPecEmail ' . $e->getMessage(),
          ['pratica' => $pratica->getId(), 'email' => $pratica->getUser()->getEmailContatto()]
        );
      }
    }
  }

  /**
   * @param $email
   * @return mixed
   */
  private function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  /**
   * @param SubscriberMessage $subscriberMessage
   * @param $fromAddress
   * @param OperatoreUser $operatore
   * @return int
   */
  public function dispatchMailForSubscriber(
    SubscriberMessage $subscriberMessage,
    $fromAddress,
    User $operatore
  ): int {
    $sentAmount = 0;

    if ($this->SubscriberHasValidContactEmail($subscriberMessage->getSubscriber())) {
      try {
        $message = $this->setupSubscriberMessage($subscriberMessage, $fromAddress, $operatore);
        $sentAmount += $this->send($message);
      } catch (SendMessageException $e) {
        $this->logger->error(
          'Error sending in dispatchMailForSubscriber ' . $e->getMessage(),
          ['email' => $subscriberMessage->getSubscriber()->getEmail()]
        );
      } catch (Exception $e) {
        $this->logger->error(
          'Error creating in dispatchMailForSubscriber ' . $e->getMessage(),
          ['email' => $subscriberMessage->getSubscriber()->getEmail()]
        );
      }
    }

    return $sentAmount;
  }

  /**
   * @param Subscriber $subscriber
   * @return mixed
   */
  private function SubscriberHasValidContactEmail(
    Subscriber $subscriber
  ) {
    $email = $subscriber->getEmail();
    return $this->isValidEmail($email);
  }

  /**
   * @param SubscriberMessage $subscriberMessage
   * @param $fromAddress
   * @param OperatoreUser $operatoreUser
   * @return Swift_Message
   * @throws Error
   */
  private function setupSubscriberMessage(
    SubscriberMessage $subscriberMessage,
    $fromAddress,
    User $operatoreUser
  ): Swift_Message {
    $toEmail = $subscriberMessage->getSubscriber()->getEmail();
    $toName = $subscriberMessage->getFullName();

    $ente = $operatoreUser->getEnte();
    $fromName = $ente instanceof Ente ? $ente->getName() : null;

    $emailMessage = (new Swift_Message())
      ->setSubject($subscriberMessage->getSubject())
      ->setFrom($fromAddress, $fromName)
      ->setTo($toEmail, $toName)
      ->setBcc($operatoreUser->getEmail(), $operatoreUser->getFullName())
      ->setBody(
        $this->templating->render(
          'Emails/Subscriber/subscriber_message.html.twig',
          [
            'message' => $subscriberMessage->getMessage(),
          ]
        ),
        'text/html'
      )
      ->addPart(
        $this->templating->render(
          'Emails/Subscriber/subscriber_message.txt.twig',
          [
            'message' => $subscriberMessage->getMessage(),
          ]
        ),
        'text/plain'
      );
    if ($subscriberMessage->getAutoSend()) {
      $emailMessage->setCc($operatoreUser->getEmail(), $operatoreUser->getFullName());
    }

    return $emailMessage;
  }

  /**
   * @param Pratica $pratica
   * @param Swift_Message $message
   * @throws FileNotFoundException
   */
  private function addAttachments(
    Pratica $pratica,
    Swift_Message $message
  ) {
    // Send attachment to user if status is submitted
    if ($pratica->getStatus() == Pratica::STATUS_SUBMITTED && $pratica->getModuliCompilati()->count() > 0) {
      /** @var ModuloCompilato $moduloCompilato */
      $moduloCompilato = $pratica->getModuliCompilati()->first();
      if ($this->fileService->attachmentExists($moduloCompilato)) {
        $attachment = new \Swift_Attachment(
          $this->fileService->getAttachmentContent($moduloCompilato),
          $moduloCompilato->getFile()->getFilename(),
          $this->fileService->getAttachmentMimeType($moduloCompilato)
        );
        $message->attach($attachment);
      }
    }

    // Send operator attachment to user if status is complete
    if ($pratica->getStatus() == Pratica::STATUS_COMPLETE && $pratica->getAllegatiOperatore()->count() > 0) {
      /** @var AllegatoOperatore $allegato */
      foreach ($pratica->getAllegatiOperatore() as $allegato) {
        if ($this->fileService->attachmentExists($allegato)) {
          $attachment = new \Swift_Attachment(
            $this->fileService->getAttachmentContent($allegato),
            $allegato->getFile()->getFilename(),
            $this->fileService->getAttachmentMimeType($allegato)
          );
          $message->attach($attachment);
        }
      }
    }
  }
}
