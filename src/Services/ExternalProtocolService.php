<?php

namespace App\Services;

use App\Entity\OperatoreUser;
use App\Services\Manager\UserManager;
use App\Utils\StringUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExternalProtocolService
{
  const OPERATOR_USERNAME = 'protocollo';
  const OPERATOR_TMP_PASSWORD = 'changeme';

  /** @var InstanceService */
  private InstanceService $instanceService;

  /** @var RouterInterface */
  private RouterInterface $router;

  /** @var EntityManagerInterface */
  private EntityManagerInterface $entityManager;

  /** @var UserPasswordEncoderInterface */
  private UserPasswordEncoderInterface $passwordEncoder;

  /** @var LoggerInterface  */
  private LoggerInterface $logger;

  /** @var TranslatorInterface  */
  private TranslatorInterface $translator;

  /** @var UserManager  */
  private UserManager $userManager;


  /**
   * @param InstanceService $instanceService
   * @param RouterInterface $router
   * @param EntityManagerInterface $entityManager
   * @param UserPasswordEncoderInterface $passwordEncoder
   * @param LoggerInterface $logger
   * @param TranslatorInterface $translator
   * @param UserManager $userManager
   */
  public function __construct(
    InstanceService $instanceService,
    RouterInterface $router,
    EntityManagerInterface $entityManager,
    UserPasswordEncoderInterface $passwordEncoder,
    LoggerInterface $logger,
    TranslatorInterface $translator,
    UserManager $userManager
  )
  {
    $this->instanceService = $instanceService;
    $this->router = $router;
    $this->entityManager = $entityManager;
    $this->passwordEncoder = $passwordEncoder;
    $this->logger = $logger;
    $this->translator = $translator;
    $this->userManager = $userManager;
  }

  /**
   * @throws Exception
   */
  public function setup(array $configuration, $registryIdentifier) {

    $url = $configuration['url'] ?? '';
    if (!$url){
      $this->logger->debug('servizio.no_external_protocol_configuration_needed');
      return null;
    }

    $isTypeProxy = isset($configuration['type']) && $configuration['type'] === 'proxy';

    if ($isTypeProxy) {
      return $this->setupRegistryProxy($configuration, $registryIdentifier);
    } else {
      return $this->setupRegistry($configuration);
    }
  }

  /**
   * @throws Exception
   */
  private function setupRegistryProxy(array $configuration, $registryIdentifier)
  {
    $currentInstance = $this->instanceService->getCurrentInstance();
    $client = new Client();
    $headers = [
      'Content-Type' => 'application/json',
    ];

    foreach($configuration['headers'] ?? [] as $header) {
      $exploded = explode('=', $header);
      $headers[$exploded[0]] = $exploded[1];
    }

    $tenant = null;
    $url = $configuration['url'];
    $requestUrl = $url . '/tenants/' . $currentInstance->getId();

    $request = new Request(
      'GET',
      $requestUrl,
      $headers
    );

    $isNewTenant = false;
    try {
      $response = $client->send($request);
      $tenant = json_decode($response->getBody(), true);
    } catch (GuzzleException $e) {
      // I registry proxy restituiscono 404 se il tenant non è presente, 404 va escluso dalle eccezioni che sollevano un errore
      if ($e->getCode() != 404) {
        $this->logger->error("Error retrieving tenant {$currentInstance->getId()} from external protocol: {$e->getMessage()}");
        throw new Exception($this->translator->trans('servizio.error_retrieving_protocol_tenant'));
      }
    }


    if (!$tenant) {
      if (!$currentInstance->getIpaCode()) {
        $this->logger->error("Error creating tenant {$currentInstance->getId()}: missing IPA Code");
        throw new Exception($this->translator->trans('servizio.missing_ipa_code'));
      }
      if (!$currentInstance->getAooCode()) {
        $this->logger->error("Error creating tenant {$currentInstance->getId()}: missing AOO Code");
        throw new Exception($this->translator->trans('servizio.missing_aoo_code'));
      }

      // Fixme: non ho la password se l'utente non viene creato
      // Se l'operatore protocollo esiste già, ma non è presente una configurazione per il tenant esterna non ho modo
      // di recuperare la password dell'operatore necessaria per la configurazione, imposto quindi una password temporanea
      // e mostro un messaggio che informi l'amministratore della necessità di una modifica manuale


      $payload = [
        'id' => $currentInstance->getId(),
        'description' => $currentInstance->getName(),
        'slug' => $currentInstance->getSlug(),
        'base_url' => rtrim($this->router->generate('api_base', [], UrlGeneratorInterface::ABSOLUTE_URL), '/'),
        'institution_code' => $currentInstance->getIpaCode(),
        'aoo_code' => $currentInstance->getAooCode(),
      ];

      $request = new Request(
        'POST',
        $url .'/tenants/',
        $headers,
        \json_encode($payload)
      );

      try {
        $response = $client->send($request);
        $tenant = json_decode($response->getBody(), true);
        $isNewTenant = true;
      } catch (GuzzleException $e) {
        $this->logger->error("Error creating tenant {$currentInstance->getId()} from external protocol: {$e->getMessage()}");
        throw new Exception($this->translator->trans('servizio.error_creating_protocol_tenant'));
      }
    }

    $this->entityManager->flush();
    return $tenant;
  }

  /**
   * @throws Exception
   */
  private function setupRegistry(array $configuration)
  {
    $currentInstance = $this->instanceService->getCurrentInstance();
    $client = new Client();
    $headers = [
      'Content-Type' => 'application/json',
    ];

    foreach($configuration['headers'] ?? [] as $header) {
      $exploded = explode('=', $header);
      $headers[$exploded[0]] = $exploded[1];
    }

    $tenant = null;
    $url = $configuration['url'];
    $requestUrl = $url . '/tenants/?sdc_id=' . $currentInstance->getId();

    $request = new Request(
      'GET',
      $requestUrl,
      $headers
    );

    $isNewTenant = false;

    try {
      $response = $client->send($request);
      $data = json_decode($response->getBody(), true);
      if ($data['count'] == 1) {
        $tenant = $data['results'][0];
      }
    } catch (GuzzleException $e) {
      $this->logger->error("Error retrieving tenant {$currentInstance->getId()} from external protocol: {$e->getMessage()}");
      throw new Exception($this->translator->trans('servizio.error_retrieving_protocol_tenant'));
    }

    $operatoreRepo = $this->entityManager->getRepository(OperatoreUser::class);
    $username = $tenant ? $tenant['sdc_username'] :  self::OPERATOR_USERNAME;
    $operatoreUser = $operatoreRepo->findOneBy(['username' => $username]);

    $password = null;
    if (!$operatoreUser) {
      $operatoreUser = new OperatoreUser();
      $password = StringUtils::randomPassword();
      $operatoreUser
        ->setEnte($this->instanceService->getCurrentInstance())
        ->setUsername(self::OPERATOR_USERNAME)
        ->setNome(self::OPERATOR_USERNAME)
        ->setCognome(self::OPERATOR_USERNAME)
        ->setSystemUser(true)
        ->setEnabled(true)
        ->setPlainPassword($password)
        ->setPassword($this->passwordEncoder->encodePassword($operatoreUser, $password))
        ->setLastChangePassword(new \DateTime());
    } elseif (!$operatoreUser->isSystemUser()) {
      $operatoreUser->setSystemUser(true);
    }

    $this->userManager->save($operatoreUser, false);

    if (!$tenant) {
      if (!$currentInstance->getIpaCode()) {
        $this->logger->error("Error creating tenant {$currentInstance->getId()}: missing IPA Code");
        throw new Exception($this->translator->trans('servizio.missing_ipa_code'));
      }
      if (!$currentInstance->getAooCode()) {
        $this->logger->error("Error creating tenant {$currentInstance->getId()}: missing AOO Code");
        throw new Exception($this->translator->trans('servizio.missing_aoo_code'));
      }

      // Fixme: non ho la password se l'utente non viene creato
      // Se l'operatore protocollo esiste già, ma non è presente una configurazione per il tenant esterna non ho modo
      // di recuperare la password dell'operatore necessaria per la configurazione, imposto quindi una password temporanea
      // e mostro un messaggio che informi l'amministratore della necessità di una modifica manuale


      $payload = [
        'description' => $currentInstance->getName(),
        'slug' => $currentInstance->getSlug(),
        'sdc_id' => $currentInstance->getId(),
        'sdc_base_url' => rtrim($this->router->generate('api_base', [], UrlGeneratorInterface::ABSOLUTE_URL), '/'),
        'sdc_username' => $operatoreUser->getUsername(),
        'sdc_password' => $password ?? self::OPERATOR_TMP_PASSWORD,
        'sdc_institution_code' => $currentInstance->getIpaCode(),
        'sdc_aoo_code' => $currentInstance->getAooCode(),
        'latest_registration_number' => 0,
        'latest_registration_number_issued_at' => (new DateTime())->format('c'),
        'register_after_date' => (new DateTime())->format('c')
      ];

      $request = new Request(
        'POST',
        $url .'/tenants/',
        $headers,
        \json_encode($payload)
      );

      try {
        $response = $client->send($request);
        $tenant = json_decode($response->getBody(), true);
        $isNewTenant = true;
      } catch (GuzzleException $e) {
        $this->logger->error("Error creating tenant {$currentInstance->getId()} from external protocol: {$e->getMessage()}");
        throw new Exception($this->translator->trans('servizio.error_creating_protocol_tenant'));
      }
    }

    $this->entityManager->flush();

    if (!$password && $isNewTenant) {
      // Operatore già presente, nuovo tenant
      throw new Exception($this->translator->trans('servizio.configuration_needs_technical_support'));
    }

    return $tenant;
  }
}
