<?php

namespace App\Services;


use App\Entity\Allegato;
use App\Entity\GiscomPratica;
use App\Entity\Integrazione;
use App\Entity\IntegrazioneRepository;
use App\Entity\Message;
use App\Entity\ModuloCompilato;
use App\Entity\Pratica;
use App\Entity\RichiestaIntegrazione;
use App\Entity\RichiestaIntegrazioneDTO;
use App\Entity\RispostaIntegrazione;
use App\Entity\RispostaOperatore;
use App\Entity\RispostaOperatoreDTO;
use App\Entity\Ritiro;
use App\Entity\ScheduledAction;
use App\Entity\Servizio;
use App\Entity\SubscriptionPayment;
use App\Model\FeedbackMessage;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use App\Services\FileService\AllegatoFileService;
use App\Utils\StringUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gotenberg\Exceptions\GotenbergApiErroed;
use Gotenberg\Stream;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Vich\UploaderBundle\Mapping\PropertyMappingFactory;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use Gotenberg\Gotenberg;

class ModuloPdfBuilderService implements ScheduledActionHandlerInterface
{

  const MIME_TYPE = 'application/pdf';

  const SCHEDULED_CREATE_FOR_PRATICA = 'createForPratica';
  const SCHEDULED_UPDATE_FOR_PRATICA = 'updateForPratica';

  const PRINTABLE_USERNAME = 'ez';

  const WITHOUT_CHANGE_STATUS = 'no-change-status';

  /** @var FileSystemService */
  private FileSystemService $filesystem;

  /** @var EntityManagerInterface */
  private EntityManagerInterface $em;

  /** @var TranslatorInterface */
  private TranslatorInterface $translator;

  /** @var PropertyMappingFactory */
  private PropertyMappingFactory $propertyMappingFactory;

  /** @var DirectoryNamerInterface */
  private DirectoryNamerInterface $directoryNamer;

  /** @var string */
  private string $wkhtmltopdfService;

  /** @var Environment */
  private Environment $templating;

  /** @var UrlGeneratorInterface */
  private UrlGeneratorInterface $router;

  /** @var string */
  private string $printablePassword;

  /** @var PraticaStatusService */
  private PraticaStatusService $statusService;

  /** @var ScheduleActionService */
  private ScheduleActionService $scheduleActionService;

  /** @var LoggerInterface */
  private LoggerInterface $logger;
  /** @var PraticaPlaceholderService */
  private PraticaPlaceholderService $praticaPlaceholderService;
  private AllegatoFileService $allegatoFileService;
  private string $pdfConvertType;

  /**
   * @param FileSystemService $filesystem
   * @param EntityManagerInterface $em
   * @param TranslatorInterface $translator
   * @param PropertyMappingFactory $propertyMappingFactory
   * @param DirectoryNamerInterface $directoryNamer
   * @param string $wkhtmltopdfService
   * @param Environment $templating
   * @param UrlGeneratorInterface $router
   * @param string $printablePassword
   * @param PraticaStatusService $statusService
   * @param ScheduleActionService $scheduleActionService
   * @param LoggerInterface $logger
   * @param PraticaPlaceholderService $praticaPlaceholderService
   * @param AllegatoFileService $allegatoFileService
   * @param string $pdfConvertType
   */
  public function __construct(
    FileSystemService $filesystem,
    EntityManagerInterface $em,
    TranslatorInterface $translator,
    PropertyMappingFactory $propertyMappingFactory,
    DirectoryNamerInterface $directoryNamer,
    string $wkhtmltopdfService,
    Environment $templating,
    UrlGeneratorInterface $router,
    string $printablePassword,
    PraticaStatusService $statusService,
    ScheduleActionService $scheduleActionService,
    LoggerInterface $logger,
    PraticaPlaceholderService $praticaPlaceholderService,
    AllegatoFileService $allegatoFileService,
    string $pdfConvertType
  )
  {
    $this->filesystem = $filesystem;
    $this->em = $em;
    $this->translator = $translator;
    $this->propertyMappingFactory = $propertyMappingFactory;
    $this->directoryNamer = $directoryNamer;
    $this->wkhtmltopdfService = $wkhtmltopdfService;
    $this->templating = $templating;
    $this->router = $router;
    $this->printablePassword = $printablePassword;
    $this->statusService = $statusService;
    $this->scheduleActionService = $scheduleActionService;
    $this->logger = $logger;
    $this->praticaPlaceholderService = $praticaPlaceholderService;
    $this->allegatoFileService = $allegatoFileService;
    $this->pdfConvertType = $pdfConvertType;
  }

  /**
   * @param Pratica $pratica
   *
   * @return RispostaOperatore
   * @throws Exception
   */
  public function createUnsignedResponseForPratica(Pratica $pratica): RispostaOperatore
  {
    $unsignedResponse = new RispostaOperatore();
    $this->createAllegatoInstance($pratica, $unsignedResponse);

    $fileDescription = "Risposta {$pratica->getServizio()->getName()}";
    $unsignedResponse->setOriginalFilename($this->generateFileName($fileDescription));
    $unsignedResponse->setDescription($this->generateFileDescription($fileDescription));

    $this->em->persist($unsignedResponse);
    return $unsignedResponse;
  }


  /**
   * @param Pratica $pratica
   *
   * @return RispostaOperatore
   * @throws Exception
   */
  public function createSignedResponseForPratica(Pratica $pratica): RispostaOperatore
  {
    $signedResponse = new RispostaOperatore();
    $this->createAllegatoInstance($pratica, $signedResponse);

    $fileDescription = "Risposta {$pratica->getServizio()->getName()}";
    $signedResponse->setOriginalFilename($this->generateFileName($fileDescription));
    $signedResponse->setDescription($this->generateFileDescription($fileDescription));

    $this->em->persist($signedResponse);
    return $signedResponse;
  }

  /**
   * @param Pratica $pratica
   *
   * @return Ritiro
   * @throws Exception
   */
  public function createWithdrawForPratica(Pratica $pratica): Ritiro
  {
    $withdrawAttachment = new Ritiro();
    $this->createAllegatoInstance($pratica, $withdrawAttachment);

    $fileDescription = "Ritiro pratica {$pratica->getServizio()->getName()}";
    $withdrawAttachment->setOriginalFilename($this->generateFileName($fileDescription));
    $withdrawAttachment->setDescription($this->generateFileDescription($fileDescription));

    $this->em->persist($withdrawAttachment);
    return $withdrawAttachment;
  }


  /**
   * @param Pratica $pratica
   *
   * @return ModuloCompilato
   * @throws Exception
   */
  public function createForPratica(Pratica $pratica): ModuloCompilato
  {
    $moduloCompilato = new ModuloCompilato();
    $this->createAllegatoInstance($pratica, $moduloCompilato);

    $fileDescription = "Modulo {$pratica->getServizio()->getName()}";
    $moduloCompilato->setOriginalFilename($this->generateFileName($fileDescription));
    $moduloCompilato->setDescription($this->generateFileDescription($fileDescription));

    $this->em->persist($moduloCompilato);
    return $moduloCompilato;
  }

  /**
   * @param Pratica $pratica
   * @return false|mixed
   * @throws FileNotFoundException
   */
  public function updateForPratica(Pratica $pratica)
  {
    $moduloCompilato = $pratica->getModuliCompilati()->last();

    if ($moduloCompilato) {
      $this->updateAllegatoInstance($pratica, $moduloCompilato);

      $fileDescription = "Modulo {$pratica->getServizio()->getName()}";
      $moduloCompilato->setOriginalFilename($this->generateFileName($fileDescription));
      $moduloCompilato->setDescription($this->generateFileDescription($fileDescription));

      $this->em->persist($moduloCompilato);
      $this->em->flush();
    }
    return $moduloCompilato;
  }

  /**
   * @param Pratica $pratica
   * @param RichiestaIntegrazioneDTO $integrationRequest
   * @param array $attachments
   * @return RichiestaIntegrazione
   * @throws FileExistsException
   */
  public function creaModuloProtocollabilePerRichiestaIntegrazione(Pratica $pratica, RichiestaIntegrazioneDTO $integrationRequest, array $attachments = []): RichiestaIntegrazione
  {
    $integration = new RichiestaIntegrazione();
    $payload = $integrationRequest->getPayload();

    if (isset($payload['FileRichiesta']) && !empty($payload['FileRichiesta'])) {
      $content = base64_decode($payload['FileRichiesta']);
      unset($payload['FileRichiesta']);
      $extension = '.pdf.p7m';
      $fileName = uniqid() . $extension;
      $integration->setMimeType('application/pkcs7-mime');
    } else {
      $content = $this->renderForPraticaIntegrationRequest($pratica, $integrationRequest, $attachments);
      $extension = '.pdf';
      $fileName = uniqid() . $extension;
      $integration->setMimeType('application/pdf');
    }

    $originalFileName = "Richiesta integrazione: {$integration->getId()}";

    $integration->setPayload($payload);
    $integration->setOwner($pratica->getUser());
    $integration->setOriginalFilename($this->generateFileName($originalFileName, $extension));
    $integration->setDescription($this->generateFileDescription($originalFileName));
    $integration->setPratica($pratica);

    $destinationDirectory = $this->getDestinationDirectoryFromContext($integration);
    $filePath = $destinationDirectory . DIRECTORY_SEPARATOR . $fileName;

    $this->filesystem->getFilesystem()->write($filePath, $content);
    $integration->setFile(new File($filePath, false));
    $integration->setFilename($fileName);

    $this->em->persist($integration);

    return $integration;
  }

  /**
   * @param Pratica $pratica
   * @param array|null $messages
   * @param bool $cancel
   * @return RispostaIntegrazione
   * @throws FileExistsException
   */
  public function creaModuloProtocollabilePerRispostaIntegrazione(Pratica $pratica, array $messages = null, bool $cancel = false): RispostaIntegrazione
  {

    $integrationRequest = $pratica->getRichiestaDiIntegrazioneAttiva();
    $payload[RichiestaIntegrazione::TYPE_DEFAULT] = $integrationRequest->getId();

    // Serve per non recuperare i messaggi nelle pratiche legacy
    if ($pratica->getServizio()->isLegacy()) {
      $messages = [];
    }

    //Se messages è null recupero i messaggi in automatico, per retrocompatibilità su prima versione
    if ($messages === null) {
      $repo = $this->em->getRepository('App\Entity\Pratica');
      $filters['from_date'] = $integrationRequest->getCreatedAt();
      $filters['visibility'] = Message::VISIBILITY_APPLICANT;
      $messages = $repo->getMessages($filters, $pratica);
    }

    if (!empty($messages)) {
      /** @var Message $m */
      foreach ($messages as $m) {
        $payload[RispostaIntegrazione::PAYLOAD_MESSAGES][]= $m->getId();
        /** @var Allegato $a */
        foreach ($m->getAttachments() as $a ) {
          $payload[RispostaIntegrazione::PAYLOAD_ATTACHMENTS][]= $a->getId();
        }
      }
    }

    if ($cancel) {
      $content = $this->renderForPraticaIntegrationAnswer($pratica, $integrationRequest, $messages, $cancel);
      $description = 'Cancellazione richiesta integrazione: ' . $integrationRequest->getId();
    } else {
      $content = $this->renderForPraticaIntegrationAnswer($pratica, $integrationRequest, $messages, $cancel);
      $description = 'Risposta a richiesta integrazione: ' . $integrationRequest->getId();
    }

    $fileName = uniqid() . '.pdf';
    $attachment = new RispostaIntegrazione();
    $attachment->setPayload($payload);
    $attachment->setOwner($pratica->getUser());
    $attachment->setOriginalFilename($this->generateFileName($description));
    $attachment->setDescription($this->generateFileDescription($description));

    $destinationDirectory = $this->getDestinationDirectoryFromContext($attachment);
    $filePath = $destinationDirectory . DIRECTORY_SEPARATOR . $fileName;

    $this->filesystem->getFilesystem()->write($filePath, $content);
    $attachment->setFile(new File($filePath, false));
    $attachment->setFilename($fileName);

    $this->em->persist($attachment);

    return $attachment;
  }

  /**
   * @param Pratica $pratica
   * @param RispostaOperatoreDTO $rispostaOperatore
   * @return RispostaOperatore|null
   * @throws Exception
   */
  public function creaRispostaOperatore(Pratica $pratica, RispostaOperatoreDTO $rispostaOperatore): ?RispostaOperatore
  {
    $response = new RispostaOperatore();
    $payload = $rispostaOperatore->getPayload();

    if (isset($payload['FileRichiesta']) && !empty($payload['FileRichiesta'])) {
      $content = base64_decode($payload['FileRichiesta']);
      unset($payload['FileRichiesta']);
      $uniqid = uniqid();
      $fileName = $uniqid . '.p7m';

      $response->setOwner($pratica->getUser());
      $response->setOriginalFilename($uniqid . '.pdf.p7m');
      $response->setDescription($rispostaOperatore->getMessage() ?? '');

      $destinationDirectory = $this->getDestinationDirectoryFromContext($response);
      $filePath = $destinationDirectory . DIRECTORY_SEPARATOR . $fileName;

      $this->filesystem->getFilesystem()->write($filePath, $content);
      $response->setFile(new File($filePath, false));
      $response->setFilename($fileName);

      $this->em->persist($response);

    } else {
      $response = $this->createUnsignedResponseForPratica($pratica);
    }
    return $response;
  }

  /**
   * @param Pratica $pratica
   * @param RichiestaIntegrazioneDTO $integrationRequest
   * @return string
   */
  private function renderForPraticaIntegrationRequest(Pratica $pratica, RichiestaIntegrazioneDTO $integrationRequest, $attachments = []): string
  {

    $html = $this->templating->render('Pratiche/pdf/parts/integration.html.twig', [
      'pratica' => $pratica,
      'integration_request' => $integrationRequest,
      'user' => $pratica->getUser(),
      'attachments' => $attachments
    ]);

    return $this->generatePdf($html);
  }

  /**
   * @param Pratica $pratica
   * @param RichiestaIntegrazione $integrationRequest
   * @param array|null $messages
   * @return string
   * @throws GotenbergApiErroed
   */
  private function renderForPraticaIntegrationAnswer(Pratica $pratica, RichiestaIntegrazione $integrationRequest, ?array $messages, $cancel): string
  {

    /** @var IntegrazioneRepository $integrationRepo */
    $integrationRepo = $this->em->getRepository('App\Entity\Integrazione');

    /** @var Integrazione[] $integrations */
    $integrations = $integrationRepo->findByIntegrationRequest($integrationRequest->getId());

    // Recupero l'id del messaggio associato all'ultimo cambio di stato di richiesta integrazione
    $applicationRepo = $this->em->getRepository('App\Entity\Pratica');
    $integrationMessages = $applicationRepo->findStatusMessagesByStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);
    $lastIntegrationMessage = end($integrationMessages);

    if ($cancel) {
      $html = $this->templating->render('Pratiche/pdf/parts/cancel_integration.html.twig', [
        'pratica' => $pratica,
        'richiesta_integrazione' => $integrationRequest,
        'integration_request_message' => $lastIntegrationMessage,
        'user' => $pratica->getUser(),
      ]);
    } else {
      $html = $this->templating->render('Pratiche/pdf/parts/answer_integration.html.twig', [
        'pratica' => $pratica,
        'richiesta_integrazione' => $integrationRequest,
        'integration_request_message' => $lastIntegrationMessage,
        'integrazioni' => $integrations,
        'messages' => $messages ?? [],
        'user' => $pratica->getUser(),
      ]);
    }

    return $this->generatePdf($html);
  }

  /**
   * @param Pratica $pratica
   * @param bool $showProtocolNumber
   * @return string
   * @throws LoaderError
   * @throws RuntimeError
   * @throws SyntaxError
   */
  public function renderForPratica(Pratica $pratica, bool $showProtocolNumber = false): string
  {
    $className = (new ReflectionClass($pratica))->getShortName();
    if ($pratica->isFormIOType()) {
      if ($pratica->getServizio()->getReceipt() === Servizio::RECEIPT_F24) {
        $html = $this->templating->render('Print/printF24.html.twig', [
          'pratica' => $pratica
        ]);
      } else {
        $attachments = $pratica->getAllegati();
        $preparedAttachments = [];
        $preparedNoProtocolAttachments = [];
        if ($attachments->count() > 0) {
          /** @var Allegato $a */
          foreach ($attachments as $a) {
            $protocolRequired = $a->isProtocolRequired() ?? true;
            $temp = [];
            $temp['id'] = $a->getId();
            $temp['local_name'] = $a->getFilename();
            $temp['original_filename'] = $a->getOriginalFilename();
            try {
              $temp['hash'] = $this->allegatoFileService->getHash($this->allegatoFileService->getFilenameWithPath($a));
            } catch (FileNotFoundException $e) {
              $this->logger->error('Attachment not found during render for class pdf', ['application' => $pratica->getId(), 'attachment' => $a->getId()]);
            }
            if ($protocolRequired) {
              $preparedAttachments[] = $temp;
            } else {
              $preparedNoProtocolAttachments[] = $temp;
            }
          }
        }
        $html = $this->templating->render('Print/printPratica.html.twig', [
          'show_protocol_number' => $showProtocolNumber,
          'pratica' => $pratica,
          'user' => $pratica->getUser(),
          'attachments' => $preparedAttachments,
          'attachmentsNoProtocol' => $preparedNoProtocolAttachments,
        ]);
      }
    } else {
      $html = $this->templating->render('Pratiche/pdf/' . $className . '.html.twig', [
        'pratica' => $pratica,
        'user' => $pratica->getUser(),
      ]);
    }
    return $html;
  }

  /**
   * @param Pratica $pratica
   *
   * @return string
   * @throws GotenbergApiErroed|LoaderError|RuntimeError|SyntaxError
   */
  public function renderForResponse(Pratica $pratica): string
  {
    // Risposta Operatore di default
    $html = $this->templating->render('Pratiche/pdf/RispostaOperatore.html.twig', [
      'pratica' => $pratica,
      'user' => $pratica->getUser(),
    ]);

    $feedbackMessages = $pratica->getServizio()->getFeedbackMessages();
    $status = $pratica->getEsito() ? Pratica::STATUS_COMPLETE : Pratica::STATUS_CANCELLED;
    if (isset($feedbackMessages[$status])) {
      /** @var FeedbackMessage $feedbackMessage */
      $feedbackMessage = $feedbackMessages[$status];

      $placeholders = $this->praticaPlaceholderService->getPlaceholders($pratica);

      $html = $this->templating->render(
        'Pratiche/pdf/RispostaOperatoreCustom.html.twig',
        array(
          'pratica' => $pratica,
          'placeholder' => $placeholders,
          'text' => strtr($feedbackMessage['message'], $placeholders),
        )
      );
    }

    return $this->generatePdf($html);

  }

  /**
   * @param Pratica $pratica
   *
   * @return string
   * @throws GotenbergApiErroed|LoaderError|RuntimeError|SyntaxError
   */
  public function renderForWithdraw(Pratica $pratica): string
  {
    $html = $this->templating->render('Pratiche/pdf/Ritiro.html.twig', [
      'pratica' => $pratica,
      'user' => $pratica->getUser(),
    ]);
    return $this->generatePdf($html);
  }

  /**
   * @param Allegato $moduloCompilato
   *
   * @return string
   */
  private function getDestinationDirectoryFromContext(Allegato $moduloCompilato): string
  {
    $mapping = $this->propertyMappingFactory->fromObject($moduloCompilato)[0];
    return $this->directoryNamer->directoryName($moduloCompilato, $mapping);
  }

  /**
   * @param Pratica $pratica
   * @param Allegato $allegato
   * @throws FileExistsException
   * @throws Exception
   */
  private function createAllegatoInstance(Pratica $pratica, Allegato $allegato)
  {
    $allegato->setOwner($pratica->getUser());
    $destinationDirectory = $this->getDestinationDirectoryFromContext($allegato);
    $fileName = uniqid() . '.pdf';
    $filePath = $destinationDirectory . DIRECTORY_SEPARATOR . $fileName;

    if ($allegato instanceof RispostaOperatore) {
      $content = $this->renderForResponse($pratica);
      $this->filesystem->getFilesystem()->write($filePath, $content);
    } else if ($allegato instanceof Ritiro) {
      $content = $this->renderForWithdraw($pratica);
      $this->filesystem->getFilesystem()->write($filePath, $content);
    } else {
      $content = $this->generateApplicationPdf($pratica);
      $this->filesystem->getFilesystem()->write($filePath, $content);
    }

    $allegato->setFile(new File($filePath, false));
    $allegato->setFilename($fileName);
    $allegato->setMimeType(self::MIME_TYPE);
  }

  /**
   * @param Pratica $pratica
   * @param Allegato $allegato
   * @throws FileNotFoundException
   */
  private function updateAllegatoInstance(Pratica $pratica, Allegato $allegato)
  {
    $destinationDirectory = $this->getDestinationDirectoryFromContext($allegato);
    $filePath = $destinationDirectory . DIRECTORY_SEPARATOR . $allegato->getFilename();

    if ($allegato instanceof RispostaOperatore) {
      $content = $this->renderForResponse($pratica);
      $this->filesystem->getFilesystem()->update($filePath, $content);
    } else if ($allegato instanceof Ritiro) {
      $content = $this->renderForWithdraw($pratica);
      $this->filesystem->getFilesystem()->update($filePath, $content);
    } else {
      $content = $this->generateApplicationPdf($pratica, $pratica->getNumeroProtocollo() ?? false);
      $this->filesystem->getFilesystem()->update($filePath, $content);
    }
  }

  /**
   * @param $html
   * @return string
   * @throws GotenbergApiErroed
   */
  public function generatePdf($html): string
  {

    $client = Gotenberg::chromium($this->wkhtmltopdfService)
      ->margins(1,0,0,0)
      ->paperSize("8.27","11.7");
    $request = $client->html(Stream::string('index.html', $html));
    $response = Gotenberg::send($request);
    return $response->getBody()->getContents();
  }

  /**
   * @param Pratica $pratica
   * @param bool $showProtocolNumber
   * @return string
   * @throws GotenbergApiErroed|LoaderError|RuntimeError|SyntaxError
   */
  public function generateApplicationPdf(Pratica $pratica, bool $showProtocolNumber = false): string
  {

    $client = Gotenberg::chromium($this->wkhtmltopdfService)
      ->margins(1,0,0,0)
      ->printBackground()
      ->paperSize("8.27","11.7");

    if ($pratica->isFormIOType() && $pratica->getServizio()->getReceipt() !== Servizio::RECEIPT_F24) {
      $client->waitForExpression("window.status === 'ready'");
    }
    // Rimuovo conversione a PDF/A-1a per questo errore di gotenberg, da indagare
    // convert PDF to 'PDF/A-1a' with unoconv: unoconv PDF: unix process error: wait for unix process: exit status 5
    //->pdfFormat('PDF/A-1a')

    if ($this->pdfConvertType === 'url') {
      $locale = $pratica->getLocale();
      $url = $this->router->generate('print_application', ['_locale' => $locale, 'pratica' => $pratica, 'protocol' => $showProtocolNumber], UrlGeneratorInterface::ABSOLUTE_URL);
      $request = $client
        ->extraHttpHeaders([
          'Authorization' => 'Basic '.base64_encode(implode(':', [self::PRINTABLE_USERNAME, $this->printablePassword]))
        ])
        ->url($url);

    } else {
      $html = $this->renderForPratica($pratica, $showProtocolNumber);
      $request = $client->html(Stream::string('index.html', $html));
    }
    $response = Gotenberg::send($request);
    return $response->getBody()->getContents();
  }

    /**
     * @param $fileName
     * @param string $extension
     * @return string
     */
  private function generateFileName($fileName, string $extension = '.pdf'): string
  {
    $now = new DateTime();
    $now->setTimestamp(time());

    $fileName = ("{$fileName} " . $now->format('Ymdhi'));
    $fileName = StringUtils::shortenString($fileName);
    return $fileName . $extension;
  }

  /**
   * @param $description
   * @return string
   */
  private function generateFileDescription($description): string
  {
    $now = new DateTime();
    $now->setTimestamp(time());

    $description = ("{$description} " . $now->format('Y-m-d h:i'));
    return StringUtils::shortenString($description);
  }

  /**
   * @param Servizio $service
   * @return string
   * @throws GotenbergApiErroed
   */
  public function generateServicePdfUsingGotemberg( Servizio $service ): string
  {
    $url = $this->router->generate('print_service', ['service' => $service->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
    $request = Gotenberg::chromium($this->wkhtmltopdfService)
      ->margins(1,0,0,0)
      ->paperSize("8.27","11.7")
      ->waitDelay("20s")
      ->extraHttpHeaders([
        'Authorization' => 'Basic '.base64_encode(implode(':', [self::PRINTABLE_USERNAME, $this->printablePassword]))
      ])
      ->url($url);

    $response = Gotenberg::send($request);
    return $response->getBody()->getContents();
  }

  /**
   * @param Pratica|GiscomPratica $pratica
   * @param bool|int $nextStatus
   * @throws AlreadyScheduledException
   */
  public function createForPraticaAsync(Pratica $pratica, $nextStatus = false)
  {
    $params = [
      'pratica' => $pratica->getId()
    ];

    if ($nextStatus) {
      $params['next_status'] = $nextStatus;
    }

    $this->scheduleActionService->appendAction(
      'ocsdc.modulo_pdf_builder',
      self::SCHEDULED_CREATE_FOR_PRATICA,
      serialize($params)
    );
  }

  /**
   * @param Pratica|GiscomPratica $pratica
   * @throws AlreadyScheduledException
   */
  public function updateForPraticaAsync(Pratica $pratica)
  {
    $params = [
      'pratica' => $pratica->getId()
    ];

    $this->scheduleActionService->appendAction(
      'ocsdc.modulo_pdf_builder',
      self::SCHEDULED_UPDATE_FOR_PRATICA,
      serialize($params)
    );
  }

  /**
   * @param SubscriptionPayment $payment
   * @return string
   * @throws GotenbergApiErroed
   */
  public function renderForSubscriptionPayment(SubscriptionPayment $payment): string
  {
    $pratica = $this->em->getRepository(Pratica::class)->find($payment->getExternalKey());
    // Certificato di default
    $html = $this->templating->render('Subscriptions/pdf/Payment.html.twig', [
      "payment"=>$payment,
      "pratica"=>$pratica
    ]);

    return $this->generatePdf($html);
  }

  /**
   * @param ScheduledAction $action
   * @throws Exception
   */
  public function executeScheduledAction(ScheduledAction $action)
  {
    $params = unserialize($action->getParams());

    /** @var Pratica $pratica */
    $pratica = $this->em->getRepository('App\Entity\Pratica')->find($params['pratica']);
    if (!$pratica instanceof Pratica) {
      throw new Exception('Not found application with id: ' . $params['pratica']);
    }

    if ($action->getType() == self::SCHEDULED_CREATE_FOR_PRATICA) {


      // Todo: trovare una logica migliore e disaccoppiare la transizione del cambio stato con gli event listener
      // Serve ad evitare che un errore successivo (integrazioni backoffice, messaggi kafka, webhook) crei un pdf all'infinito
      //if ($pratica->getModuliCompilati()->isEmpty()) {
        $pdf = $this->createForPratica($pratica);
        $pratica->addModuloCompilato($pdf);
      //}

      try {
        if (isset($params['next_status']) && !empty($params['next_status'])) {
          if ($params['next_status'] === self::WITHOUT_CHANGE_STATUS){
            $this->em->persist($pratica);
            $this->em->flush();
          }else{
            $this->statusService->setNewStatus($pratica, $params['next_status'], null, true);
          }
        } else {
          $this->statusService->validateChangeStatus($pratica, Pratica::STATUS_SUBMITTED);
          $this->statusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED);
        }
      } catch (Exception $e) {
        $this->logger->warning(
          'ModuloPdfBuilderService -  ' . $this->translator->trans('errori.pratica.change_status_invalid') . ' - ' . $e->getMessage()
        );
      }
    } elseif ($action->getType() == self::SCHEDULED_UPDATE_FOR_PRATICA) {

      if (!$pratica->getModuliCompilati()->isEmpty()) {
        $this->updateForPratica($pratica);
      }

    }
  }
}
