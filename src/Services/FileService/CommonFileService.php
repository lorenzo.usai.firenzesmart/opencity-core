<?php

namespace App\Services\FileService;

use League\Flysystem\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;

class CommonFileService extends AbstractFileService
{

  const URI_PREFIX = 'uploads/';

  /**
   * @throws FileNotFoundException
   */
  public function getFileContent(File $file)
  {
    $pathname = str_replace(self::URI_PREFIX, '', $file->getPathname());
    return $this->getContent($pathname);
  }


  /**
   * @throws FileNotFoundException
   */
  public function getFileStream(File $file)
  {
    $pathname = str_replace(self::URI_PREFIX, '', $file->getPathname());
    return $this->getStream($pathname);
  }
}
