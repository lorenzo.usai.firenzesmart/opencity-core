<?php

namespace App\Services;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;

class GeoService
{

  private EntityManagerInterface $entityManager;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->entityManager = $entityManager;
  }

  /**
   * @param $featuresCollection
   * @return string
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   *
   * @description Transform a features collection of multiple type (Polygon, Line, Multiline) in a single Polygon
   */
  public function makePolygon($featuresCollection): string
  {

    // Se la features collection è vuota non devo fare nulla
    if (empty($featuresCollection)) {
      return $featuresCollection;
    }

    $geoData = json_decode($featuresCollection, true);
    $features = $geoData['features'];

    // Se la features collection ha una singola geometria di tipo Polygon non devo fare nulla
    if (count($features) == 1 && $features[0]['geometry']['type'] === 'Polygon') {
      return $featuresCollection;
    }

    $geometries = [];
    // Ciclo tutte le features della collection e preparo la query per creare il poligono
    foreach ($features as $g) {
      if ($g['type'] !== 'Point') {
        $geometries []= sprintf("(SELECT ST_Force2D(ST_GeomFromGeoJSON('%s')))", json_encode($g['geometry']));
      }
    }

    $unionQuery = sprintf("SELECT ST_AsGeoJSON(ST_Dump(ST_Polygonize(ST_Union(ARRAY[%s]))))", implode(',', $geometries));
    $stmt = $this->entityManager->getConnection()->prepare($unionQuery);
    $polygon = json_decode($stmt->executeQuery()->fetchOne(), true);

    return json_encode([
      'type' => 'FeatureCollection',
      'features' => [$polygon]
    ]);

  }

  /*
   * SELECT ST_Contains(
	 *   ST_GeomFromGeoJSON('{"type":"Polygon","coordinates":[[[12.245518,42.0134],[12.380902,41.753026],[12.69626,41.76171],[13.025897,41.934294],[12.820752,42.034625],[12.467418,42.073927],[12.245518,42.0134]]]}'),
	 *   ST_SetSRID(ST_MakePoint(12.476446,41.887935), 4326)
   * )
   */
  /**
   * @throws Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function geoFenceContainsPoint(float $lon, float $lat, string $geofence): bool
  {
    $geoData = json_decode($geofence, true);
    // ST_GeomFromGeoJSON works only for JSON Geometry fragments. It throws an error if you try to use it on a whole JSON document.
    $geometry = $geoData['features'][0]['geometry'];
    $sql = sprintf("SELECT ST_Contains(ST_GeomFromGeoJSON('%s'), ST_SetSRID(ST_MakePoint(%f,%f), 4326))", json_encode($geometry), $lon, $lat);
    $stmt = $this->entityManager->getConnection()->prepare($sql);
    $result = $stmt->executeQuery();
    return $result->fetchOne();
  }
}
