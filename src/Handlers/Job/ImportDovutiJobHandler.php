<?php

namespace App\Handlers\Job;

use App\Entity\CPSUser;
use App\Entity\Job;
use App\Entity\Servizio;
use App\Model\Payment;
use App\Model\Payment\PaymentTransaction;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Payment\GatewayCollection;
use App\Services\CPSUserProvider;
use App\Services\FileService\CommonFileService;
use App\Services\InstanceService;
use App\Services\KafkaService;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Ramsey\Uuid\Uuid;
use \Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImportDovutiJobHandler implements JobHandlerInterface
{

  private EntityManagerInterface $entityManager;
  private CommonFileService $fileService;
  private KafkaService $kafkaService;
  private CPSUserProvider $cpsUserProvider;
  private RouterInterface $router;
  private GatewayCollection $gatewayCollection;

  private array $gateways = [];

  private Servizio $service;
  private MailerService $mailerService;
  private string $defaultSender;
  private InstanceService $instanceService;
  private TranslatorInterface $translator;

  private string $separator = ',';


  /**
   * @param EntityManagerInterface $entityManager
   * @param CommonFileService $fileService
   * @param KafkaService $kafkaService
   * @param CPSUserProvider $cpsUserProvider
   * @param RouterInterface $router
   * @param GatewayCollection $gatewayCollection
   * @param MailerService $mailerService
   * @param string $defaultSender
   * @param InstanceService $instanceService
   * @param TranslatorInterface $translator
   */
  public function __construct(EntityManagerInterface $entityManager, CommonFileService $fileService, KafkaService $kafkaService, CPSUserProvider $cpsUserProvider, RouterInterface $router, GatewayCollection $gatewayCollection, MailerService $mailerService, string $defaultSender, InstanceService $instanceService, TranslatorInterface $translator)
  {
    $this->entityManager = $entityManager;
    $this->fileService = $fileService;
    $this->kafkaService = $kafkaService;
    $this->cpsUserProvider = $cpsUserProvider;
    $this->router = $router;
    $this->gatewayCollection = $gatewayCollection;
    $this->mailerService = $mailerService;
    $this->defaultSender = $defaultSender;
    $this->instanceService = $instanceService;
    $this->translator = $translator;

    // Recupero la lista dei gateway payment proxy disponibili
    $availablePaymentGateways = $this->gatewayCollection->getAvailablePaymentGateways();
    foreach ($availablePaymentGateways as $k => $g) {
      if (!in_array($k, [Bollo::IDENTIFIER, MyPay::IDENTIFIER])) {
        $this->gateways[$k] = $g;
      }
    }
  }

  public function processJob(Job $job): void
  {

    // Verifica delle configurazioni del job
    try {
      $this->validateJob($job);
    } catch (Exception $e) {
      $job->setStatus(Job::STATUS_FAILED);
      $job->setOutput($e->getMessage());
      $job->setTerminatedAt(new \DateTime());
      $this->entityManager->persist($job);
      $this->entityManager->flush();
      return;
    }

    $tenant = $this->instanceService->getCurrentInstance();
    $serviceId = $this->service->getId();
    $serviceLink = $this->router->generate('servizi_show', ['slug' => $this->service->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);

    $header = [];
    $fileStream = $this->fileService->getFileStream($job->getFile());

    $processedRow = $this->getProcessedRow($job);
    $errors = $job->getErrors();
    $rowNumber = 0;

    // Leggo il file riga per riga
    while (($row = fgetcsv($fileStream, null, $this->separator)) !== false) {
      if (empty($header)) {
        $header = $row;
      } else {
        $rowNumber++;

        // Se il dato della riga corrente nel metadato del job è maggiore della riga attuale salto esecuzione
        if ($processedRow > $rowNumber) {
          continue;
        }

        $data = array_combine($header, $row);
        $this->saveJobCurrentState($job, $data, $rowNumber);

        try {

          $createdAt = \DateTime::createFromFormat('d-m-Y',$data['created_at']);
          if (!$createdAt instanceof \DateTimeInterface) {
            $createdAt = new \DateTime();
          }

          $debtor = $this->provideUser($data);
          // Creo il Payment
          $payment = new Payment();
          $id = Uuid::uuid4()->toString();
          $payment->setId($id);
          $payment->setUserId($debtor->getId());
          $payment->setTenantId($tenant->getId());
          $payment->setServiceId($serviceId);
          $payment->setCreatedAt($createdAt);
          $payment->setUpdatedAt($createdAt);
          $payment->setStatus(Payment::STATUS_PAYMENT_PENDING);
          $payment->setReason($data['reason']);
          $payment->setRemoteId($id);

          $expireAt = \DateTime::createFromFormat('d-m-Y',$data['expire_at']);
          if (!$expireAt instanceof \DateTimeInterface) {
            $expireAt = clone $createdAt;
            $expireAt->modify('+90 days');
          }

          $paymentTransaction = new PaymentTransaction();
          $paymentTransaction->setTransactionId($id);
          $paymentTransaction->setExpireAt($expireAt);
          $paymentTransaction->setAmount(str_replace(',', '.', $data['amount']));
          $paymentTransaction->setNoticeCode($data['notice_number']);
          $paymentTransaction->setIud($id);
          $paymentTransaction->setIuv($data['iuv']);
          $payment->setPayment($paymentTransaction);

          $paymentLinks = new Payment\PaymentLinks();
          $paymentLinks->setOnlinePaymentBegin([
            'url' => $this->generateOnlinePaymentBeginUrl($id),
            'last_opened_at' => null,
            'method' => 'GET'
          ]);
          $paymentLinks->setOnlinePaymentLanding(
            [
              'url' => $this->generateOnlinePaymentLandingUrl($id),
              'last_opened_at' => null,
              'method' => 'GET'
            ]
          );
          $paymentLinks->setOfflinePayment(
            [
              'url' => null,
              'last_opened_at' => null,
              'method' => 'GET'
            ]
          );
          $paymentLinks->setReceipt(
            [
              'url' => $this->generateReceiptUrl($id),
              'last_opened_at' => null,
              'method' => 'GET'
            ]
          );
          $paymentLinks->setUpdate(
            [
              'url' => null,
              'last_opened_at' => null,
              'method' => 'GET'
            ]
          );
          $paymentLinks->setNotify(
            [
              [
                'url' => '#',
                'last_opened_at' => null,
                'method' => 'POST'
              ]
            ]
          );
          $payment->setLinks($paymentLinks);

          $payer = new Payment\Payer();
          $payer->setTaxIdentificationNumber($debtor->getCodiceFiscale());
          $payer->setName($debtor->getNome());
          $payer->setFamilyName($debtor->getCognome());
          $payer->setStreetName($debtor->getIndirizzoResidenza() ?? '');
          $payer->setBuildingNumber('');
          $payer->setPostalCode($debtor->getCapResidenza() ?? '');
          $payer->setTownName($debtor->getCittaResidenza() ?? '');
          $payer->setCountrySubdivision($debtor->getProvinciaResidenza() ?? '');
          $payer->setCountry('IT');
          $payer->setEmail($debtor->getEmail());
          $payment->setPayer($payer);

          // Invio il pagamento a kafka
          $this->kafkaService->produceMessage($payment);

          // Invio la notifica al cittadino
          $subject = $this->translator->trans('payment.emails.import.subject', ['%notice_number%' => $data['notice_number']]);
          $message = $this->translator->trans('payment.emails.import.message', [
            '%user_name%' => $debtor->getFullName(),
            '%notice_number%' => $data['notice_number']
          ]);
          $callToActions = [
            [
              'label' => 'payment.emails.import.call_to_action_label',
              'link' => $serviceLink
            ]
          ];
          $this->mailerService->dispatchMail($this->defaultSender, $tenant->getName(), $debtor->getEmail(),  $debtor->getFullName(), $message, $subject, $tenant, $callToActions);


        } catch (\Exception $e) {
          $errors [$rowNumber] = "Errore d'importazione a riga {$rowNumber} (" . implode(', ', $data) . '): ' . $e->getMessage();
          $this->saveJobErrors($job, $errors);
        }
      }
    }

    if (empty($errors)) {
      $job->setStatus(Job::STATUS_FINISHED);
      $job->setOutput('Job process finished successfully');
    } else {
      $job->setOutput('Job process finished with errors');
      $job->setStatus(Job::STATUS_FAILED);
    }
    $job->setTerminatedAt(new \DateTime());
    $this->entityManager->persist($job);;
    $this->entityManager->flush();

  }

  /**
   * @throws Exception
   */
  public function validateJob(Job $job)
  {
    $args = $job->getArgsAsArray();

    if (empty($args) || empty($args['service_id'])) {
      throw new Exception('service_id argument is mandatory');
    }

    if (!Uuid::isValid($args['service_id'])) {
      throw new Exception('service_id argument must be a valid Uuid');
    }

    $repository = $this->entityManager->getRepository('App\Entity\Servizio');
    $service = $repository->find($args['service_id']);

    if (!$service instanceof Servizio) {
      throw new Exception("Service with id {$args['service_id']} not found");
    }

    $this->service = $service;

    $paymentParameters = $service->getPaymentParameters();
    if (empty($paymentParameters['gateways'])) {
      throw new Exception("Service must have at least one payment gateway");
    }

    $this->detectSeparator($job);
  }

  private function detectSeparator($job): void
  {
    $delimiters = [
      ';' => 0,
      ',' => 0
    ];

    $fileStream = $this->fileService->getFileStream($job->getFile());
    $firstLine = fgets($fileStream);
    foreach ($delimiters as $delimiter => $count) {
      $delimiters[$delimiter] = count(str_getcsv($firstLine, $delimiter));
    }

    $this->separator =  array_search(max($delimiters), $delimiters, true);
  }


  private function provideUser($data): CPSUser
  {

    $fiscalCode = $data['debtor_fiscal_code'];
    $email = $data['debtor_fiscal_code'];

    $qb = $this->entityManager->createQueryBuilder()
      ->select('user')
      ->from('App:CPSUser', 'user')
      ->andWhere('lower(user.codiceFiscale) = :cf')
      ->setParameter('cf', strtolower($fiscalCode))
      ->setMaxResults(1);

    $result = $qb->getQuery()->getResult();

    if (count($result) > 0) {
      return reset($result);
    }

    $user = $this->cpsUserProvider->createAnonymousUser(false);
    $user
      ->setNome(substr($fiscalCode, 0, 3))
      ->setCognome(substr($fiscalCode, 2, 3))
      ->setUsername($fiscalCode)
      ->setCodiceFiscale($fiscalCode);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $user
        ->setEmail($email)
        ->setEmailContatto($email);
    }

    $this->entityManager->persist($user);
    $this->entityManager->flush();
    return $user;
  }

  /**
   * @param $paymentId
   * @return string
   */
  private function generateOnlinePaymentLandingUrl($paymentId): string
  {
    return $this->router->generate('payments_details_cpsuser', [
      'paymentId' => $paymentId
    ], UrlGeneratorInterface::ABSOLUTE_URL);
  }

  /**
   * @param $paymentId
   * @return string
   */
  private function generateOnlinePaymentBeginUrl($paymentId): string
  {
    $paymentParameters = $this->service->getPaymentParameters();
    $identifier = reset($paymentParameters['gateways'])['identifier'];
    $paymentGateway = $this->gateways[$identifier];

    $url = $paymentGateway['url'];
    if (substr($url, -1) !== '/') {
      $url .= '/';
    }

    return $url . 'online-payment/' . $paymentId;
  }

  private function generateReceiptUrl($paymentId): string
  {
    $paymentParameters = $this->service->getPaymentParameters();
    $identifier = reset($paymentParameters['gateways'])['identifier'];
    $paymentGateway = $this->gateways[$identifier];

    $url = $paymentGateway['url'];
    if (substr($url, -1) !== '/') {
      $url .= '/';
    }

    return $url . 'receipt/' . $paymentId;
  }

  private function saveJobCurrentState(Job $job, $row, $rowNumber)
  {
    $job->setMetadata(['current_row' => $rowNumber]);
    $job->setRunningOutput("Processing row {$rowNumber} (" . implode(', ', $row) . ")");
    $this->entityManager->persist($job);
    $this->entityManager->flush();
  }

  private function saveJobErrors(Job $job, $errors)
  {
    $job->setErrors($errors);
    $this->entityManager->persist($job);
    $this->entityManager->flush();
  }

  private function getProcessedRow(Job $job)
  {
    $currentRow = 0;
    if (!empty($job->getMetadata()['current_row'])) {
      $currentRow = $job->getMetadata()['current_row'];
    }
    return $currentRow;
  }

}
