<?php

namespace App\Command;

use App\Entity\Servizio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Categoria;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class SyncTopicsCommand
 */
class SyncTopicsCommand extends Command
{

  /** @var EntityManagerInterface */
  private EntityManagerInterface $entityManager;

  private array $topicsMapping = [
    "Popolazione e società" => "Salute, benessere e assistenza",
    "Ambiente" => "Ambiente",
    "Governo e settore pubblico" => "Autorizzazioni",
    "Economia e finanze" => "Tributi, finanze e contravvenzioni",
    "Istruzione, cultura e sport" => "Cultura e tempo libero",
    "Salute" => "Salute, benessere e assistenza",
    "Giustizia, sistema giuridico e sicurezza pubblica" => "Giustizia e sicurezza pubblica",
    "Anagrafe e stato civile" => "Anagrafe e stato civile",
    "Scienza e Tecnologia" => "Impresa e commercio",
    "Catasto e urbanistica" => "Catasto e urbanistica",
    "Elezioni e voto" => "Anagrafe e stato civile",
    "Energia" => "Ambiente",
    "Trasporti" => "Mobilità e trasporti",
  ];

  private $pnrrTopics;


  /**
   * SyncTopicsCommand constructor.
   * @param EntityManagerInterface $entityManager
   */
  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->entityManager = $entityManager;
    parent::__construct();

    // Caricamento categorie pnrr
    $this->loadCategories();
  }

  protected function configure()
  {
    $this
      ->setName('ocsdc:sync-topics')
      ->setDescription(
        'Sync PNRR topics'
      )
      ->addOption('add', '-a', InputOption::VALUE_NONE, 'Force creation of missing topics');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $symfonyStyle = new SymfonyStyle($input, $output);
    $symfonyStyle->note('Start process...');

    // Trova tutte le categorie radice (senza parent)
    $topicRepo = $this->entityManager->getRepository(Categoria::class);
    $rootTopics = $topicRepo->findBy(['parent' => null]);

    foreach ($rootTopics as $rootTopic) {
      if (array_key_exists($rootTopic->getName(), $this->topicsMapping)) {
        // Sposto ricorsivamente le categorie figlie alla categoria padre
        $this->processTopic($rootTopic);

        // Recupero la categoria PNRR corrispondente
        $topicName = $this->topicsMapping[$rootTopic->getName()];
        $topicData = $this->pnrrTopics[$topicName];

        // Se la categoria non esiste rinomino la radice, altrimenti sposto tutti i servizi ed elimino la radice
        $topic = $topicRepo->findOneBy(['slug'=>$topicData['slug']]);
        /** @var Categoria $topic */

        // Se la categiria non esiste oppure coincide con la categoria padre aggiorno
        if (!$topic || $topic->getId() == $rootTopic->getId()) {
          // Aggiorno i dati della categoria
          $this->updateTopic($rootTopic, $topicName);
        } else {
          // Sposto tutti i servizi nella categoria esistente ed elimino la radice
          $this->moveServices($rootTopic, $topic);
        }
        $this->entityManager->flush();
      }
    }

    // Aggiungo le categorie pnrr mancanti
    if ($input->getOption('add')) {
      foreach ($this->pnrrTopics as $name => $topicData) {
        $topic = $topicRepo->findOneBy(['slug'=>$topicData['slug']]);
        if (!$topic) {
          $topic = new Categoria();
          $this->updateTopic($topic, $name);
          $this->entityManager->flush();
        }
      }
    }

    $symfonyStyle->success('Process completed.');
    return 0;
  }

  private function processTopic(Categoria $topic): void
  {
    // Trova e processa ricorsivamente le categorie figlie
    foreach ($topic->getChildren() as $child) {
      $this->processTopic($child);
      // Associo i servizi dei figli alla categoria padre ed elimino i figli
      $this->moveServices($child, $topic);
    }
  }

  private function updateTopic(Categoria $topic, string $topicName): void
  {
    if (!isset($this->pnrrTopics[$topicName])) {
      // La categoria non è tra quelle da allineare
      return;
    }
    $topicData = $this->pnrrTopics[$topicName];
    $topic->setName($topicData['name']);
    $topic->setSlug($topicData['slug']);
    $topic->setDescription($topicData['description']);
    $this->entityManager->persist($topic);
  }

  private function moveServices($fromTopic, $toTopic): void
  {
    foreach ($fromTopic->getServices() as $service) {
      /** @var Servizio $service */
      $service->setTopics($toTopic);
      $this->entityManager->persist($service);
    }
    $this->entityManager->remove($fromTopic);
  }

  private function loadCategories(): void
  {
    $filename = 'data/categories.csv';

    $header = NULL;
    $pnrrTopics = array();
    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 0, ',')) !== FALSE) {
        if (!$header) {
          $temp = array();
          foreach ($row as $r) {
            $temp [] = $r;
          }
          $header = $temp;
        } else {
          $pnrrTopics[$row[0]] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }
    $this->pnrrTopics = $pnrrTopics;
  }
}
