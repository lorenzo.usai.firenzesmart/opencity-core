<?php

namespace App\Command;

use App\Entity\Servizio;
use App\Services\Satisfy\SatisfyService;
use App\Services\Satisfy\SatisfyTenant;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class BuiltInCreateCommand
 */
class ConfigureSatisfyTenantCommand extends Command
{

  private EntityManagerInterface $entityManager;
  private SatisfyTenant $satisfyTenant;

  /**
   * AdministratorCreateCommand constructor.
   * @param EntityManagerInterface $entityManager
   * @param SatisfyTenant $satisfyTenant
   */
  public function __construct(EntityManagerInterface $entityManager, SatisfyTenant $satisfyTenant)
  {
    $this->entityManager = $entityManager;
    $this->satisfyTenant = $satisfyTenant;
    parent::__construct();

  }

  protected function configure()
  {
    $this
      ->setName('ocsdc:tenant:configure-satisfy')
      ->addOption(
        'email',
        null,
        InputOption::VALUE_OPTIONAL,
        'Specify email address for satisfy account'
      )
      ->addOption(
        'password',
        null,
        InputOption::VALUE_OPTIONAL,
        'Specify password satisfy entrypoint'
      )
      ->addOption('delete', null, InputOption::VALUE_NONE, 'Delete entrypoint');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $symfonyStyle = new SymfonyStyle($input, $output);

    $instance = $input->getOption('instance');
    $email = $input->getOption('email');
    $password = $input->getOption('password');
    $delete = $input->getOption('delete');


    if (!$delete && (empty($email) || empty($password))) {
      $symfonyStyle->error('Email and password are required for creating a satisfy tenant');
      return 1;
    }

    $repo = $this->entityManager->getRepository('App\Entity\Ente');
    $tenant = $repo->findOneBySlug($instance);

    if (!$tenant) {
      $symfonyStyle->error('Tenant not found');
      return 1;
    }


    if ($delete) {
      try {
        $symfonyStyle->note('Delete satisfy entrypoint for tenant '.$tenant->getName() . ' - ' . $tenant->getId());
        $this->satisfyTenant->deleteTenant($tenant);
      } catch (\Exception $e) {
        $symfonyStyle->error($e->getMessage());
      }
    } else {
      try {
        $symfonyStyle->note('Configure satisfy tenant ' .$tenant->getName() . ' - ' . $tenant->getId());
        $password = hash('sha256', $password);
        $this->satisfyTenant->createTenant($tenant, $email, $password);
      } catch (\Exception $e) {
        $symfonyStyle->error($e->getMessage());
      }
    }

    return 0;
  }
}
