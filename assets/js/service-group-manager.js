import './core';
import './utils/TextEditor';
import {TextEditor} from "./utils/TextEditor";
import tinymce from "tinymce";
const $language = document.documentElement.lang.toString();
import Swal from 'sweetalert2/src/sweetalert2.js'

$(document).ready(function () {
  $('.attachment-delete').on('click', function () {
    let btn = $(this);
    let deleteUrl = $(this).data("delete-url");

    $.ajax(deleteUrl,
      {
        method: 'DELETE',
        success: function () {   // success callback function
          btn.closest('li').remove();
        },
        error: function () { // error callback
          Swal.fire(
            `${Translator.trans('error_message_detail', {}, 'messages', $language)}`,
            `${Translator.trans('servizio.error_missing_filename', {}, 'messages', $language)}`,
            'error'
          );
        }
      });
  });

  // Show/Hide external card url
  const $externalCardUrlCheckbox = $('#service_group_enable_external_card_url');
  const $externalCardUrlTabs = $('#tabs-externalCardUrl');
  const $CardFieldsContainer = $('#card-fields-container');
  const hideExternalCardUrl = function () {
    if ($externalCardUrlCheckbox.is(":checked")) {
      $externalCardUrlTabs.show();
    } else {
      $("[id^='service_group_externalCardUrl_']" ).val('')
      $externalCardUrlTabs.hide();
    }
  };
  hideExternalCardUrl()
  $externalCardUrlCheckbox.click(function () {
    hideExternalCardUrl()
  });

  const limitChars = 2000;
  TextEditor.init(     (editor)  => {
    editor.on('input', function (event) {
      let numChars = tinymce.activeEditor.plugins.wordcount.body.getCharacterCount();
      //Update value
      let elm = event.srcElement.dataset.id
      $('.form-text text-muted').innerHTML = ''
      let html = `<small class='form-text text-muted'>${Translator.trans('servizio.max_limit_of', {}, 'messages', $language)} ${limitChars} ${Translator.trans('characters', {}, 'messages', $language)} (<span class='total-chars'>${numChars}</span> / <span class='max-chars'> ${limitChars} </span>)</small>`;
      $('.form-text.text-muted').remove()
      $('#'+elm).nextAll().after(html)
      //Check and Limit Charaters
      if (numChars > limitChars) {
        event.preventDefault();
        return false;
      }
    });
  })
})
