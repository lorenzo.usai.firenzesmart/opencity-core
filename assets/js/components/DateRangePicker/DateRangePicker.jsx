import React, {useEffect, useState} from 'react';
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import {Button} from "design-react-kit"
import "react-dates/lib/css/_datepicker.css";
import "../../../styles/components/DateRangePicker/react_dates_overrides.scss";
const lang = document.documentElement.lang.toString() || 'it'
import moment from "moment";
import {Default, Mobile} from "../../utils/Responsive";
moment.locale(lang)

function DateRangePickerWrapper({childToParent,detectChanges}) {

  const [startDate, setStartDate] = useState(moment().subtract(1, 'month'));
  const [endDate, setEndDate] = useState(moment());
  const [focusedInput, setFocusedInput] = useState(null);

  const onDatesChange = (startDate, endDate) => {
    setStartDate(startDate)
    setEndDate(endDate)
    childToParent({ startDate, endDate })
  }

  const onDetectDatesChange = (startDate, endDate) => {
    detectChanges({ startDate, endDate })
  }

  useEffect(() => {
    onDatesChange(moment().subtract(1, 'month'),moment())
  }, []);

  function renderDatePresets() {
    const presets = [
      {
        text: Translator.trans('time.last_month', {}, 'messages', lang),
        start: moment().subtract(1, 'month'),
        end: moment(),
      },
      {
        text: Translator.trans('time.last_2_month', {}, 'messages', lang),
        start: moment().subtract(2, 'months').startOf('month'),
        end: moment().endOf('month'),
      },
      {
        text: Translator.trans('time.last_6_month', {}, 'messages', lang),
        start: moment().subtract(6, 'months').startOf('month'),
        end: moment().endOf('month'),
      },
      {
        text: Translator.trans('time.last_year', {}, 'messages', lang),
        start: moment().subtract(12, 'months').startOf('month'),
        end: moment().endOf('month'),
      }
    ]

    return (
      <div className="d-block d-md-flex flex-row flex-md-column">
        {presets.map(({ text, start, end }) => {
          return (
            <Button
              size='xs'
              outline
              color={'primary'}
              key={text}
              type="button"
              onClick={() => onDatesChange(start,end)}
              className={'my-3 mx-2'}
            >
              {text}
            </Button>
          );
        })}
      </div>
    );
  }

    return (
        <div className='mx-0'>
          <Mobile>
            <p className={'mb-0 text-button-sm'}>
              <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period', {}, 'messages', lang)}</strong>
            </p>
            <DateRangePicker
              customArrowIcon={' '}
              calendarInfoPosition="before"
              renderCalendarInfo={renderDatePresets}
              startDatePlaceholderText={Translator.trans('iscrizioni.data_inizio', {}, 'messages', lang)}
              endDatePlaceholderText={Translator.trans('iscrizioni.data_fine', {}, 'messages', lang)}
              displayFormat={() => moment.localeData().longDateFormat('L')}
              minimumNights={0}
              isOutsideRange={day => (moment().diff(day) < 0)}
              startDate={startDate}
              startDateId="start_date_id_mobile"
              endDate={endDate}
              endDateId="end_date_id_mobile"
              monthFormat="MMMM YYYY"
              onDatesChange={({ startDate, endDate }) => {
                onDatesChange(startDate, endDate)
                onDetectDatesChange(startDate, endDate)
              }} // PropTypes.func.isRequired,
              focusedInput={focusedInput}
              onFocusChange={e => {setFocusedInput(e)}} // PropTypes.func.isRequired,
              orientation="vertical"
              verticalHeight={568}
              withFullScreenPortal
            />
          </Mobile>
          <Default>
            <p className={'mb-0 text-button-sm'}>
              <strong className="date-range-picker-label">{Translator.trans('pratica.selected_period', {}, 'messages', lang)}</strong>
            </p>
          <DateRangePicker
            anchorDirection="right"
            customArrowIcon={'-'}
            calendarInfoPosition="before"
            renderCalendarInfo={renderDatePresets}
            startDatePlaceholderText={Translator.trans('iscrizioni.data_inizio', {}, 'messages', lang)}
            endDatePlaceholderText={Translator.trans('iscrizioni.data_fine', {}, 'messages', lang)}
            displayFormat={() => moment.localeData().longDateFormat('L')}
            minimumNights={0}
            isOutsideRange={day => (moment().diff(day) < 0)}
            startDate={startDate}
            startDateId="start_date_id"
            endDate={endDate}
            endDateId="end_date_id"
            monthFormat="MMMM YYYY"
            onDatesChange={({ startDate, endDate }) => {
              onDatesChange(startDate, endDate)
              onDetectDatesChange(startDate, endDate)
            }} // PropTypes.func.isRequired,
            focusedInput={focusedInput}
            onFocusChange={e => {setFocusedInput(e)}} // PropTypes.func.isRequired,
          />
          </Default>
        </div>
    )
}



export default DateRangePickerWrapper;
