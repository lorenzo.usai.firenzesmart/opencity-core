import React, { useEffect, useRef, useState } from "react";
import {Button, Icon, Modal, ModalBody, ModalFooter, ModalHeader, notify, Progress, Spinner} from "design-react-kit";
import axios from "axios";
import { useAuth } from "../pages/contexts/AuthContext";
import { useLanguage } from "../pages/contexts/LanguageContext";
import { getBase64 } from "../utils/Base64";
import {useSearchParams} from "react-router-dom";
export const JobsFileUploader = ({ handleFile }) => {
  // Create a reference to the hidden file input element
  const hiddenFileInput = useRef(null);
  const [loading, setLoading] = useState(true);
  const [progress, setProgress] = useState(0);
  const [debouncing, setDebouncing] = useState(false);
  const [isOpen, toggleModal] = useState(false);
  const [jobId, setJobId] = useState(null);
  const [countJob, setCountJob] = useState(0);
  const [feedback, setFeedback] = useState("");

  const [searchParams] = useSearchParams();
  const apiUrl = window.location.pathname.split("/")[1];

  const { authUser } = useAuth();
  const { lang } = useLanguage();

  // Programatically click the hidden file input element
  // when the Button component is clicked
  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  // Call a function (passed as a prop from the parent component)
  // to handle the user-selected file
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    handleFile(fileUploaded);
    uploadJobs(fileUploaded);
  };

  useEffect(() => {
    if (debouncing) {
      const getData = setInterval(() => {
        getJobId();
      }, 10000);

      return () => clearInterval(getData);
    }
  }, [debouncing]);

  const getJobId = (id) => {
    setLoading(true);
    const config = {
      headers: {
        Authorization: `Bearer ${authUser.token}`,
      },
      onUploadProgress: (progressEvent) => {
        const progress = parseInt(
          Math.round((progressEvent.loaded * 100) / progressEvent.total)
        );
        // Update state here
        setProgress(progress);
      }
    };

    axios
      .get(`/${apiUrl}/api/jobs/${id ? id : jobId}`, config)
      .then((response) => {
        const countJobs = response.data.status === "pending" ? 1 : 0;
        if (countJobs > 0) {
          toggleModal(true)
          setDebouncing(true);
          setCountJob(countJobs);
        } else {
          setDebouncing(false);
          setProgress(0);
          setCountJob(countJobs);
          setFeedback(Translator.trans("servizio.due_import_success", {}, "messages", lang));
          toggleModal(false)
          location.reload()
        }
        setLoading(false);
      })
      .catch((e) => {
        notify(
          Translator.trans("warning", {}, "messages", lang),
          <p>{e.message}</p>,
          {
            dismissable: true,
            state: "error",
            duration: 3000,
          }
        );
        setProgress(0);
        setLoading(false);
      });
    setLoading(true);
  };

  const uploadJobs = (file) => {
    setLoading(true);
    const config = {
      headers: {
        Authorization: `Bearer ${authUser.token}`,
      },
      onUploadProgress: (progressEvent) => {
        const progress = parseInt(
          Math.round((progressEvent.loaded * 100) / progressEvent.total)
        );
        // Update state here
        setProgress(progress);
      },
    };

    getBase64(file).then((el) => {
      const extractOnlyData = el.split(",");

      const payload = {
        attachment: {
          name: file.name,
          mime_type: file.type,
          file: extractOnlyData[1],
        },
        args: {
          service_id: searchParams.get('service_id'),
        },
        type: "import_dovuti",
      };

      axios
        .post(`/${apiUrl}/api/jobs`, payload, config)
        .then((response) => {
          setJobId(response.data.id)
          getJobId(response.data.id);
          setLoading(false);
          setProgress(0);
        })
        .catch((e) => {
          notify(
            Translator.trans("warning", {}, "messages", lang),
            <p>{e.message}</p>,
            {
              dismissable: true,
              state: "error",
              duration: 3000,
            }
          );
          setProgress(0);
          setLoading(false);
        });
    });
  };

  return (
    <>
      {authUser?.token && searchParams.get('service_id') ? (
        <>
          <Button
            color="secondary"
            className="btn-progress"
            disabled={debouncing}
            onClick={handleClick}
            size={'sm'}
          >
            Carica file CSV  <Icon color="light" icon="it-upload" size={'xs'} aria-hidden />
            <span>{progress > 0 ? <Progress value={progress} /> : null}</span>
          </Button>
          <input
            type="file"
            onChange={handleChange}
            ref={hiddenFileInput}
            accept=".csv"
            style={{ display: "none" }} // Make the file input element invisible
          />
          <a
              href={'/bundles/app/files/import-dovuti.csv'}
              download="template-csv"
              target="_blank"
              rel="noreferrer"
          >
            <Button icon={true}>Scarica template <Icon icon={'it-file'}></Icon></Button>
          </a>
        </>
      ) : null}



      <Modal
          isOpen={isOpen}
          toggle={() => toggleModal(!isOpen)}
          labelledBy='jobModal'
      >
        <ModalHeader toggle={() => toggleModal(!isOpen)} id='jobModal'>
          {countJob > 0 ? (
              <p className="mb-3">
                <small>In attesa del caricamento dei dovuti
                  <div className='d-flex justify-content-center mt-3'><Spinner small active /></div>
                </small>
              </p>
          ) : null}
        </ModalHeader>
        <ModalBody>
          <p>
            {feedback && feedback !== ""
                ? <small className={'text-success'}>{feedback}</small>
                : null}
          </p>
          <p>
            {Translator.trans("servizio.due_import_loaded", {}, "messages", lang)}
          </p>
        </ModalBody>
        <ModalFooter>
        </ModalFooter>
      </Modal>
    </>
  );
};
