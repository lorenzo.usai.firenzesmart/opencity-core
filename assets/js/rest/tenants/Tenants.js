import BasePath from "../../utils/BasePath";
import Auth from "../auth/Auth";
import axios from "axios";

class Tenants {

  constructor() {
    this.token = null;
    this.basePath = null;
    this.init()
  }

  init() {
    const auth = new Auth();
    auth.getSessionAuthTokenPromise().then( res => {
      this.token = res.token
    });
    this.basePath = new BasePath().getBasePath()
  }

  patch(identifier, data) {
    let self = this;
    axios.patch(self.basePath + '/api/tenants/' + identifier, data, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

export default Tenants;
