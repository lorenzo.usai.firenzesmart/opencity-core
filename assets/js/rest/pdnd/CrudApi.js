const axios = require('axios');

class CRUDAPI {
  constructor(baseURL, token) {
    this.baseURL = baseURL;
    this.token = token;
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
      headers: {
        'Authorization': `Bearer ${this.token}`,
        'Content-Type': 'application/json'
      }
    });
  }

  // Method to set new JWT token
  setToken(token) {
    this.token = token;
    this.axiosInstance.defaults.headers['Authorization'] = `Bearer ${this.token}`;
  }

  async create(resource, data) {
    try {
      const response = await this.axiosInstance.post(`/${resource}`, data);
      return response.data;
    } catch (error) {
      console.error('Error creating resource:', error);
      throw error;
    }
  }

  async read(resource, id) {
    try {
      const response = await this.axiosInstance.get(`/${resource}/${id}`);
      return response.data;
    } catch (error) {
      console.error('Error reading resource:', error);
      throw error;
    }
  }

  async update(resource, id, data) {
    try {
      const response = await this.axiosInstance.put(`/${resource}/${id}`, data);
      return response.data;
    } catch (error) {
      console.error('Error updating resource:', error);
      throw error;
    }
  }

  async delete(resource, id) {
    try {
      const response = await this.axiosInstance.delete(`/${resource}/${id}`);
      return response.data;
    } catch (error) {
      console.error('Error deleting resource:', error);
      throw error;
    }
  }
}

// Example usage:
const api = new CRUDAPI('https://your-api-base-url.com', 'your-jwt-token');

// Create a new resource
api.create('posts', { title: 'New Post', content: 'Lorem ipsum...' })
  .then(data => console.log('Created:', data))
  .catch(error => console.error('Error:', error));

// Read a resource
api.read('posts', 123)
  .then(data => console.log('Read:', data))
  .catch(error => console.error('Error:', error));

// Update a resource
api.update('posts', 123, { title: 'Updated Post', content: 'New content...' })
  .then(data => console.log('Updated:', data))
  .catch(error => console.error('Error:', error));

// Delete a resource
api.delete('posts', 123)
  .then(data => console.log('Deleted:', data))
  .catch(error => console.error('Error:', error));
