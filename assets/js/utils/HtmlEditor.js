import * as ace from 'brace';
import 'brace/mode/html';
import 'brace/theme/monokai';

class HtmlEditor {

  static init() {

    const headerTextarea = $('.custom-template-header')
    if (headerTextarea.length > 0) {
      const headerEditor = ace.edit("header-editor");
      headerTextarea.hide();
      headerEditor.setTheme("ace/theme/monokai");
      headerEditor.session.setMode("ace/mode/html");
      headerEditor.getSession().setValue(headerTextarea.val());
      headerEditor.getSession().on('change', function () {
        headerTextarea.val(headerEditor.getSession().getValue());
      });
    }

    const footerTextarea = $('.custom-template-footer');
    if (footerTextarea.length > 0) {
      const footerEditor = ace.edit("footer-editor");
      footerTextarea.hide();
      footerEditor.setTheme("ace/theme/monokai");
      footerEditor.session.setMode("ace/mode/html");
      footerEditor.getSession().setValue(footerTextarea.val());
      footerEditor.getSession().on('change', function () {
        footerTextarea.val(footerTextarea.getSession().getValue());
      });
    }
  }
}

export default HtmlEditor;
