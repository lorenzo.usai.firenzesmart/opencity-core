export default function currencyFormatter(value) {
    if (!Number(value)) return "";

    const amount = new Intl.NumberFormat("it-IT", {
        style: "currency",
        currency: "EUR"
    }).format(value);

    return `${amount}`;
}
