import tinymce from "tinymce";
import {i18nTinymceITA} from "../translations/tinymce/langs/it";
import {i18nTinymceDE} from "../translations/tinymce/langs/de";

const lang = document.documentElement.lang.toString();

export class TextEditor {

  static init(callback) {
    const stripTags =   ['font', 'style', 'embed', 'param', 'script', 'html', 'body', 'head', 'meta', 'title', 'link', 'iframe', 'applet', 'noframes', 'noscript', 'form', 'input', 'select', 'option', 'colgroup', 'col', 'std', 'xml:', 'st1:', 'o:', 'w:', 'v:','h1','h2','div','span','meta','cite','svg','path','br'];
    const stripAttributes = ['font', 'style', 'embed', 'param', 'title','class','role','viewBox','ping','data-ved'];

    tinymce.init({
      selector: 'textarea:not(.simple)',
      menubar: false,
      max_height: 500,
      min_height: 200,
      language: lang,
      language_url: lang === 'de' ? i18nTinymceDE : lang === 'it' ? i18nTinymceITA : null, //Default is EN
      plugins: ['link', 'wordcount', 'code','autoresize','lists'],
      toolbar: 'undo redo | bold italic strikethrough | fontselect | link | formatOL formatUL numlist bullist | alignleft aligncenter alignright alignjustify | code',
      default_font_stack: [ 'Titillium Web','sans-serif' ],
      font_family_formats: 'Titillium Web, sans-serif',
      branding: false,
      content_style:
        "body { font-family: Titillium Web; }",
      setup: (editor) => {
        // Set text of the fields
        let messageTextEditor = null;
        editor.on('input', function (e) {
          messageTextEditor = tinymce.activeEditor.getContent()
          callback(editor,messageTextEditor)
        });
        editor.on('change', function (e) {
          if(!editor.getContent()){
            editor.setContent('<div>&nbsp</div>')
          }
          editor.save();
        })
        //Workaround for error "fields is not focusable"
        const inputs = document.querySelectorAll('textarea[required]:not(visible):not(.simple)');
        for (const input of inputs) {
          input.classList.add("tinymce");
        }
        callback(editor,messageTextEditor)
      },
      paste_preprocess: (editor, args) => {
        if (args.content) {
          let tagStripper = new RegExp('<[ /]*(' + stripTags.join('|') + ')[^>]*>', 'gi'),
            attributeStripper = new RegExp(' (' + stripAttributes.join('|') + ')(="[^"]*"|=\'[^\']*\'|=[^ ]+)?', 'gi'),
            commentStripper = new RegExp('<!--(.*)-->', 'g');
          args.content = args.content.toString().replace(commentStripper, '').replace(tagStripper, '').replace(attributeStripper, ' ').replace(/( class=(")?Mso[a-zA-Z]+(")?)/g, ' ').replace(/[\t ]+\</g, "<").replace(/\>[\t ]+\</g, "><").replace(/\>[\t ]+$/g, ">").replace(/[\u2018\u2019\u201A]/g, "'").replace(/[\u201C\u201D\u201E]/g, '"').replace(/\u2026/g, '...').replace(/[\u2013\u2014]/g, '-')
        }
      }
    })
  }

}
