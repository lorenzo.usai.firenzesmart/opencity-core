import InfoPayment from "../rest/payment/InfoPayment";

export function getPaymentStatus(status) {
  switch (status) {
    case 'CREATION_PENDING':
      return Translator.trans('payment.creation_pending', {}, 'messages', InfoPayment.$language);
    case 'PAYMENT_PENDING':
      return Translator.trans('STATUS_PAYMENT_PENDING', {}, 'messages', InfoPayment.$language);
    case 'CREATION_FAILED':
      return Translator.trans('STATUS_PAYMENT_CREATION_FAIL', {}, 'messages', InfoPayment.$language);
    case 'PAYMENT_STARTED':
      return Translator.trans('STATUS_PAYMENT_STARTED', {}, 'messages', InfoPayment.$language);
    case 'PAYMENT_CONFIRMED':
      return Translator.trans('STATUS_PAYMENT_SUCCESS', {}, 'messages', InfoPayment.$language);
    case 'PAYMENT_FAILED':
      return Translator.trans('STATUS_PAYMENT_ERROR', {}, 'messages', InfoPayment.$language);
    case 'NOTIFICATION_PENDING':
      return Translator.trans('STATUS_NOTIFICATION_PENDING', {}, 'messages', InfoPayment.$language);
    case 'COMPLETE':
      return Translator.trans('STATUS_PAYMENT_COMPLETE', {}, 'messages', InfoPayment.$language);
    case 'EXPIRED':
      return Translator.trans('STATUS_PAYMENT_EXPIRED', {}, 'messages', InfoPayment.$language);
    case 'CANCELED':
      return Translator.trans('STATUS_PAYMENT_CANCELED', {}, 'messages', InfoPayment.$language);
    default:
      console.log(`Status not found - ${status}.`);
  }
}

export function ifPaymentFailed(status) {
  return status === 'CREATION_FAILED' || status === 'PAYMENT_FAILED' || status === 'EXPIRED'
}
export function ifPaymentPending(status) {
  return  status === 'PAYMENT_STARTED' || status === 'PAYMENT_CONFIRMED' || status === 'NOTIFICATION_PENDING' || status === 'PAYMENT_PENDING'
}

export function ifPaymentToPay(status) {
  return status !== 'CREATION_FAILED' && status !== 'COMPLETE' && status !== 'PAYMENT_STARTED' && status !== 'PAYMENT_CONFIRMED' && status !== 'PAYMENT_FAILED' && status !== 'NOTIFICATION_PENDING' && status !== 'EXPIRED'
}
