import Swal from 'sweetalert2/src/sweetalert2.js';
import axios from "axios";
import ClipboardJS from "clipboard";
import Tenants from "../../rest/tenants/Tenants";
import Auth from "../../rest/auth/Auth";
require("jsrender")();    // Load JsRender as jQuery plugin (jQuery instance as parameter)

const auth = new Auth();
const tenants = new Tenants();
const language = document.documentElement.lang.toString();
new ClipboardJS('.btn-clipboard');

function getAuthConfig(token) {
  return {
    headers: {
      Authorization: `Bearer ${token}`,
    }
  };
}

function isValidUUID(uuid) {
  const uuidPattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
  return uuidPattern.test(uuid);
}


function getPdndClients(token) {
  const container = $("#pdnd-clients-container");
  const defaultPdndContainerId = container.data('default-pdnd-client-id');
  const tenantUrl = container.data('tenant-url');
  const tenantIdentifier = container.data('tenant-identifier');
  const authConfig = getAuthConfig(token);

  axios.get(container.data('url'), authConfig)
    .then(function (response) {
      // handle success
      if (response.data.meta.total >= 1) {
        $.each(response.data.data, function (index, value) {
          value.is_default = (value.id === defaultPdndContainerId);
          //$('.client-name').text(value.name)
          value.tenant_url = tenantUrl;
          container.append($.templates("#tpl-form").render(value));
        });
      } else {
        container.append($.templates("#tpl-empty").render());
      }
    })
    .catch(function (error) {
      if (error.response.status == 404) {
        container.append($.templates("#tpl-empty").render());
      }
    })
    .finally(function () {
      $('[data-toggle="popover"]').popover();
      $('.default').on('click', function(e) {
        e.preventDefault();
        const btn = $(this);
        const client = btn.parents('.client');

        Swal.fire({
          title: `${Translator.trans('pdnd.clients.list.card.set_default_tooltip_title', {}, 'messages', language)}`,
          text: `${Translator.trans('pdnd.clients.list.card.set_default_modal_text', {}, 'messages', language)}`,
          confirmButtonText: `${Translator.trans('pdnd.clients.list.card.set_default_tooltip_title', {}, 'messages', language)}`,
          showCancelButton: true,
          cancelButtonText: `${Translator.trans('annulla', {}, 'messages', language)}`
        }).then((result) => {
          if (result.isConfirmed) {
            tenants.patch(tenantIdentifier, {
              "default_pdnd_client_id": client.data('id')
            });
            $('.default i').removeClass('fa-star').addClass('fa-star-o');
            btn.children('i').removeClass('fa-star-o').addClass('fa-star');
          }
        });
      })

      $('.delete').on('click', (e) => {
        e.preventDefault();
        const client = $(e.target).parents('.client');
        const clientName = $(client.find('.card-title')[0]).text();
        console.log(clientName)

        Swal.fire({
          title: `<div class="text-left"><svg class="icon icon-danger"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-warning-circle"></use></svg><span class="text-danger ml-2">${Translator.trans('pdnd.clients.list.card.delete.modal.title', {}, 'messages', language)}</span></div>`,
          html: `<div class="text-left">${Translator.trans('pdnd.clients.list.card.delete.modal.text', {}, 'messages', language)}</div>`,
          confirmButtonText: `${Translator.trans('pdnd.clients.list.card.delete.modal.confirm_button', {}, 'messages', language)}`,
          confirmButtonColor: "#d9364f",
          showCancelButton: true,
          cancelButtonText: `${Translator.trans('annulla', {}, 'messages', language)}`,
          input: "text",
          inputPlaceholder: `${Translator.trans('pdnd.clients.list.card.delete.modal.input_placeholder', {}, 'messages', language)}`,
          inputAutoFocus: false,
          inputValidator: (value) => {
            if (!value) {
              return `${Translator.trans('pdnd.clients.list.card.delete.modal.empty_alert', {}, 'messages', language)}`;
            }
            if (value !== clientName) {
              return `${Translator.trans('pdnd.clients.list.card.delete.modal.wrong_input_alert', {'client_name': clientName}, 'messages', language)}`;
            }
          }
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            axios.delete(container.data('url') + '/' + client.data('id'), authConfig)
              .then( (response)=> {
                Swal.fire({
                  title: `${Translator.trans('pdnd.clients.list.card.delete.success', {}, 'messages', language)}`,
                  icon: "success",
                });
                client.parent().remove();
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        });
      })
    });
}

// List clients
if ($("#pdnd-clients-container").length) {
  auth.execAuthenticatedCall(getPdndClients)
}

function createNewClient(token) {
  const container = $("#pdnd-new-client-container");
  const form = $('#new-client');
  const saveBtn = form.find('.save');
  const authConfig = getAuthConfig(token);

  axios.get(container.data('keys-url'), authConfig)
    .then(function (response) {
      $('#key_pair_id ').val(response.data.id);
      $('#public_key').val(response.data.public_key);

    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });

  saveBtn.on('click', () => {
    console.log(form.serializeArray())
    let hasError = false;
    let payload = {};
    $.map(form.serializeArray(), function (n, i) {
      if (n['value'] == '') {
        hasError = true;
        Swal.fire({
          title: `${Translator.trans('pdnd.clients.new.empty_alert', {}, 'messages', language)}`,
          icon: "error"
        });
        return;
      }
      payload[n['name']] = n['value'];
    });

    if (!isValidUUID($('#key_id').val())) {
      hasError = true;
      Swal.fire({
        title: `${Translator.trans('pdnd.clients.new.uuid_alert', {}, 'messages', language)}`,
        icon: "error"
      });
    }


    if (!hasError) {
      // Passiamo anche l'id che per convenzione teniamo uguale al kid
      payload.id = payload.key_id;
      axios.post(container.data('post-url'), payload, authConfig)
        .then(function (response) {
          Swal.fire({
            title: `${Translator.trans('pdnd.clients.new.success', {}, 'messages', language)}`,
            icon: "success",
            showConfirmButton: false
          });
          window.location.replace(container.data('back-url') + '/' + response.data.id);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  });
}

if ($("#pdnd-new-client-container").length) {
  auth.execAuthenticatedCall(createNewClient)
}

function initClientDetail(token) {
  const container = $("#pdnd-client-detail-container");
  const eservicesContainer = $("#pdnd-eservices-container");
  const clientID = eservicesContainer.data('client-id');
  const configsApiUrl = eservicesContainer.data('configs-url')
  const configsByEservices = eservicesContainer.data('configs-by-eservices');
  const configs = eservicesContainer.data('configs');
  const authConfig = getAuthConfig(token);
  const tenantIdentifier = container.data('tenant-identifier');

  axios.get(container.data('url'), authConfig)
    .then(function (response) {
      container.append($.templates("#tpl-form").render(response.data));
      $('.client-name').text(response.data.name);
    })
    .catch(function (error) {
      console.log(error);
    })
    .finally(function () {
      $('.client-save').click(function (e) {
        e.preventDefault();
        const formClient = $(e.target).parents('form');
        let payload = {}
        let hasError = false;
        $.map(formClient.serializeArray(), function (n, i) {
          if (n['value'] == '') {
            hasError = true;
            Swal.fire({
              title: `${Translator.trans('pdnd.clients.new.empty_alert', {}, 'messages', language)}`,
              icon: "error"
            });
            return;
          }
          payload[n['name']] = n['value'];
        });

        if (!isValidUUID($('#key_id').val())) {
          hasError = true;
          Swal.fire({
            title: `${Translator.trans('pdnd.clients.new.uuid_alert', {}, 'messages', language)}`,
            icon: "error"
          });
        }
        if (!hasError) {
          axios.put(container.data('url'), payload, authConfig)
            .then(function (response) {
              Swal.fire({
                title: `${Translator.trans('pdnd.clients.edit.success', {}, 'messages', language)}`,
                icon: "success"
              });
            })
            .catch(function (error) {
              console.log(error);
              Swal.fire({
                title: `${Translator.trans('pdnd.clients.edit.error', {}, 'messages', language)}`,
                icon: "error"
              });
            });
        }

      });
    });

  // Qui iniziano gli e-services
  axios.get(eservicesContainer.data('url'), authConfig)
    .then(function (response) {
      // handle success
      if (response.data.meta.total >= 1) {

        console.log(configsByEservices)

        $.each(response.data.data, function (index, value) {
          if (configsByEservices.hasOwnProperty(value.id)) {
            value.config = configsByEservices[value.id];
          } else {
            value.config = {
              id: null
            }
          }
          eservicesContainer.append($.templates("#tpl-eservice").render(value));
        });
      } else {
        eservicesContainer.append($.templates("#tpl-empty").render());
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      $('.toggle-eservices').click(function (e) {
        e.preventDefault();
        const eserviceContainer = $(e.target).parents('.e-service');
        const eServiceId = eserviceContainer.data('eservice-id');
        const configId = eserviceContainer.data('config-id');

        let eserviceHasConfig = false;
        if (configId.length > 0) {
          eserviceHasConfig = true;
        }

        const html = `
                <div>
                  <div class="col-12">
                    <div class="form-group">
                      <input type="text" class="form-control" id="purpose-id" value="">
                      ${Translator.trans('pdnd.e-services.enable.help', {}, 'messages', language)}
                    </div>
                  </div>
                </div>`
        Swal.fire({
          title: eserviceHasConfig ? `${Translator.trans('pdnd.e-services.enable.title_edit', {}, 'messages', language)}` : `${Translator.trans('pdnd.e-services.enable.title_new', {}, 'messages', language)}`,
          html: html,
          showCloseButton: true,
          confirmButtonText: `${Translator.trans('salva', {}, 'messages', language)}`,
          showCancelButton: false,
          didOpen: () => {
            axios.get(configsApiUrl + '/' + configId, authConfig)
              .then(function (response) {
                const purposeID = Swal.getPopup().querySelector("#purpose-id");
                $(purposeID).val(response.data.purpose_id)
              })
              .catch(function (error) {
                console.log(error)
              });
          },
        }).then((result) => {
          if (result.isConfirmed) {
            const payload = {
              "client_id": clientID,
              "eservice_id": eServiceId,
              "is_active": true,
              "purpose_id": Swal.getPopup().querySelector("#purpose-id").value
            }
            if (!eserviceHasConfig) {
              axios.post(eservicesContainer.data('configs-url'), payload, authConfig)
                .then(function (response) {

                  eserviceContainer.data('config-id', response.data.id);
                  eserviceContainer.find('.status').each((i, el) => {
                    $(el).removeClass('badge-danger').addClass('badge-success').text(`${Translator.trans('enabled', {}, 'messages', language)}`)
                  })
                  eserviceContainer.find('.btn').each((i, el) => {
                    $(el).toggle()
                  })

                  configs.push(response.data.id);
                  tenants.patch(tenantIdentifier, {
                    "pdnd_config_ids": configs
                  });
                  eservicesContainer.data('configs-by-eservices', JSON.stringify(configs));
                  Swal.fire({
                    title: `${Translator.trans('pdnd.e-services.enable.success', {}, 'messages', language)}`,
                    icon: "success"
                  });
                })
                .catch(function (error) {
                  console.log(error);
                  Swal.fire({
                    title: `${Translator.trans('pdnd.e-services.enable.error', {}, 'messages', language)}`,
                    text: error.response.data.detail,
                    icon: "error"
                  });
                });
            } else {

              axios.put(eservicesContainer.data('configs-url') + '/' + configId, payload, authConfig)
                .then(function (response) {

                  eserviceContainer.find('.status').each((i, el) => {
                    $(el).removeClass('badge-danger').addClass('badge-success').text(`${Translator.trans('enabled', {}, 'messages', language)}`)
                  })
                  eserviceContainer.find('.btn').each((i, el) => {
                    $(el).toggle()
                  })

                  Swal.fire({
                    title: `${Translator.trans('pdnd.e-services.edit.success', {}, 'messages', language)}`,
                    icon: "success"
                  });
                  eserviceContainer.data('config-id', response.data.id);
                })
                .catch(function (error) {
                  console.log(error);
                  Swal.fire({
                    title: `${Translator.trans('pdnd.e-services.edit.error', {}, 'messages', language)}`,
                    icon: "error"
                  });
                });
            }
          }
        });
      })

      // Disable button
      $('.disable-eservices').click((e) => {
        e.preventDefault();

        const eserviceContainer = $(e.target).parents('.e-service');
        const configId = eserviceContainer.data('config-id');

        axios.patch(eservicesContainer.data('configs-url') + '/' + configId , {"is_active": false}, authConfig)
          .then(function (response) {
            Swal.fire({
              title: `${Translator.trans('pdnd.e-services.disable.success', {}, 'messages', language)}`,
              icon: "success"
            });
            eserviceContainer.find('.status').each((i, el) => {
              $(el).removeClass('badge-success').addClass('badge-danger').text('Abilitato')
            })
            eserviceContainer.find('.btn').each((i, el) => {
              $(el).toggle()
            })
          })
          .catch(function (error) {
            console.log(error);
            Swal.fire({
              title: `${Translator.trans('pdnd.e-services.disable.error', {}, 'messages', language)}`,
              icon: "error"
            });
          });
      })

    });
}

if ($("#pdnd-client-detail-container").length) {
  auth.execAuthenticatedCall(initClientDetail)
}
