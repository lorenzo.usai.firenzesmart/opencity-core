import React, {useEffect, useState} from 'react';
import PaymentListItem from "./PaymentListItem";
import {Spinner, notify, NotificationManager,Dropdown,DropdownToggle,LinkList,DropdownMenu,LinkListItem} from "design-react-kit"
import axios from 'axios';
import {useAuth} from "../contexts/AuthContext";
import {uniqueArrayProperty} from "../../utils/UniqueArrayProperty";
import {getPaymentStatus} from "../../utils/PaymentStatus";
import {useLanguage} from "../contexts/LanguageContext";
import DateRangePickerWrapper from "../../components/DateRangePicker/DateRangePicker";
import {useSearchParams} from "react-router-dom";
import moment from "moment/moment";


function PaymentList() {

  const [paymentList, setPaymentList] = useState([])
  const [filterList, setFilterList] = useState([])
  const [loading, setLoading] = useState(true);
  const [open, toggle] = useState(false);
  const [status, setStatus] = useState(false);
  const [createdAt, setCreatedAt] = useState(`created_at[after]=${moment().subtract(1, 'month').format("YYYY-MM-DD")}&created_at[before]=${moment().add(1, 'month').format("YYYY-MM-DD")}`);
  const [searchParams] = useSearchParams();

  const apiUrl =
      window.location.pathname.split("/")[1];

  const {
    authUser
  } = useAuth()

  const  {
    lang
  } = useLanguage()

  useEffect(() => {
    if (authUser?.token && authUser?.token !== '') {
      getPayments()
    }
  }, [authUser?.token]);



  const getPayments = () => {
    setLoading(true);
    const config = {
      headers: {
        Authorization: `Bearer ${authUser?.token}`
      }
    };

    axios.get(`/${window.location.pathname.split('/')[1]}/api/payments?${searchParams.get('service_id') ? `service_id=${searchParams.get('service_id')}` : ''}${status ? `&status=${status}` : ''}${createdAt ? `&${createdAt}` : ''}`, config)
      .then(list => {
        list.data.data = list.data.data.filter(payment => payment.status !== 'CREATION_PENDING')
        setPaymentList(list.data.data)
        if (!status) {
          setFilterList(uniqueArrayProperty(list.data.data, 'status'))
        }
        setLoading(false);
      }).catch((e) => {
      if (e.response.status !== 404) {
        notify(
          Translator.trans('warning', {}, 'messages', lang),
          <p>
            {e.message}
          </p>, {
            dismissable: true,
            state: 'error',
            duration: 3000
          }
        )
      }else{
        setPaymentList([])
      }
      setLoading(false);
    })
  }

  useEffect(() => {
    if (authUser?.token && (status || status === null || createdAt)) {
      getPayments()
    }
  }, [status,createdAt,authUser?.token]);


  const handleClickFilterStatus = (chosenState) => {
    if (status === chosenState) {
      setStatus(null)
    } else {
      setStatus(chosenState)
    }
    toggle(!open)
  }

  const childToParent = ({ startDate, endDate }) => {
    if(startDate && endDate){
      setCreatedAt(`created_at[after]=${startDate.format('YYYY-MM-DD')}&created_at[before]=${endDate.add(1, 'month').format('YYYY-MM-DD')}`)
    }
  }


  return (
    <section className="it-page-section mb-50 mb-lg-90">
      <div className="">
        <div className="filter-section">
          {filterList?.length > 0 ?
            <div className="filter-wrapper d-flex flex-column flex-md-row justify-content-between align-items-start my-3">
              <Dropdown className='m-3' isOpen={open} toggle={() => toggle(!open)}>
                <DropdownToggle color='primary' caret tag='a' className='btn p-0 pe-2'>
                 <span className="rounded-icon">
                  <svg className="icon icon-primary icon-xs me-1" aria-hidden="true">
                     <use href="/bootstrap-italia/dist/svg/sprite.svg#it-funnel"></use>
                  </svg>
                </span>
                  <span>{status ? getPaymentStatus(status): Translator.trans('pratica.filter', {}, 'messages', lang)}</span>
                </DropdownToggle>
                <DropdownMenu>
                  <LinkList className='pl-0'>
                    {filterList.map((item, index) => (
                      <LinkListItem key={`item-${index}`} size='large'
                                    className="dropdown-item list-item cursor-pointer" active={status === item.status}
                                    onClick={() => handleClickFilterStatus(item.status)}>{getPaymentStatus(item.status)}</LinkListItem>
                    ))}
                  </LinkList>
                </DropdownMenu>
              </Dropdown>
              <DateRangePickerWrapper childToParent={childToParent}></DateRangePickerWrapper>
            </div>
            : null}
        </div>
      </div>

      <div className="cmp-accordion">
        <div className="accordion">
          {paymentList.length === 0 && loading ? <span className="d-flex justify-content-center">
              <Spinner small active/><span className={'mx-2'}>{Translator.trans('loading', {}, 'messages', lang)}</span> </span> :
            paymentList.length > 0 && loading ? <span className="d-flex justify-content-center">
                <Spinner small active/><span className={'mx-2'}>{Translator.trans('loading', {}, 'messages', lang)}</span> </span> :
              paymentList.map((payment, index) => (
                <PaymentListItem key={payment.id} payment={payment}></PaymentListItem>
              ))
          }
          {paymentList.length === 0 && !loading ? Translator.trans('backoffice.integration.subscription_service.payment.no_payments', {}, 'messages', lang) : ''}
        </div>
      </div>
      <NotificationManager></NotificationManager>
    </section>
  );
}

export default PaymentList;
