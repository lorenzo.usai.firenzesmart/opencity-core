import React, {useId, useState} from 'react';
import moment from "moment";
import {getPaymentStatus, ifPaymentFailed, ifPaymentPending, ifPaymentToPay} from "../../utils/PaymentStatus";
import {Button} from "design-react-kit"
const tabElement = document.querySelector("[data-list-payment-path]")
import folderPay from '../../../images/folder-pay.svg';
import folderToPay from '../../../images/folder-to-pay.svg';
import {useLanguage} from "../contexts/LanguageContext";


function PaymentListItem({payment}) {

  const [collapse, setCollapse] = useState(false)
  const accordion = useId();

  const  {
    lang
  } = useLanguage()

  const handleDropdown = () => {
    setCollapse(!collapse)
  };

  return (
      <div className="accordion-item">
        <div className="accordion-header" id={`heading-${accordion}`}>
          <button className={`accordion-button  ${collapse ? '' : 'collapsed'}`} type="button" onClick={handleDropdown}
                  data-bs-toggle="collapse" aria-expanded={collapse}
                  aria-controls={`collapse-${accordion}`} data-focus-mouse="false">
            <div className="button-wrapper d-block d-md-flex">
              {payment?.reason}
              <div className="icon-wrapper w-100 w-lg-25 mr-3 text-sm-left text-md-right">
                <img className="icon-folder" src={
                  ifPaymentFailed(payment?.status) ? folderToPay :
                    ifPaymentPending(payment?.status)
                    ? folderToPay : folderPay } alt={getPaymentStatus(payment?.status)}
                     role="img"/>
                <span className={
                  ifPaymentFailed(payment?.status) ? 'u-main-error' : ifPaymentPending(payment?.status) ? 'u-main-alert' : 'text-success' }>{' '}{getPaymentStatus(payment?.status)}</span>
              </div>
            </div>
          </button>
            <p className="accordion-date title-xsmall-regular mb-2">{Translator.trans('payment.amount', {}, 'messages', lang)}: <span className="label"><strong>{payment?.payment?.amount} {payment?.payment?.currency}</strong></span></p>
            {ifPaymentToPay(payment?.status)
                ?  (
                    <div className={'mx-3'}>
                        {payment?.links.online_payment_begin.url ?
                            <a href={payment?.links.online_payment_begin.url} className="btn btn-primary justify-content-center my-3 le-3">
                                <span className="">{Translator.trans('payment.paga_online', {}, 'messages', lang)}</span>
                            </a> : null }
                        {payment?.links.offline_payment.url !== null ?
                            <a href={payment?.links.offline_payment.url} className="btn justify-content-center my-3 ml-3 btn-outline-primary">
                                <span className="">{Translator.trans('payment.paga_offline', {}, 'messages', lang)}</span>
                            </a> : null }
                    </div>
                ) : null}
            <p className="accordion-date title-xsmall-regular mb-0">{moment(moment.utc(payment?.created_at).toDate()).local(lang).format('L LTS')}</p>
        </div>

        <div id={`collapse-${accordion}`} className={`accordion-collapse p-0 collapse ${collapse ? 'show' : ''}`}
             role="region" aria-labelledby={`heading-${accordion}`}>
          <div className="accordion-body">
            <p className="mb-2 fw-normal text-capitalize">{Translator.trans('pratica.applications', {}, 'messages', lang)}: <span className="label">{payment?.id}</span></p>
            <p className="mb-2 fw-normal text-capitalize">{Translator.trans('payment.type', {}, 'messages', lang)}: <span className="label">{payment?.type}</span></p>
            <p className="mb-2 fw-normal text-capitalize">{Translator.trans('payment.amount', {}, 'messages', lang)}: <span className="label">{payment?.payment?.amount} {payment?.payment?.currency}</span></p>

            <a href={`/${window.location.pathname.split('/')[1]}/servizi/${payment?.service_id}`} className="mb-2">
              <span className="t-primary">{Translator.trans('service_card', {}, 'messages', lang)}</span>
            </a>

            {!payment?.service_id ?
            <a className="chip chip-simple" href="#">
              <span className="chip-label">{Translator.trans('no_digital_service', {}, 'messages', lang)}</span>
            </a>
              : null}

            <div className="cmp-icon-list">
              <div className="link-list-wrapper">
                <ul className="link-list">
                  {payment?.status === 'COMPLETE' && payment?.links?.receipt.url !== null ? (
                  <li className="shadow p-0">
                    <a className="list-item icon-left t-primary title-small-semi-bold" href={payment?.links?.receipt.url}
                       aria-label={Translator.trans('receipt_required', {}, 'messages', lang)} download target={'_blank'}>
                      <span className="list-item-title-icon-wrapper">
                        <svg className="icon icon-sm align-self-start icon-color mt-0" aria-hidden="true"><use
                          href="/bootstrap-italia/dist/svg/sprite.svg#it-clip"></use></svg>
                        <span
                          className="list-item-title title-small-semi-bold mt-0">{Translator.trans('receipt_required', {}, 'messages', lang)}</span>
                      </span>
                    </a>
                  </li>
                  ) : null}
                </ul>
              </div>
            </div>

            <Button size='xs' tag={'a'} outline color='primary' role='button' className="title-xsmall-semi-bold" href={`${tabElement}/${payment.id}`}>
              <span className="">{Translator.trans('operatori.vai_al_dettaglio', {}, 'messages', lang)}</span>
            </Button>
          </div>
        </div>
      </div>
  );
}

export default PaymentListItem;
