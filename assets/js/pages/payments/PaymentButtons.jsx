import React from 'react';
import pagopa from "../../../images/pagopa.svg";
import {useLanguage} from "../contexts/LanguageContext";

function PaymentButtons({payment}) {

    const  {
        lang
    } = useLanguage()

    return (
        <div className="cmp-text-button mb-40 mb-lg-80 mt-lg-40 d-flex">
            {payment?.status !== 'COMPLETE' && payment?.links?.online_payment_begin?.url !== null  ?
                (  <div className="button-wrapper mr-3">
                    <a href={payment?.links?.online_payment_begin?.url} className="btn btn-icon btn-re  btn-primary mb-2 justify-content-center" role="button" aria-pressed="true">
                        <img src={pagopa} alt="paga con pagoPA" className="me-2 icon"/>
                        <span className="text-capitalize">{Translator.trans('payment.payment_pagopa', {}, 'messages', lang)}</span>
                    </a>
                </div>) :null}
            {payment?.status !== 'COMPLETE' && payment?.links?.offline_payment?.url !== null ?
                (  <div className="button-wrapper mr-3">
                    <a target="_blank" href={payment?.links?.offline_payment?.url} className="btn btn-outline-primary " role="button" aria-pressed="true" download> {Translator.trans('payment.paga_offline', {}, 'messages', lang)}</a></div>) : null}

            {payment?.status !== 'COMPLETE' && payment?.links?.online_payment_landing?.url !== null ?
                (  <div className="button-wrapper mr-3"><a target="_blank" href={payment?.links?.online_payment_landing?.url} className="btn btn-re btn-outline-primary mb-2 justify-content-center" role="button" aria-pressed="true"> {Translator.trans('operatori.vai_al_dettaglio', {}, 'messages', lang)}</a></div>) : null}

            {payment?.status === 'COMPLETE' && payment?.links?.receipt.url !== null ? (
                <div className="button-wrapper mr-3">
                    <a aria-label="Scarica la Ricevuta richiesta" download target={'_blank'} href={payment?.links?.receipt.url} className="btn btn-re btn-outline-primary mb-2 justify-content-center" role="button" aria-pressed="true"> {Translator.trans('receipt_required', {}, 'messages', lang)}</a></div>) : null}
        </div>
    )
}

export default PaymentButtons;
