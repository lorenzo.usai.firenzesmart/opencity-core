import Calendar from '../Calendar';
import DynamicCalendar from '../DynamicCalendar';
import PageBreak from '../PageBreak';
import FinancialReport from "../FinancialReport";
import SdcFile from "../SdcFile";
import 'formiojs';
import 'formiojs/dist/formio.form.min.css';
import Swal from 'sweetalert2/src/sweetalert2.js';
import "@sweetalert2/theme-bootstrap-4/bootstrap-4.min.css";
import Users from "../rest/users/Users";
import Form from '../Formio/Form';
import FormIoHelper from "../utils/FormIoHelper";

Formio.registerComponent('calendar', Calendar);
Formio.registerComponent('dynamic_calendar', DynamicCalendar);
Formio.registerComponent('pagebreak', PageBreak);
Formio.registerComponent('financial_report', FinancialReport);
Formio.registerComponent('sdcfile', SdcFile);
const language = document.documentElement.lang.toString();

window.onload = function () {

  window.FormioHelper = new FormIoHelper();
  window.FormioHelper.init();

  const applicationBuiltInFormContainer = $('#formio-builtin');
  const applicationFormContainer = $('#formio');

  if (applicationFormContainer.length) {
    Form.initApplicationForm(applicationFormContainer)
  } else if (applicationBuiltInFormContainer.length) {
    Form.initBuiltInForm(applicationBuiltInFormContainer);
  }

};

$(document).ready(function () {
  let $input = $('#autocomplete-users')
  let $autocomplete = $('#users-list');
  let url = $input.data('url')
  const users = new Users()
  users.init();

  $input.on('input', function (e) {
    const q = $input.val()
    $autocomplete.empty()
    if (q.length === 16) {
      $('.autocomplete-icon').html('<i class="fa fa-circle-o-notch fa-spin fa-fw" aria-hidden="true"></i>')
      users.getUsers(q)
        .fail(function (xhr, type, exception) {
          console.log(xhr)
          console.log(type)
          console.log(exception)
          Swal.fire(
            'Oops...',
            'Something went wrong!',
            'error'
          );
        })
        .done(function (data, code, xhr) {

          if (data.length) {
            for (const item in data) {
              let optionText = data[item].nome + ' ' + data[item].cognome;
              let optionLabel = '<em>' + data[item].codice_fiscale + '</em>';
              let optionIcon =  '<svg class="icon icon-sm"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-user"></use></svg>';
              let optionLink = url + '?user=' + data[item].id;

              $autocomplete.addClass('autocomplete-list-show')
              let option = $(`<li>
                <a href="${optionLink}">
                  ${optionIcon}
                  <span class="autocomplete-list-text">
                    <span>${optionText}</span>
                    ${optionLabel}
                  </span>
                </a>
                </li>`
              )
              $autocomplete.append(option)
            }
          }
          $('.autocomplete-icon').html('<i class="fa fa-search" aria-hidden="true"></i>');
        });

    } else if (q.length === 0) {
      $autocomplete.removeClass('autocomplete-list-show');
    } else {
      $autocomplete.addClass('autocomplete-list-show');
      $autocomplete.html(`<li class="text-danger text-center"><span class="autocomplete-list-text">${Translator.trans('pratica.enter_correct_cf', {}, 'messages', language)}</span></li>`)
    }
  })


})