import Calendar from '../Calendar';
import DynamicCalendar from '../DynamicCalendar';
import PageBreak from '../PageBreak';
import FinancialReport from "../FinancialReport";
import SdcFile from "../SdcFile";
import 'formiojs';
import '../../styles/vendor/_formio.scss';
import '../../styles/components/_messages.scss';
import {TextEditor} from "../utils/TextEditor";
import moment from "moment";
import RequestIntegration from "../utils/RequestIntegration";
import InfoPayment from "../rest/payment/InfoPayment";
import ApplicationsMessage from "../rest/applications/Message";
import Form from '../Formio/Form';
import Users from "../rest/users/Users";
import Swal from 'sweetalert2/src/sweetalert2.js'
import {ApplicationMap} from "../utils/ApplicationMap";
import String from "../utils/String";

import '../../app/js/chosen.jquery.min'
import '../../app/css/component-chosen.min.css'

Formio.registerComponent('calendar', Calendar);
Formio.registerComponent('dynamic_calendar', DynamicCalendar);
Formio.registerComponent('pagebreak', PageBreak);
Formio.registerComponent('financial_report', FinancialReport);
Formio.registerComponent('sdcfile', SdcFile);

import VueSweetalert2 from 'vue-sweetalert2';
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

import unicodeToChar from '../utils/unicodeToChar';
const plugins = {
  install() {
    Vue.helpers = unicodeToChar;
    Vue.prototype.$helpers = unicodeToChar;
  }
}

Vue.config.productionTip = false
Vue.use(VueSweetalert2);
Vue.use(plugins);

const language = document.documentElement.lang.toString();

// Todo: spostare in ./assets/js/Formio/Formio.js
window.onload = function () {
  // Init formIo
  if ($('#formio_summary').length > 0) {
    Form.init('formio_summary');
  }

  // Backoffice
  const backofficeFormContainer = $('#backoffice-form');
  if (backofficeFormContainer.length) {
    const saveInfo = $('.save-backoffice-info');
    const backofficeTextInfo = saveInfo.find('span');
    const backofficeFormIOI18n = {
      en: {},
      de: {},
      it: {
        next: `${Translator.trans('following', {}, 'messages', language)}`,
        previous: `${Translator.trans('previous', {}, 'messages', language)}`,
        cancel: `${Translator.trans('annulla', {}, 'messages', language)}`,
        submit: `${Translator.trans('salva', {}, 'messages', language)}`,
      }
    }
    Formio.icons = 'fontawesome';
    Formio.createForm(document.getElementById('backoffice-form'), backofficeFormContainer.data('formserver_url') + '/form/' + backofficeFormContainer.data('form_id'), {
      noAlerts: true,
      language: language,
      i18n: backofficeFormIOI18n,
      buttonSettings: {
        showCancel: false
      }
    }).then(function (form) {
      form.formReady.then(() => {
        // On ready
      });

      // Recupero i dati della pratica se presenti
      if (backofficeFormContainer.data('submission') !== '' && backofficeFormContainer.data('submission') !== null) {
        form.submission = {
          data: backofficeFormContainer.data('submission').data
        };
      }

      form.on('prevPage', function () {
      });

      $('.btn-wizard-nav-cancel').on('click', function (e) {
        e.preventDefault()
        location.reload();
      })

      form.nosubmit = true;

      // Triggered when they click the submit button.
      form.on('submit', function (submission) {
        let submitButton = backofficeFormContainer.find('.btn-wizard-nav-submit');
        submitButton.html(`<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>${Translator.trans('salva', {}, 'messages', language)}`)
        axios.post(backofficeFormContainer.data('backoffice-save-url'), submission.data)
          .then(function (response) {
            saveInfo.removeClass('d-none');
            backofficeTextInfo.text(`${Translator.trans('time.few_seconds_ago', {}, 'messages', language)}`)
            form.emit('submitDone', submission)
          })
          .catch(function (error) {
            saveInfo.removeClass('d-none');
            backofficeTextInfo.text(`${Translator.trans('servizio.error_from_save', {}, 'messages', language)}`)
          })
          .then(function () {
            submitButton.html(`${Translator.trans('salva', {}, 'messages', language)}`)
          });
      });
    });
  }
};

$(document).ready(function () {
  const userGroupInput = $('#user_group');
  const assignedUserGroupID = userGroupInput.data('value')
  const operatorInput = $('#operator');
  const assigneOperatorID = operatorInput.data('value')
  const assignBtn = $('#assign_operator_btn');
  const assigneeLoader = $('#assignee-loader')
  const users = new Users()

  //Disabled Init zoom function
  //Zoom($)


  function initUserGroupsSelect() {
    assigneeLoader.html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
    users.getUserGroups()
      .fail(function (xhr, type, exception) {
        assigneeLoader.html('');
        Swal.fire(
          `${Translator.trans('error_message_detail', {}, 'messages', language)}`,
          `${Translator.trans('operatori.error_get_users_groups', {}, 'messages', language)}`,
          'error'
        );
      })
      .done((data) => {
        const emptyOption = $(`<option disabled selected value>${Translator.trans('operatori.select_user_group', {}, 'messages', language)}</option>`);
        userGroupInput.append(emptyOption);
        data.forEach((item) => {
          let option = $(`<option value="${item.id}">${item.name}</option>`);
          if (item.id === assignedUserGroupID) {
            option.attr('selected', 'selected');
          }
          userGroupInput.append(option);
        })
        assigneeLoader.html('');

        if(!userGroupInput.val()){
          userGroupInput.chosen({no_results_text: `${Translator.trans('operatori.no_found_select_user_group', {}, 'messages', language)}`,allow_single_deselect: true});
          userGroupInput.trigger("chosen:updated");
        }else{
          userGroupInput.trigger('change');
        }


      })
  }

  initUserGroupsSelect();
  function initFilteredOperatorsSelect() {

    if (userGroupInput.val() && users.token) {
      assigneeLoader.html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
      try {
        users.getFilteredOperators(userGroupInput.val())
          .fail(function (xhr, type, exception) {
            assigneeLoader.html('');
            Swal.fire(
              `${Translator.trans('error_message_detail', {}, 'messages', language)}`,
              `${Translator.trans('operatori.error_get_operators', {}, 'messages', language)}`,
              'error'
            );
          })
          .done((data) => {
            assigneeLoader.html('');
            let emptyOption = $(`<option selected value>${Translator.trans('operatori.select_operator', {}, 'messages', language)}</option>`);
            operatorInput.append(emptyOption);
            data.forEach((item) => {
              let option = $(`<option value="${item.id}">${item.full_name}</option>`);
              if (item.id === assigneOperatorID) {
                option.attr('selected', 'selected');
              }
              operatorInput.append(option);
            })

            operatorInput.removeClass("d-none");
            operatorInput.removeAttr("disabled");

            userGroupInput.chosen({no_results_text: `${Translator.trans('operatori.no_found_select_user_group', {}, 'messages', language)}`,allow_single_deselect: true});
            userGroupInput.trigger("chosen:updated");
            operatorInput.chosen({no_results_text: `${Translator.trans('operatori.no_found_select_operator', {}, 'messages', language)}`,allow_single_deselect: true});
            operatorInput.chosen("chosen:updated");

          })
      } catch (e) {
        console.log(e)
      }

    }else{
      users.refreshToken().then(() =>{
        userGroupInput.chosen({no_results_text: `${Translator.trans('operatori.no_found_select_user_group', {}, 'messages', language)}`,allow_single_deselect: true});
        userGroupInput.trigger("chosen:updated");
        userGroupInput.trigger('change');
      })

    }
  }

  userGroupInput.on('change', () => {
    if (userGroupInput.val()) {
      assigneeLoader.html('');
      operatorInput.empty();
      assignBtn.removeAttr("disabled");
      operatorInput.chosen("destroy");
      initFilteredOperatorsSelect();
    }
  });


  $('.edit-meeting').on('click', function editMeeting(e) {
    let el = $(e.target)
    let payload = {}
    if (el.data('status')) {
      payload['status'] = el.data('status');
    }

    if (el.data('expiration') && el.data('extend-seconds')) {
      let currentExpiration = moment(el.data('expiration'));
      let extendSeconds = parseInt(el.data('extend-seconds'));
      let newExpiration = currentExpiration.add(extendSeconds, 's');
      payload['draft_expiration'] = newExpiration.format()
    }

    if ($.isEmptyObject(payload)) {
      return;
    }

    let errorEl = el.closest('div').find('.update_error');
    errorEl.addClass('d-none');

    if(confirm(el.data('confirm')) === true){
      $.ajax({
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${$('#hidden').data('token')}`
        },
        url: el.data('url'),
        type: 'PATCH',
        data: JSON.stringify(payload),
        success: function (response, textStatus, jqXhr) {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          errorEl.removeClass('d-none');
        }
      });
    }
  });

  $('#modal_approve').on('click', function () {

    $('#outcome_outcome_0').prop('checked', true);
    $('#outcome_outcome_1').prop('checked', false);

    $('#modalTitle').html(`${Translator.trans('pratica.approved_pratice', {}, 'messages', language)}`);
    $('#email_text').show();
    if ($('#outcome_payment_amount').length > 0) {
      $('#outcome_payment_amount').closest('.form-group').removeClass('d-none');
      $('#outcome_payment_amount').attr('required', 'required');
    }
  });

  $('#modal_refuse').on('click', function () {

    $('#outcome_outcome_1').prop('checked', true);
    $('#outcome_outcome_0').prop('checked', false);

    $('#modalTitle').html(`${Translator.trans('pratica.reject_pratice', {}, 'messages', language)}`);
    $('#email_text').hide();
    if ($('#outcome_payment_amount').length > 0) {
      $('#outcome_payment_amount').closest('.form-group').addClass('d-none');
      $('#outcome_payment_amount').removeAttr('required');
    }
  });

  RequestIntegration.init()

  //Init TextArea
  let messageTextEditor = null;
  TextEditor.init((editor,messageText)  => {
     messageTextEditor = messageText;
  });

  // Tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // Init Details Payment
  if ($('.payment-list').length > 0) {
    InfoPayment.init();
  }

  //Operator Message
  if ($('#change_paid_modal').length > 0) {
    ApplicationsMessage.init();
  }

  // Map
  if ( $('#map').length > 0) {
    ApplicationMap.init()
  }

  // Bolli
  // Todo: create specific class
  function initStampReasonChange()
  {
    $('.js-stamp-item .reason').each((k, v) => {
      $(v)
        .off('keyup')
        .on('keyup', (e) => {
          $(e.currentTarget).closest(".form-group").find('.identifier').val(String.snakeCase($(v).val()));
        });
    })
  }
  initStampReasonChange();

  $('#add-stamp').click(function (e) {
    e.preventDefault();
    let list = $('#current-stamps');
    let counter = list.data('widget-counter') || list.children().length;

    if ($('#no-stamps').length) {
      $('#no-stamps').remove();
    }

    let newWidget = $('#stamp-item-template').text();
    newWidget = newWidget.replace(/__name__/g, new Date().getTime());
    counter++;
    list.data('widget-counter', counter);

    let newElem = $(list.attr('data-widget-stamp')).html(newWidget);
    newElem.appendTo(list);
    initStampReasonChange();
  });

  $("#current-stamps").on("click", "a.js-remove-stamp", function (e) {
    e.preventDefault();
    $(this).closest('.js-stamp-item').remove();
    if ($('.js-stamp-item').length === 0) {
      $('#current-stamps').append(`<small id="no-stamps" class="d-block m-2 text-muted"><i class="fa fa-info-circle"></i> ${Translator.trans('servizio.stamps.helper', {}, 'messages', language)}</small>`);
    }
  });


  if ($('#message_applicant').length > 0) {
    $('#message_applicant').on('click',function (e) {
      if (messageTextEditor) {
        return confirm($('#message_applicant').data('message'))
      }else{
        Swal.fire(
          `${Translator.trans('error_message_detail', {}, 'messages', language)}`,
          `${Translator.trans('pratica.messaggio_operatore_vuoto', {}, 'messages', language)}`,
          'error'
        );
        return false;
      }
    })
  }

  $('#request_integration_form_attachments').ready(function () {

    function addAttachment(files) {

      const list = $('#request_integration_form_attachments');

      let counter = list.data('widget-counter') || list.children().length;
      let prototype = list.attr('data-prototype');

      prototype = prototype.replace(/__name__label__/g, '');
      prototype = prototype.replace(/__name__/g, counter);
      const prototypeEl = $(prototype);
      list.append(prototypeEl);

      const inputSelector = "#request_integration_form_attachments_" + counter;

      const removeButton = $('<span class="close"><i class="el-icon-close"></i></span>');
      removeButton.on('click', removeAttachment);
      removeButton.insertAfter(inputSelector);
      $('<span class="document"><i class="el-icon-document"></i></span>').insertBefore(inputSelector);

      $(inputSelector).prop('files', files)

      list.data('widget-counter', ++counter);
      $('#add-attachment-collection-widget button').trigger('blur')

    }

    $('#request_integration_load_attachment').on('change', (e) => {
      addAttachment(e.target.files)
      e.target.value = null
    })

    function removeAttachment(event) {
      $(event.target).parent().parent().remove();
    }

    $('#add-attachment-collection-widget button').on('click', () => {
      $('#request_integration_load_attachment').trigger('click')
    });
  })
});
