import React, {useEffect, useState, useMemo} from "react";
import {
  Spinner,
  notify,
  NotificationManager,
  Icon,
  Button,
} from "design-react-kit";
import axios from "axios";
import {useAuth} from "../contexts/AuthContext";
import {uniqueArrayProperty} from "../../utils/UniqueArrayProperty";
import {getPaymentStatus} from "../../utils/PaymentStatus";
import {useLanguage} from "../contexts/LanguageContext";
import DateRangePickerWrapper from "../../components/DateRangePicker/DateRangePicker";
import DataTable from "react-data-table-component";
import currencyFormatter from "../../utils/currencyFormatter";
import moment from "moment";
import {JobsFileUploader} from "../../components/JobsFileUploader";
import {useNavigate, useSearchParams} from "react-router-dom";
import {DebounceInput} from "../../components/DebounceInput/DebounceInput";

function PaymentsServiceList() {

  const [paymentList, setPaymentList] = useState([]);

  const [filterList, setFilterList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [open, toggle] = useState(false);
  const [status, setStatus] = useState(false);
  const [fiscalCode, setFiscalCode] = useState(null);
  const [createdAt, setCreatedAt] = useState(`created_at[after]=${moment().subtract(1, 'month').format("YYYY-MM-DD")}&created_at[before]=${moment().add(1, 'month').format("YYYY-MM-DD")}`);
  const apiUrl =
    window.location.pathname.split("/")[1];

  const [data, setData] = useState([]);
  const [filteredItems, setFilteredItems] = useState([]);
  const [enableDueImport, setEnableDueImport] = useState(false);
  const [enableFilters, setEnableFilters] = useState(true);
  const [filterText, setFilterText] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
  const [service, setService] = useState('');
  const [searchParams] = useSearchParams();
  const [idService, setIdService] = useState(null);

  const [fileName, setFileName] = useState("");

  const navigate = useNavigate();

  const handleFile = (file) => {
    setFileName(file.name);
  };

  const {authUser} = useAuth();

  const {lang} = useLanguage();

  useEffect(() => {
    if (authUser?.token && authUser?.token !== '') {
      if(searchParams.get('service_id')){
        setIdService(searchParams.get('service_id'))
        localStorage.setItem('service_id', JSON.stringify(searchParams.get('service_id')));
      }else{
        const items = JSON.parse(localStorage.getItem('service_id'));
        if (items) {
          setIdService(items);
        }
      }
    }
  }, [authUser?.token]);

  useEffect(() => {
    if(idService){
      getServices()
    }
  },[idService])


  const getPayments = () => {
    setLoading(true);
    const config = {
      headers: {
        Authorization: `Bearer ${authUser?.token}`,
      },
    };

    axios
      .get(
        `/${apiUrl}/api/payments?${idService ? `service_id=${idService}` : ''}${status ? `&status=${status}` : ""}${
          createdAt ? `&${createdAt}` : ""
        }${fiscalCode ? `&payer_tax_identification_number=${fiscalCode}` : ''}`,
        config
      )
      .then((list) => {

        setPaymentList(list.data.data);
        if (!status) {
          setFilterList(uniqueArrayProperty(list.data.data, "status"));
        }
        setData(list.data.data);
        setFilteredItems(list.data.data);
        setLoading(false);
      })
      .catch((e) => {
        if (e.response.status !== 404) {
          notify(
            Translator.trans("warning", {}, "messages", lang),
            <p>{e.message}</p>,
            {
              dismissable: true,
              state: "error",
              duration: 3000,
            }
          );
        } else {
          setFilteredItems([])
          setPaymentList([]);
        }
        setLoading(false);
      });
    setLoading(true);
  };

  const getServices = () => {
    const config = {
      headers: {
        Authorization: `Bearer ${authUser.token}`,
      },
    };

    axios
      .get(
        `/${apiUrl}/api/services/${idService}`,
        config
      )
      .then((response) => {
        setService(response.data.name);
        if (response.data.is_payment_configured) {
          setEnableDueImport(true)
          setEnableFilters(true)
          getPayments();
        } else {
          setEnableFilters(true)
          setErrorMessage(Translator.trans("servizio.no_enabled_payment_service", {}, "messages", lang))
        }

      })
      .catch((e) => {
        if (e.response.status !== 404) {
          notify(
            Translator.trans("warning", {}, "messages", lang),
            <p>{e.message}</p>,
            {
              dismissable: true,
              state: "error",
              duration: 3000,
            }
          );
          setLoading(false)
        }else{
          setErrorMessage(Translator.trans("servizio.no_enabled_payment_service", {}, "messages", lang))
          setLoading(false)
        }
      });
  };

  useEffect(() => {
    if (authUser?.token && authUser?.token !== '' && (status || status === null || createdAt || fiscalCode)) {
      if (enableFilters) {
        getPayments();
      }
    }
  }, [status, createdAt, enableFilters, fiscalCode]);


  const handleAutocompleteCallback = (childData) => {
    if (childData && childData.length === 16) {
      setFiscalCode(childData.toUpperCase())
    } else if (childData === '') {
      setFiscalCode('')
    }
  }


  const handleClickFilterStatus = (chosenState) => {
    if (status === chosenState) {
      setStatus(null);
    } else {
      setStatus(chosenState);
    }
    toggle(!open);
  };


  const childToParent = ({startDate, endDate}) => {
    if (startDate && endDate) {
      setCreatedAt(
        `created_at[after]=${startDate.format(
          "YYYY-MM-DD"
        )}&created_at[before]=${endDate.add(1, 'month').format("YYYY-MM-DD")}`
      );
    }
  };

  const detectChanges  = ({startDate, endDate}) => {
    setFilterText(Translator.trans("servizio.no_payment_selected_data", {}, "messages", lang))
  }


  const columns = useMemo(
    () => [
      {
        name: Translator.trans("user.profile.codice_fiscale", {}, "messages", lang),
        selector: (row, i) => row.payer.tax_identification_number,
        sortable: true,
      },
      {
        name: Translator.trans("operatori.pagamento", {}, "messages", lang),
        selector: (row, i) => currencyFormatter(row.payment.amount),
        sortable: true,
      },
      {
        name: Translator.trans("general.stato", {}, "messages", lang),
        selector: (row, i) => getPaymentStatus(row.status.toString()),
        sortable: true,
      },
      {
        name: Translator.trans("created_at", {}, "messages", lang),
        selector: (row, i) =>
          moment(moment.utc(row.created_at).toDate())
            .local(lang || "it")
            .format("L LTS"),
        sortable: true,
      },
      {
        cell: (row) => (
          <Button
            outline
            size={"sm"}
            color={"primary"}
            onClick={() => handleNavigateToDetails(row.id)}
          >
            {Translator.trans("user.dashboard.vai_al_dettaglio", {}, "messages", lang)}
          </Button>
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
    ],
    []
  );

  const paginationComponentOptions = {
    rowsPerPageText: Translator.trans("row_for_page", {}, "messages", lang),
    rangeSeparatorText: Translator.trans("of", {}, "messages", lang),
    selectAllRowsItem: true,
    selectAllRowsItemText: Translator.trans("tutti", {}, "messages", lang),
  };

  const handleNavigateToDetails = (service_id) => {
    navigate(`${service_id}`)
  };

  //  Internally, customStyles will deep merges your customStyles with the default styling.
  const customStyles = {
    rows: {
      style: {
        fontSize: "18px",
        minHeight: "72px", // override the row height
      },
    },
    headCells: {
      style: {
        paddingLeft: "8px", // override the cell padding for head cells
        paddingRight: "8px",
      },
    },
    cells: {
      style: {
        paddingLeft: "8px", // override the cell padding for data cells
        paddingRight: "8px",
      },
    },
    head: {
      style: {
        fontSize: "18px",
        fontWeight: 600,
      },
    },
    subHeader: {
      style: {
        justifyContent: "center",
        width: "100%",
        padding: "0px",
      },
    },
  };


  const subHeaderComponentMemo = useMemo(() => {

    return (
      <div className="container w-100 my-3">
        {enableFilters ? (
          <div className="my-3 w-100 row">
            <div className='col-12 col-md-4 my-3 max-w-300'>
              <DebounceInput onInputValueChange={handleAutocompleteCallback}
                             labelName={Translator.trans("operatori.search_for_fiscal_code", {}, "messages", lang)}></DebounceInput>
            </div>
            <div className="col-12 col-md-4 max-w-300 mx-auto select-wrapper my-3">
              <label htmlFor="stateSelect">{Translator.trans("pratica.filtra_per_stato", {}, "messages", lang)}</label>
              <select id="stateSelect" onChange={(e) => handleClickFilterStatus(e.target.value)}>
                <option value="">{Translator.trans("pratica.choose_options", {}, "messages", lang)}</option>
                {filterList.map((item, index) => (
                  <option
                    key={`item-${index}`}
                    value={item.status}
                  >
                    {getPaymentStatus(item.status)}
                  </option>
                ))}
              </select>
            </div>
            <div className='col-12 col-md-4 my-3 max-w-300'>
              <DateRangePickerWrapper
                childToParent={childToParent} detectChanges={detectChanges}
              ></DateRangePickerWrapper>
            </div>
          </div>
        ) : null}
      </div>
    );
  }, [filterList, fiscalCode]);

  const actionsComponentMemo = useMemo(() => {
    return (
      <div>
        {enableDueImport ?
          <JobsFileUploader handleFile={handleFile}/> : null}
      </div>
    );
  }, [enableDueImport]);

  return (
    <section className="mb-50 mb-lg-90">
      <NotificationManager></NotificationManager>
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            {enableDueImport ?
              <DataTable
                title={service}
                columns={columns}
                data={filteredItems}
                defaultSortField="created_at"
                progressPending={loading}
                sortIcon={<Icon icon="it-arrow-down"/>}
                progressComponent={<Spinner active/>}
                pagination
                subHeader
                actions={actionsComponentMemo}
                subHeaderComponent={subHeaderComponentMemo}
                paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
                striped={true}
                customStyles={customStyles}
                highlightOnHover
                pointerOnHover
                paginationComponentOptions={paginationComponentOptions}
                fixedHeader={false}
                responsive={true}
                noDataComponent={filterText ? filterText : Translator.trans("servizio.no_payment_service", {}, "messages", lang)}
              /> : null}
            {errorMessage ? errorMessage : null}
          </div>
        </div>
      </div>
    </section>
  );
}

export default PaymentsServiceList;
