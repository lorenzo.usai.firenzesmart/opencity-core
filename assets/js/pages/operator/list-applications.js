import '../../../app/css/component-chosen.min.css'
import '../../../app/css/daterangepicker.css'
import "../../../styles/pages/operator/list-applications.scss";

import * as jsrender from "jsrender";
import moment from "moment";
import '../../../app/js/daterangepicker'
import '../../../app/js/chosen.jquery.min'


const language = document.documentElement.lang.toString();
moment.locale(language)
$.views.helpers({
  'formatId': function (id) {
    return id.split('-')[4];
  },
  'formatDate': function (date, format) {
    return moment.unix(date).format(format);
  },
  'formatStatus': function (state) {
    var name = '?';
    var selector = $('#filter-stato option[value="' + state + '"]');
    if (selector.length > 0) {
      return selector.text();
    }
    return name;
  },
  'formatService': function (service) {
    var name = '?';
    var selector = $('#filter-servizio option[data-slug="' + service + '"]');
    if (selector.length > 0) {
      return selector.data('name');
    }
    return name;
  },
  'assignUrl': function (id) {
    return $('#results-data').data('assign').replace('X', id);
  },
  'showUrl': function (id) {
    return $('#results-data').data('show').replace('X', id);
  },
  'checkTypeData': function (value) {
    if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
      return value.toString();
    } else if (value instanceof Object && value.label) {
      return value.label;
    } else if (Array.isArray(value)) {
      if (value.some(e => e.hasOwnProperty('data'))) {
        return 'N° file: ' + value.length;
      }
      return value;
    }
  }
});
$.fn.animateHighlight = function (highlightColor, duration) {
  var highlightBg = highlightColor || "#00cf86";
  var animateMs = duration || 2000;
  var originalBg = this.css("background-color");
  if (!originalBg || originalBg === highlightBg)
    originalBg = "#FFFFFF"; // default to white
  jQuery(this)
    .css("backgroundColor", highlightBg)
    .animate({backgroundColor: originalBg}, animateMs, null, function () {
      jQuery(this).css("backgroundColor", originalBg);
    });
};
$.extend({
  distinct: function (anArray) {
    var result = [];
    $.each(anArray, function (i, v) {
      if ($.inArray(v, result) === -1) result.push(v);
    });
    return result;
  }
});
$(document).ready(function () {
  var localStoragePrefix = '{{ instance_service.currentInstance }}-';
  var resultsContainer = $('#results-data');
  var spinnerTemplate = $.templates("#tpl-spinner");
  var emptyTemplate = $.templates("#tpl-empty");
  var resultsTemplate = $.templates("#tpl-result");
  var defaultLimit = 10;
  var reset = $('#reset');
  var filtersContainer = $('#filters');
  var filterServizio = $('#filter-servizio');
  var filterStato = $('#filter-stato');
  var filterGeographicArea = $('#filter-geographic-area');
  var filterWorkflow = $('#filter-workflow');
  var filterQueryBy = $('#filter-query-by');
  var filterQuery = $('#filter-query');
  var filterPratiche = $('#filter-pratiche');
  var filterCollate = $('#filter-collate');
  var filterStatusRange = $('input[name="status-range"]');
  var chosenSelect = $('.form-control-chosen');
  var sort = 'submissionTime';
  var order = 'asc';
  chosenSelect.chosen();

  filterStatusRange.daterangepicker({
    "locale": {
      "format": "DD/MM/YYYY HH:mm",
      "separator": " - ",
      "applyLabel": Translator.trans('apply', {}, 'messages', language),
      "cancelLabel": Translator.trans('cancel', {}, 'messages', language),
      "fromLabel": Translator.trans('from', {}, 'messages', language),
      "toLabel": Translator.trans('to', {}, 'messages', language),
      "customRangeLabel": Translator.trans('custom', {}, 'messages', language),
      "firstDay": 1
    },
    timePicker: true,
    timePicker24Hour: true,
    alwaysShowCalendars: true,
    autoUpdateInput: false,
    opens: "center",
    ranges: {
      [Translator.trans('time.today', {}, 'messages', language)]: [moment().startOf('day'), moment().endOf('day')],
      [Translator.trans('time.yesterday', {}, 'messages', language)]: [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
      [Translator.trans('time.this_week', {}, 'messages', language)]: [moment().startOf('week'), moment().endOf('week')],
      [Translator.trans('time.last_week', {}, 'messages', language)]: [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
      [Translator.trans('time.this_month', {}, 'messages', language)]: [moment().startOf('month'), moment().endOf('month')],
      [Translator.trans('time.last_month', {}, 'messages', language)]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  }).on('apply.daterangepicker', function (ev, picker) {
    filterStatusRange.data('start', picker.startDate.format('X'));
    filterStatusRange.data('end', picker.endDate.format('X'));
    $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm') + ' - ' + picker.endDate.format('DD/MM/YYYY HH:mm'));
    loadContents();
  }).on('cancel.daterangepicker', function (ev, picker) {
    filterStatusRange.removeData('start').removeData('end').val('');
    loadContents();
  });

  var viewSettingsStorage = {
    setItem: function (key, value) {
      if (typeof (Storage) !== "undefined") {
        localStorage.setItem(localStoragePrefix + key, JSON.stringify(value));
      }
    },
    getItem: function (key, fallback) {
      if (typeof (Storage) !== "undefined") {
        if (localStorage.getItem(localStoragePrefix + key)) {
          var value = localStorage.getItem(localStoragePrefix + key);
          try {
            return JSON.parse(value);
          } catch (e) {
            return value;
          }
        }
      }

      return fallback || null;
    },

    hasItem: function (key) {
      if (typeof (Storage) !== "undefined") {
        return !(localStorage.getItem(localStoragePrefix + key) === null);
      }
      return false
    },

    removeItem: function (key) {
      if (typeof (Storage) !== "undefined" && this.hasItem(key)) {
        localStorage.removeItem(localStoragePrefix + key)
      }
    },

    displayField: function (needle, haystack) {
      return haystack[key];
    }
  };

  var buildParams = function () {
    var params = {
      'gruppo': null,
      'servizio': null,
      'stato': filterStato.val(),
      'geographic_area': filterGeographicArea.val(),
      'workflow': filterWorkflow.val(),
      'query_field': filterQueryBy.val(),
      'query': filterQuery.val(),
      'sort': sort || 'submissionTime',
      'order': order || 'asc',
      'limit': filterPratiche.val() != null ? filterPratiche.val() : defaultLimit,
      'collate': filterCollate.is(':checked') ? 1 : 0,
      'last_status_change': filterStatusRange.data('start') ? [filterStatusRange.data('start'), filterStatusRange.data('end')] : []
    };
    if (filterServizio.children("option:selected").data('type') === 'group') {
      params.gruppo = filterServizio.val();
    } else {
      params.servizio = filterServizio.val();
    }
    viewSettingsStorage.setItem('pratiche_filters', params);
    return params;
  };

  var isEmpty = function (object) {
    return object.servizio === ''
      && object.gruppo === ''
      && object.stato === ''
      && object.geographic_area === ''
      && object.workflow === ''
      && object.query === ''
      && object.collate !== 1
      && object.last_status_change === [];
  };

  reset.on('click', function (e) {
    if (typeof (Storage) !== "undefined") {
      viewSettingsStorage.setItem('pratiche_filters', {
        'gruppo': '',
        'servizio': '',
        'stato': '',
        'geographic_area': '',
        'workflow': '',
        'query_field': '1',
        'query': '',
        'limit': defaultLimit,
        'collate': 0,
        'last_status_change': []
      });
    }
    //viewSettingsStorage.removeItem('pratiche_filters')
    filterStatusRange.removeData('start').removeData('end').val('');
    // Ricarico la pagina senza la query string
    window.location = window.location.pathname;
    e.preventDefault();
  });

  var showReset = function () {
    reset.parent().show();
  };

  var hideReset = function () {
    reset.parent().hide();
  };

  filtersContainer.find('.filter-select').on('change', function () {
    loadContents();
  });
  filterCollate.on('change', function () {
    loadContents();
  })
  filterQuery.on('keydown', function (e) {
    if (e.keyCode === 13) {
      loadContents();
    }
  });

  var loadContents = function () {
    resultsContainer.html(spinnerTemplate.render({}));
    var params = buildParams();
    if (isEmpty(params)) {
      hideReset();
    } else {
      showReset();
    }
    params.offset = 0;
    $.get(resultsContainer.data('source'), params, function (response) {
      loadResponse(response);
    });
  };

  var loadUrl = function (url) {
    if (url.length) {
      resultsContainer.html(spinnerTemplate.render({}));
      $.get(url, function (response) {
        loadResponse(response);
      });
    }
  };

  var refreshDownloadButton = function (response) {
    var downloadParams = $.extend({}, response.meta.parameter);
    downloadParams['extra_headers[]'] = []
    $.each(response.meta.schema, function () {
      var fields = viewSettingsStorage.getItem('fields-' + response.meta.parameter.servizio, []);
      if ($.inArray(this.name, fields) > -1) {
        downloadParams['extra_headers[]'].push(
          $.trim(resultsContainer.find('input[data-show="' + this.name + '"]').data('label'))
        );
      }
    });
    var queryString = jQuery.param(downloadParams, true);
    var downloadButton = resultsContainer.find('[data-download="true"]');
    // Todo: find better way
    downloadButton.attr('href', downloadButton.data('href') + '?' + queryString.replace(/last_status_change/g, 'last_status_change[]'));
  };

  var refreshCalculatedFields = function (response) {
    var calcParams = $.extend({}, response.meta.parameter);
    var tbody = $('tbody.calc');
    tbody.css('display', 'none');
    tbody.find('div.calc').css('display', 'none');
    resultsContainer.find('input[data-function]').each(function () {
      var func = $(this).data('function');
      var divFunc = $('div.calc-' + func);
      var storageCalculateKey = 'calculate-' + func + '-' + response.meta.parameter.servizio;
      var calculateFields = viewSettingsStorage.getItem(storageCalculateKey, []);
      if (calculateFields.length > 0) {
        tbody.css('display', 'table-row-group');
        $.each(calculateFields, function () {
          resultsContainer.find('[data-function="' + func + '"][data-calculate="' + this + '"]').prop('checked', true);
          tbody.find('[data-key="' + this + '"] div.calc-' + func).css('display', 'block');
          divFunc.find('[data-calc="' + this + '"]').html('<div class="loader"></div>');
        })
        calcParams[func + '[]'] = calculateFields;
      }
    });
    $.get(tbody.data('source'), calcParams, function (calcResponse) {
      $.each(calcResponse, function (func, values) {
        $.each(values, function (key, value) {
          $('div.calc-' + func).find('[data-calc="' + key + '"]').html(value);
        });
      });
    });
  };

  $('#reload').on('click', function (e) {
    loadContents();
    e.preventDefault();
  });

  var loadResponse = function (response) {
    if (response.meta.count === 0) {
      resultsContainer.html(emptyTemplate.render({}));
    } else {

      // Popolo la select degli stati
      var filterStatoVal = filterStato.val();
      if (response.filters.states.length > 0) {
        filterStato.html('');
        $.each(response.filters.states, function (k, v) {
          var selected = '';
          if (filterStatoVal == v.id) {
            selected = ' selected="selected"';
          }
          filterStato.append('<option value="' + v.id + '"' + selected + '>' + v.name + '</option>')
        });
        chosenSelect.trigger("chosen:updated");
      }

      response.currentPage = response.meta.count > 0 ?
        (response.meta.parameter.offset > 0 ? Math.ceil(response.meta.parameter.offset / response.meta.parameter.limit) + 1 : 1)
        : 0;
      response.pageCount = response.meta.count > 0 ? Math.ceil(response.meta.count / response.meta.parameter.limit) : 0;
      var result = $(resultsTemplate.render(response));
      resultsContainer.html(result);
      resultsContainer.find('a.page').on('click', function (e) {
        loadUrl($(this).data('url'));
        e.preventDefault();
      });
      resultsContainer.find('a[data-sort]').on('click', function (e) {
        sort = $(this).data('sort');
        order = $(this).data('order');
        loadContents();
        e.preventDefault();
      });

      resultsContainer.find('.copy').click(function (e) {
        e.preventDefault();
        var button = $(this);
        var temp = $("<input>");
        $("body").append(temp);
        temp.val(button.data('copy')).select();
        document.execCommand("copy");
        temp.remove();
        button.animateHighlight();
      });
      resultsContainer.find('tr[data-href]').on('click', function (e) {
        if (e.target.tagName === 'TD') {
          window.location = $(this).data('href');
        }
      });
      refreshDownloadButton(response);
      resultsContainer.find('[data-toggle="tooltip"]').tooltip();

      if (response.meta.schema) {

        resultsContainer.find('[data-dismiss="modal"]').on('click', function (e) {
          refreshCalculatedFields(response);
        })

        var storageShowKey = 'fields-' + response.meta.parameter.servizio;
        var fields = viewSettingsStorage.getItem(storageShowKey, []);
        resultsContainer.find('input[data-show]').on('change', function () {
          var fieldKey = $(this).data('show');
          if ($(this).is(':checked')) {
            resultsContainer.find('[data-key="' + fieldKey + '"]').css('display', 'table-cell');
            fields.push(fieldKey);
          } else {
            resultsContainer.find('[data-key="' + fieldKey + '"]').css('display', 'none');
            resultsContainer.find('[data-calculate="' + fieldKey + '"]').prop('checked', false).trigger('change');
            fields = jQuery.grep(fields, function (value) {
              return value !== fieldKey;
            });
          }
          viewSettingsStorage.setItem(storageShowKey, $.distinct(fields));
          refreshDownloadButton(response);
        });
        resultsContainer.find('input[data-calculate]').on('change', function () {
          var fieldKey = $(this).data('calculate');
          if ($(this).is(':checked')) {
            resultsContainer.find('[data-show="' + fieldKey + '"]').prop('checked', true).trigger('change');
          }
        });

        resultsContainer.find('input[data-calculate]').on('change', function () {
          var func = $(this).data('function');
          var fieldKey = $(this).data('calculate');
          var storageCalculateKey = 'calculate-' + func + '-' + response.meta.parameter.servizio;
          var calculateFields = viewSettingsStorage.getItem(storageCalculateKey, []);
          if ($(this).is(':checked')) {
            calculateFields.push(fieldKey);
          } else {
            calculateFields = jQuery.grep(calculateFields, function (value) {
              return value !== fieldKey;
            });
          }
          viewSettingsStorage.setItem(storageCalculateKey, $.distinct(calculateFields));
        });

        $.each(response.meta.schema, function () {
          if ($.inArray(this.name, fields) > -1) {
            resultsContainer.find('input[data-show="' + this.name + '"]').trigger('click');
          }
        });
        refreshCalculatedFields(response);
      }

    }
  };

  var calculateInputSearchWidth = function () {

    // Get input width filter-query element
    $('.filter-query .chosen-drop').css('width',$('.filter-query').width());

    $( window ).on( "resize", function() {
      $('.filter-query .chosen-drop').css('width',$('.filter-query').width());
    } );
  }

  if (viewSettingsStorage.hasItem('pratiche_filters')) {
    var params = viewSettingsStorage.getItem('pratiche_filters');
    filterServizio.val(params.servizio);
    filterStato.val(params.stato);
    if (!filterGeographicArea.val()) {
      filterGeographicArea.val(params.geographic_area);
    }
    filterWorkflow.val(params.workflow);
    filterQueryBy.val(params.query_field);
    filterQuery.val(params.query);
    filterPratiche.val(params.limit ? params.limit : defaultLimit);
    sort = params.sort;
    order = params.order;
    if (params.collate === 1) {
      filterCollate.prop('checked', true);
    } else {
      filterCollate.prop('checked', false);
    }
    showReset();
    chosenSelect.trigger("chosen:updated");
    if ($.isArray(params.last_status_change) && params.last_status_change.length === 2) {
      filterStatusRange.data('start', params.last_status_change[0]);
      filterStatusRange.data('end', params.last_status_change[1]);
      filterStatusRange.val(moment.unix(params.last_status_change[0]).format('DD/MM/YYYY HH:mm') + ' - ' + moment.unix(params.last_status_change[1]).format('DD/MM/YYYY HH:mm'));
    }
  }

  loadContents();

  calculateInputSearchWidth()

});
