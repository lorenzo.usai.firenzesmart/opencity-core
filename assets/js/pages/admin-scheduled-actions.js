import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import '../../app/css/daterangepicker.css'
import '../utils/datatables'
import Swal from 'sweetalert2/src/sweetalert2.js'
import moment from "moment";
import '../../app/js/daterangepicker'


const language = document.documentElement.lang.toString();
$(document).ready(function () {



  let table; //Table reference
  const datatableSetting = JSON.parse(decodeURIComponent($('#scheduled-actions').data('config')));
  const datatableOptions = {
    dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    searching: true,
    paging: true,
    pagingType: 'simple_numbers',
    "drawCallback": function( settings ) {

      const api = new $.fn.dataTable.Api(settings);
      $('[data-toggle="popover"]').popover();

      $('.action-log').on('click', function (e) {
        e.preventDefault();
        let target = $(this);
        Swal.fire({
          title: target.data('title'),
          text: target.data('content'),
          showCloseButton: true,
          showConfirmButton: false,
        })
      });

      $('#filter-status').on('change', function (e) {
        api.column(5).search($(this).val()).draw();
      });

      // Filtro Data crazione
      const filterStatusRange = $('#filter-created-at');
      filterStatusRange.daterangepicker({
        "locale": {
          "format": "DD/MM/YYYY HH:mm",
          "separator": " - ",
          "applyLabel": Translator.trans('apply', {}, 'messages', language),
          "cancelLabel": Translator.trans('cancel', {}, 'messages', language),
          "fromLabel": Translator.trans('from', {}, 'messages', language),
          "toLabel": Translator.trans('to', {}, 'messages', language),
          "customRangeLabel": Translator.trans('custom', {}, 'messages', language),
          "firstDay": 1
        },
        timePicker: true,
        timePicker24Hour: true,
        alwaysShowCalendars: true,
        autoUpdateInput: false,
        opens: "center",
        ranges: {
          [Translator.trans('time.today', {}, 'messages', language)]: [moment().startOf('day'), moment().endOf('day')],
          [Translator.trans('time.yesterday', {}, 'messages', language)]: [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
          [Translator.trans('time.this_week', {}, 'messages', language)]: [moment().startOf('week'), moment().endOf('week')],
          [Translator.trans('time.last_week', {}, 'messages', language)]: [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
          [Translator.trans('time.this_month', {}, 'messages', language)]: [moment().startOf('month'), moment().endOf('month')],
          [Translator.trans('time.last_month', {}, 'messages', language)]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }).on('apply.daterangepicker', function (ev, picker) {
        filterStatusRange.data('start', picker.startDate.format('X'));
        filterStatusRange.data('end', picker.endDate.format('X'));
        $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm') + ' - ' + picker.endDate.format('DD/MM/YYYY HH:mm'));
        api.column(7).search(picker.startDate.format('YYYY-MM-DD HH:mm:00') + '|' + picker.endDate.format('YYYY-MM-DD HH:mm:00')).draw();

      }).on('cancel.daterangepicker', function (ev, picker) {
        filterStatusRange.removeData('start').removeData('end').val('');
        api.column(7).search('').draw();
      });


      $('.scheduled-action-retry').on('click', function (e) {
        e.preventDefault();
        $.ajax({
          url: $(this).data('retry-url'),
          dataType: 'json',
          cache: false,
          type: 'GET',
          success: function (response, textStatus, jqXhr) {
            api.draw();
          },
          error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!'
            })
          }
        });
      });
    },
  };


  $('#scheduled-actions').initDataTables(datatableSetting, datatableOptions)
    .then(function (dt) {
      // dt contains the initialized instance of DataTables
      table = dt.api();
      console.log(dt)
    });


});
