import Auth from "../../../rest/auth/Auth";
import axios from "axios";

const $language = document.documentElement.lang.toString();

$(document).ready(function () {
  if($('#container-f24').length > 0) {
    const f24Element = $('#container-f24');
    const auth = new Auth()
    let token = null
    let retry = 0
    auth.getSessionAuthTokenPromise().then((data) => {
      token = data.token;
      const interval = setInterval(() => {
        axios.get(`${f24Element.data('url')}/api/applications/${f24Element.data('application')}`, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
          .then(function (res) {
            retry++
            if (res.data.status == '2000') {
              clearInterval(interval);
              window.location.reload()
            }else if(retry > 3){
              clearInterval(interval);
              $('.alert-error').removeClass('d-none');
              $('.btn-f24-loader').remove()
              $('.alert-error').html(`${Translator.trans("pratica.error_too_late_receipt", {}, "messages", $language)}`);
            }
          })
      },10000) //Retry after 10 seconds
    }).catch((e) => {
      console.error(e);
    })
  }
});
