Upgrading From 2.27.0 To 2.28.0
===============================

Version 2.28.0 also supports Postgres 14, in addition to the already supported Postgres 11.

Upgrading From 2.13.0 To 2.14.0
===============================

Version 2.14 introduces a dependency on the database, not included in 
migrations.

Before upgrading add the PostGIS extention:

 * install the PostGIS extension, see http://postgis.net/install 
 * on each database run the command `CREATE EXTENSION postgis;`

The image of the postgres container referenced in the `docker-compose.yml` has been 
already updated.

Upgrading From 1.x To 2.0
=========================

Version 2 is a major change introducing updates in file and directory structure, symfony framework and all dependencies of the project.

Steps required for updating:
- Update source code to version 2
- Run ``docker-compose up -d --build``
- Enter into container php
- Run ``./compose_conf/php/sync-metadata-db.sh``
- Run ``./compose_conf/php/migrate-db.sh``

