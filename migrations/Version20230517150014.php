<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230517150014 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile_block (id UUID NOT NULL, user_id UUID NOT NULL, key VARCHAR(255) NOT NULL, value JSONB DEFAULT NULL, subform_url VARCHAR(255) NOT NULL, origin JSONB NOT NULL, hash VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_74CF08F0A76ED395 ON profile_block (user_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_profile_block ON profile_block (user_id, key, subform_url)');
        $this->addSql('ALTER TABLE profile_block ADD CONSTRAINT FK_74CF08F0A76ED395 FOREIGN KEY (user_id) REFERENCES utente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE servizio ADD profile_blocks JSONB DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE profile_block');
        $this->addSql('ALTER TABLE servizio DROP profile_blocks');
    }
}
