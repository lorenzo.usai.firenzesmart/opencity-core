<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230802203206 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job ADD errors JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE job DROP hostname');
        $this->addSql('ALTER TABLE job DROP retry');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F89033212A FOREIGN KEY (tenant_id) REFERENCES ente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FBD8E0F89033212A ON job (tenant_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job DROP CONSTRAINT FK_FBD8E0F89033212A');
        $this->addSql('DROP INDEX IDX_FBD8E0F89033212A');
        $this->addSql('ALTER TABLE job ADD hostname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD retry INT NOT NULL');
        $this->addSql('ALTER TABLE job DROP errors');
    }
}
